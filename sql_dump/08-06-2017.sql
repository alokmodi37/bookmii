create table `user_services`( `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT , `user_id` int(10) , `service_id` int(10) , `is_acitve` boolean DEFAULT '1' , PRIMARY KEY (`id`))  ;

alter table `profile` drop foreign key  `profile_service_id_foreign` ;
alter table `profile` drop column `service_id`;
