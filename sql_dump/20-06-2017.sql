create table `notification_token`( `user_id` int(10) , `firebase_token` varchar(255) )  ;
alter table `users` add column `device_token` varchar(255) NULL after `email`;
create table `notifications`( `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT , `user_id` int(10) , `notification` boolean DEFAULT '0' , PRIMARY KEY (`id`))  ;
