<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Enviroment
    |--------------------------------------------------------------------------
    |
    | Please provide the enviroment you would like to use for braintree.
    | This can be either 'sandbox' or 'production'.
    |
    */
    'environment' => 'sandbox',

    /*
    |--------------------------------------------------------------------------
    | Merchant ID
    |--------------------------------------------------------------------------
    |
    | Please provide your Merchant ID.
    |
    */
    'merchantId' => 'qjzg8tbhs5c39mz3',

    /*
    |--------------------------------------------------------------------------
    | Public Key
    |--------------------------------------------------------------------------
    |
    | Please provide your Public Key.
    |
    */
    'publicKey' => '29vm3tk42m2b6rw3',

    /*
    |--------------------------------------------------------------------------
    | Private Key
    |--------------------------------------------------------------------------
    |
    | Please provide your Private Key.
    |
    */
    'privateKey' => 'f0b0ea180f3e0b8a15bc55f20a02e478',

    /*
    |--------------------------------------------------------------------------
    | Client Side Encryption Key
    |--------------------------------------------------------------------------
    |
    | Please provide your CSE Key.
    |
    */
    'clientSideEncryptionKey' => 'MIIBCgKCAQEAqEIQMLjUWDrKDdSq759YTf2OU/gLkD9l4HJtlvFMD48IngifZYEn17ItV+GNNX1RXv4fmOx2SdieGA4vb9S8Wvclqh2OLyPc4FuLMm0fcf2uaAXTpJGD0iksvmEixtbvoVnyCiMNPavEanAOQWxj19twRNlP6+HGDTFngjFuULgH6kwJf9+L8KPKK5kg/YkUFRMvleWoMozsLkFy/u5MZVSZC6WhPkqqZ4si1x3Vh/YDHZED4ojPOGDxdREncxJs+Q2jMOoRPWjZTfooB/Ku9/w/hbGbZCpDi5fkRQM3CMGUzB+vngZt65nfOyICN81vagVOOnuM2a8mUMqQnelPkwIDAQAB',
    
];