-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 14, 2017 at 07:06 AM
-- Server version: 5.6.37
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apphos7_bookmii`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`apphos7_bookmii`@`localhost` FUNCTION `lowerword` (`str` VARCHAR(128), `word` VARCHAR(5)) RETURNS VARCHAR(128) CHARSET latin1 BEGIN
  DECLARE i INT DEFAULT 1;
  DECLARE loc INT;

  SET loc = LOCATE(CONCAT(word,' '), str, 2);
  IF loc > 1 THEN
    WHILE i <= LENGTH (str) AND loc <> 0 DO
      SET str = INSERT(str,loc,LENGTH(word),LCASE(word));
      SET i = loc+LENGTH(word);
      SET loc = LOCATE(CONCAT(word,' '), str, i);
    END WHILE;
  END IF;
  RETURN str;
END$$

CREATE DEFINER=`apphos7_bookmii`@`localhost` FUNCTION `tcase` (`str` VARCHAR(128)) RETURNS VARCHAR(128) CHARSET latin1 BEGIN 
  DECLARE c CHAR(1); 
  DECLARE s VARCHAR(128); 
  DECLARE i INT DEFAULT 1; 
  DECLARE bool INT DEFAULT 1; 
  DECLARE punct CHAR(17) DEFAULT ' ()[]{},.-_!@;:?/'; 
  SET s = LCASE( str ); 
  WHILE i <= LENGTH( str ) DO
    BEGIN 
      SET c = SUBSTRING( s, i, 1 ); 
      IF LOCATE( c, punct ) > 0 THEN 
        SET bool = 1; 
      ELSEIF bool=1 THEN  
        BEGIN 
          IF c >= 'a' AND c <= 'z' THEN  
            BEGIN 
              SET s = CONCAT(LEFT(s,i-1),UCASE(c),SUBSTRING(s,i+1)); 
              SET bool = 0; 
            END; 
          ELSEIF c >= '0' AND c <= '9' THEN 
            SET bool = 0; 
          END IF; 
        END; 
      END IF; 
      SET i = i+1; 
    END; 
  END WHILE;

  SET s = lowerword(s, 'A');
  SET s = lowerword(s, 'An');
  SET s = lowerword(s, 'And');
  SET s = lowerword(s, 'As');
  SET s = lowerword(s, 'At');
  SET s = lowerword(s, 'But');
  SET s = lowerword(s, 'By');
  SET s = lowerword(s, 'For');
  SET s = lowerword(s, 'If');
  SET s = lowerword(s, 'In');
  SET s = lowerword(s, 'Of');
  SET s = lowerword(s, 'On');
  SET s = lowerword(s, 'Or');
  SET s = lowerword(s, 'The');
  SET s = lowerword(s, 'To');
  SET s = lowerword(s, 'Via');

  RETURN s; 
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `account_detail`
--

CREATE TABLE `account_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transit_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `account_detail`
--

INSERT INTO `account_detail` (`id`, `user_id`, `account_number`, `transit_number`, `branch_number`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 2, '12345678904567894', '234567456', '345674567', 1, NULL, NULL),
(2, 3, '88383835353535353', '535386868', '686868686', 1, NULL, '2017-08-22 13:45:30'),
(3, 4, '55659595665656588', '989566565', '959896866', 1, NULL, NULL),
(4, 5, '98989895665665555', '686866866', '545754564', 1, NULL, NULL),
(5, 6, '55807646788668498', '656564884', '689864646', 1, NULL, NULL),
(6, 7, '84884848484888555', '858885556', '848848484', 1, NULL, NULL),
(7, 1, '545481854', '5454', '455454', 1, NULL, NULL),
(8, 11, '23456734567865789', '546745674', '324563456', 1, NULL, NULL),
(9, 14, '34423423423423423', '234234324', '323423423', 1, NULL, NULL),
(10, 19, '53457890985432345', '345678909', '456789098', 1, NULL, NULL),
(11, 20, '54848848484484994', '848488484', '545454545', 1, NULL, '2017-09-06 17:50:49'),
(12, 21, 'gfhf', '657575756', '575675757', 1, NULL, NULL),
(13, 23, '12312343214234234', '213134234', '412341234', 1, NULL, NULL),
(14, 24, '12345678901234567', '123456789', '123456789', 1, NULL, NULL),
(15, 29, '2131212312312330', '112312331', '12312123', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$WGDVcWyDWJx58FJaeHQDzebgQTFyP5nOUKixSkEmxoUeJ/CLmAfwO', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `provider_id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_date` date NOT NULL,
  `end_time` time NOT NULL,
  `charges` float(10,2) NOT NULL DEFAULT '0.00',
  `status` enum('PENDING','CONFIRM','REJECTED','FINISHED') COLLATE utf8mb4_unicode_ci DEFAULT 'PENDING',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `provider_id`, `client_id`, `start_date`, `start_time`, `end_date`, `end_time`, `charges`, `status`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 2, 7, '2017-12-12', '10:10:00', '2017-12-12', '12:10:00', 0.00, 'PENDING', '2017-08-23 11:12:41', '2017-08-23 11:12:41', 0),
(2, 3, 7, '2017-08-23', '12:40:19', '2017-08-23', '14:40:19', 1728.00, 'PENDING', '2017-08-23 11:13:26', '2017-09-14 08:00:05', 0),
(3, 11, 6, '2017-08-23', '14:20:15', '2017-08-23', '15:20:15', 27.00, 'PENDING', '2017-08-23 12:50:20', '2017-09-14 08:00:05', 0),
(4, 3, 6, '2017-08-23', '14:26:51', '2017-08-23', '15:26:51', 864.00, 'PENDING', '2017-08-23 12:56:56', '2017-09-14 08:00:05', 0),
(5, 1, 5, '2017-08-23', '17:40:24', '2017-08-23', '18:40:24', 48.60, 'REJECTED', '2017-08-24 01:41:10', '2017-09-14 08:00:05', 0),
(6, 1, 5, '2017-08-24', '17:44:25', '2017-08-24', '21:44:25', 194.40, 'PENDING', '2017-08-24 01:44:38', '2017-09-14 08:00:05', 0),
(7, 1, 5, '2017-08-24', '18:44:55', '2017-08-24', '20:44:55', 97.20, 'CONFIRM', '2017-08-24 01:45:01', '2017-09-14 08:00:05', 0),
(8, 1, 2, '2017-08-24', '01:24:59', '2017-08-24', '04:24:59', 145.80, 'PENDING', '2017-08-24 09:25:10', '2017-09-14 08:00:05', 0),
(9, 1, 15, '2017-08-24', '11:55:00', '2017-08-24', '13:55:00', 97.20, 'PENDING', '2017-08-24 10:25:29', '2017-09-14 08:00:05', 0),
(10, 1, 15, '2017-08-24', '11:58:03', '2017-08-24', '12:58:03', 48.60, 'PENDING', '2017-08-24 10:28:27', '2017-09-14 08:00:05', 0),
(11, 14, 22, '2017-08-24', '12:50:50', '2017-08-24', '17:50:50', 1264863.62, 'PENDING', '2017-08-24 11:20:53', '2017-09-14 08:00:05', 0),
(12, 3, 22, '2017-08-24', '12:55:54', '2017-08-24', '15:55:54', 2592.00, 'PENDING', '2017-08-24 11:26:02', '2017-09-14 08:00:05', 0),
(13, 3, 22, '2017-08-24', '12:56:56', '2017-08-24', '14:56:56', 1728.00, 'PENDING', '2017-08-24 11:27:00', '2017-09-14 08:00:05', 0),
(14, 3, 22, '2017-08-24', '12:59:26', '2017-08-24', '15:59:26', 2592.00, 'PENDING', '2017-08-24 11:29:33', '2017-09-14 08:00:05', 0),
(15, 23, 10, '2017-08-24', '14:02:42', '2017-08-24', '18:02:42', 19634.40, 'PENDING', '2017-08-24 12:32:59', '2017-09-14 08:00:05', 0),
(16, 23, 10, '2017-08-24', '14:58:51', '2017-08-24', '18:58:51', 19634.40, 'PENDING', '2017-08-24 13:28:58', '2017-09-14 08:00:05', 0),
(17, 24, 22, '2017-08-24', '18:08:27', '2017-08-24', '21:08:27', 103.68, 'PENDING', '2017-08-24 16:38:51', '2017-09-14 08:00:05', 0),
(18, 24, 22, '2017-08-24', '19:12:45', '2017-08-24', '21:12:45', 69.12, 'PENDING', '2017-08-24 17:42:52', '2017-09-14 08:00:05', 0),
(19, 23, 22, '2017-08-24', '19:17:03', '2017-08-24', '21:17:03', 9817.20, 'PENDING', '2017-08-24 17:47:11', '2017-09-14 08:00:05', 0),
(20, 24, 22, '2017-08-24', '19:23:58', '2017-08-24', '20:23:58', 34.56, 'PENDING', '2017-08-24 17:54:06', '2017-09-14 08:00:05', 0),
(21, 24, 22, '2017-08-24', '19:26:11', '2017-08-24', '20:26:11', 34.56, 'PENDING', '2017-08-24 17:56:20', '2017-09-14 08:00:05', 0),
(22, 3, 10, '2017-08-25', '13:49:46', '2017-08-25', '14:49:46', 864.00, 'PENDING', '2017-08-25 12:20:30', '2017-09-14 08:00:05', 0),
(23, 20, 10, '2017-08-25', '15:17:25', '2017-08-25', '17:17:25', 5672.16, 'PENDING', '2017-08-25 13:47:32', '2017-09-14 08:00:05', 0),
(24, 23, 25, '2017-08-25', '14:29:44', '2017-08-25', '16:29:44', 9817.20, 'PENDING', '2017-08-25 22:30:30', '2017-09-14 08:00:05', 0),
(25, 19, 26, '2017-09-05', '20:09:57', '2017-09-05', '21:09:57', 599.40, 'PENDING', '2017-09-05 18:38:19', '2017-09-14 08:00:05', 0),
(26, 3, 26, '2017-09-05', '20:12:24', '2017-09-05', '21:12:24', 864.00, 'PENDING', '2017-09-05 18:40:50', '2017-09-14 08:00:05', 0),
(27, 3, 6, '2017-09-06', '14:05:07', '2017-09-06', '17:05:07', 2592.00, 'PENDING', '2017-09-06 12:35:16', '2017-09-14 08:00:05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cancelation_history`
--

CREATE TABLE `cancelation_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `canceled_by` int(10) UNSIGNED NOT NULL,
  `charge_amount` float(10,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_charge_applicable` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cancelation_history`
--

INSERT INTO `cancelation_history` (`id`, `booking_id`, `canceled_by`, `charge_amount`, `created_at`, `updated_at`, `is_charge_applicable`, `is_active`) VALUES
(1, 5, 1, 0.00, '2017-08-24 01:55:27', '2017-08-24 01:55:27', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card_detail`
--

CREATE TABLE `card_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_cvv` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `card_detail`
--

INSERT INTO `card_detail` (`id`, `user_id`, `first_name`, `card_number`, `card_cvv`, `is_active`, `updated_at`, `created_at`) VALUES
(1, 2, 'abdallah haji', '472409013484', '878', 1, NULL, NULL),
(2, 4, 'john', '86868686868356656', '252', 1, NULL, NULL),
(3, 5, 'ftggdd', '21212112121212121', '111', 1, NULL, NULL),
(4, 6, 'dfghjklkjhgfdfghjkjhgfdfg', '4111111111111111', '234', 1, '2017-09-06 08:36:02', NULL),
(5, 10, 'Zack', '686868686686855', '886', 1, NULL, NULL),
(6, 16, 'sa', '34324322332342342', '234', 1, NULL, NULL),
(7, 15, 'ewe adds', '32424242342342342', '234', 1, NULL, NULL),
(8, 22, 'hjkdghs', '4111111111111111', '123', 1, '2017-08-24 13:50:19', NULL),
(9, 25, 'Eldhose', '12121212112222222', '111', 1, NULL, NULL),
(10, 26, 'sukhmander', '4111111111111111', '345', 1, '2017-09-05 14:39:56', NULL),
(11, 27, 'anuj', '41111111111111', '124', 1, NULL, NULL),
(12, 30, 'ASASDASASDASDASDASDASDASD', '32312312312312312', '123', 1, NULL, NULL),
(13, 31, 'test client', '123243545635', '234', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chat_head`
--

CREATE TABLE `chat_head` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_one` int(10) DEFAULT NULL,
  `sender_two` int(10) DEFAULT NULL,
  `firebase_identifier` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `chat_head`
--

INSERT INTO `chat_head` (`id`, `sender_one`, `sender_two`, `firebase_identifier`) VALUES
(1, 6, 3, '82de16021ed26a8df7992e07307a6da7'),
(2, 10, 3, 'ca65285430a2fb23dbb11ba46f13775f'),
(3, 5, 1, '95d24449c38019af23bee81dcc81a80d'),
(4, 2, 1, '2b0ce74a0289c78eb4934aac6587c232'),
(5, 22, 11, '333461f692c8e948d095f7d9ae112d4d'),
(6, 23, 10, 'c920f22d018b32d984ba8af1923af164'),
(7, 22, 24, '2006353c66854cc5345c13b0104843a4');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(10) UNSIGNED NOT NULL,
  `content_type` enum('ABOUT','TERMS','HELP','POLICY') COLLATE utf8_bin DEFAULT 'ABOUT',
  `content` text COLLATE utf8_bin,
  `is_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `content_type`, `content`, `is_active`) VALUES
(1, 'ABOUT', 'About page content for Bookmii', 1),
(2, 'HELP', 'About Bookmii Help', 1),
(3, 'TERMS', 'About term and condition', 1),
(4, 'POLICY', 'Cancelation policy', 1);

-- --------------------------------------------------------

--
-- Table structure for table `favourite`
--

CREATE TABLE `favourite` (
  `id` int(10) UNSIGNED NOT NULL,
  `provider_id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favourite`
--

INSERT INTO `favourite` (`id`, `provider_id`, `client_id`, `is_active`) VALUES
(1, 3, 8, 1),
(2, 4, 8, 1),
(3, 5, 8, 1),
(4, 7, 8, 1),
(5, 1, 2, 1),
(6, 3, 4, 1),
(7, 3, 5, 0),
(8, 1, 6, 0),
(9, 1, 7, 0),
(10, 3, 7, 0),
(11, 32, 7, 1),
(12, 11, 15, 1),
(13, 1, 5, 0),
(14, 24, 22, 1),
(15, 3, 10, 1),
(16, 20, 10, 0),
(17, 14, 10, 0),
(18, 1, 25, 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2017_05_14_064428_create_services_table', 1),
(14, '2017_05_14_064429_create_profile_table', 1),
(15, '2017_05_14_064447_create_review_table', 1),
(16, '2017_05_14_064517_create_favourite_table', 1),
(17, '2017_05_14_064533_create_booking_table', 1),
(18, '2017_05_14_064843_create_service_images_table', 1),
(19, '2017_05_14_065638_create_cancelation_history_table', 1),
(20, '2017_05_18_134020_create_account_detail_table', 2),
(21, '2017_05_18_134034_create_card_detail_table', 2),
(22, '2017_07_04_095217_create_payment_history_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `notification` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `notification`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1),
(14, 14, 1),
(15, 15, 1),
(16, 16, 1),
(17, 17, 1),
(18, 18, 1),
(19, 19, 1),
(20, 20, 1),
(21, 21, 1),
(22, 22, 1),
(23, 23, 1),
(24, 24, 1),
(25, 25, 1),
(26, 26, 1),
(27, 27, 1),
(28, 28, 1),
(29, 29, 1),
(30, 30, 1),
(31, 31, 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification_token`
--

CREATE TABLE `notification_token` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `firebase_token` longtext COLLATE utf8_bin,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `notification_token`
--

INSERT INTO `notification_token` (`id`, `user_id`, `firebase_token`, `updated_at`, `created_at`) VALUES
(1, 1, 'cRm6Qwcp5mA:APA91bH7e5W2M0eswyrsWy0SIRgJDu7OyRNqXQeHtdoM1JdsiEcDukgXaChJS6M9Peype7clfd8vbrd3pv8OMly4WlSgqwVrKGCmVaHh5-poWl2PG5c1x3DnxrZJzJxRVl65knTk6udI', '2017-08-25 18:25:10', NULL),
(2, 2, 'fS5jaqK4Ro4:APA91bFm00pL1ks_gZdcGL1qxn3cKukNiRIOjYAXo1px6eMeHg5caR0wRv44K4haMEGwtZam9SclVIAbNhXk7oyySE7JW2R0Hqw8PQdW5GhwMb_k30DTbihXBobfd1bY5Ub6TRp5aDy9', '2017-08-24 05:23:38', NULL),
(3, 3, 'eERO5u2Fd3g:APA91bGQ-Vp3qRFvOSEqj7VeQyp3chbElwJiLck__LoGGBPP8IAX0chBTR40QnL_S5sESIvQUtYalz1K8RyI03QUXrmXAqD7xd_PPbaK2Iwhs8oI7Ehq9nh_zt1KRvl_2Hq_7TxoUtcI', NULL, NULL),
(4, 4, 'eERO5u2Fd3g:APA91bGQ-Vp3qRFvOSEqj7VeQyp3chbElwJiLck__LoGGBPP8IAX0chBTR40QnL_S5sESIvQUtYalz1K8RyI03QUXrmXAqD7xd_PPbaK2Iwhs8oI7Ehq9nh_zt1KRvl_2Hq_7TxoUtcI', NULL, NULL),
(5, 5, 'dzlvKoVkD9U:APA91bHzIYcGwpnj5OFZbpLY3LUWhQFCfSK5aZju2BthqW9iNT4k1qmR03z0FLdS-BJQYe9YaThVJDt1F-4AIR3D-qCwxgr_xY2h20836HPWX_9cYL8V8H8WSo2NhtboRaSNCQI5nvbq', '2017-08-23 21:39:53', NULL),
(6, 6, 'esXh26BOHiA:APA91bGB6Y7jOGjQIssSYiS9vGpwcRCvvYxuyBlzjLrRNrvwe97DeffxlNY-8Xx0aWKZclXl9luBqZddqCK4RWuJOit94d__g4KByR1YCxipgtHJBDX7JBExihsduQOnwdGYmfMXt_hR', '2017-09-06 08:13:31', NULL),
(7, 7, 'cHtzbOtmRNA:APA91bGgrZ5UWWFa9bZHuayfLg3zptIqX7ckkA-tCFhva6JkqcbvSpevJaRjM2fEf_7ILTXSbxhha0CXvX98QllNh21qARdeiKWG2OE8HU81yOrs3hnPFoMQKVlnvDGOms6AWTltzvIP', NULL, NULL),
(8, 8, 'eERO5u2Fd3g:APA91bGQ-Vp3qRFvOSEqj7VeQyp3chbElwJiLck__LoGGBPP8IAX0chBTR40QnL_S5sESIvQUtYalz1K8RyI03QUXrmXAqD7xd_PPbaK2Iwhs8oI7Ehq9nh_zt1KRvl_2Hq_7TxoUtcI', NULL, NULL),
(9, 9, 'eERO5u2Fd3g:APA91bGQ-Vp3qRFvOSEqj7VeQyp3chbElwJiLck__LoGGBPP8IAX0chBTR40QnL_S5sESIvQUtYalz1K8RyI03QUXrmXAqD7xd_PPbaK2Iwhs8oI7Ehq9nh_zt1KRvl_2Hq_7TxoUtcI', NULL, NULL),
(10, 10, 'efcmWyv7_2w:APA91bFGRaj2J_9BTry3f5MagBRB9yaoXmztmktxD0Ev-CsA59OJSMlPNaSs7FeH_ndxG7Oe6toZ_6eByMmO8hwmOVHR1CFZNb4-jYPP8H74P8Bql4nA1qktrRVyeFVqNdIud5t70aha', '2017-08-25 08:12:29', NULL),
(11, 11, 'cHtzbOtmRNA:APA91bGgrZ5UWWFa9bZHuayfLg3zptIqX7ckkA-tCFhva6JkqcbvSpevJaRjM2fEf_7ILTXSbxhha0CXvX98QllNh21qARdeiKWG2OE8HU81yOrs3hnPFoMQKVlnvDGOms6AWTltzvIP', NULL, NULL),
(12, 12, 'cHtzbOtmRNA:APA91bGgrZ5UWWFa9bZHuayfLg3zptIqX7ckkA-tCFhva6JkqcbvSpevJaRjM2fEf_7ILTXSbxhha0CXvX98QllNh21qARdeiKWG2OE8HU81yOrs3hnPFoMQKVlnvDGOms6AWTltzvIP', NULL, NULL),
(13, 13, 'cHtzbOtmRNA:APA91bGgrZ5UWWFa9bZHuayfLg3zptIqX7ckkA-tCFhva6JkqcbvSpevJaRjM2fEf_7ILTXSbxhha0CXvX98QllNh21qARdeiKWG2OE8HU81yOrs3hnPFoMQKVlnvDGOms6AWTltzvIP', NULL, NULL),
(14, 14, 'cHtzbOtmRNA:APA91bGgrZ5UWWFa9bZHuayfLg3zptIqX7ckkA-tCFhva6JkqcbvSpevJaRjM2fEf_7ILTXSbxhha0CXvX98QllNh21qARdeiKWG2OE8HU81yOrs3hnPFoMQKVlnvDGOms6AWTltzvIP', NULL, NULL),
(15, 15, 'cHtzbOtmRNA:APA91bGgrZ5UWWFa9bZHuayfLg3zptIqX7ckkA-tCFhva6JkqcbvSpevJaRjM2fEf_7ILTXSbxhha0CXvX98QllNh21qARdeiKWG2OE8HU81yOrs3hnPFoMQKVlnvDGOms6AWTltzvIP', '2017-08-24 05:50:27', NULL),
(16, 16, 'cHtzbOtmRNA:APA91bGgrZ5UWWFa9bZHuayfLg3zptIqX7ckkA-tCFhva6JkqcbvSpevJaRjM2fEf_7ILTXSbxhha0CXvX98QllNh21qARdeiKWG2OE8HU81yOrs3hnPFoMQKVlnvDGOms6AWTltzvIP', NULL, NULL),
(17, 17, 'cHtzbOtmRNA:APA91bGgrZ5UWWFa9bZHuayfLg3zptIqX7ckkA-tCFhva6JkqcbvSpevJaRjM2fEf_7ILTXSbxhha0CXvX98QllNh21qARdeiKWG2OE8HU81yOrs3hnPFoMQKVlnvDGOms6AWTltzvIP', NULL, NULL),
(18, 18, 'cHtzbOtmRNA:APA91bGgrZ5UWWFa9bZHuayfLg3zptIqX7ckkA-tCFhva6JkqcbvSpevJaRjM2fEf_7ILTXSbxhha0CXvX98QllNh21qARdeiKWG2OE8HU81yOrs3hnPFoMQKVlnvDGOms6AWTltzvIP', NULL, NULL),
(19, 19, 'cHtzbOtmRNA:APA91bGgrZ5UWWFa9bZHuayfLg3zptIqX7ckkA-tCFhva6JkqcbvSpevJaRjM2fEf_7ILTXSbxhha0CXvX98QllNh21qARdeiKWG2OE8HU81yOrs3hnPFoMQKVlnvDGOms6AWTltzvIP', NULL, NULL),
(20, 20, 'cQAuhpGRIS0:APA91bE-_MjTlC9akoVXPVC8VI-9GmpioOy5CuzheGHCchLD89Ps3XbQYO5JcnMQJozAF8E_6fzbWh3-uxSMf9qCi-8ReLajhNAHedw29YeimGM5DxC_9lzipxALRCru3WO8UmEyVbMa', '2017-09-06 13:49:24', NULL),
(21, 21, 'cHtzbOtmRNA:APA91bGgrZ5UWWFa9bZHuayfLg3zptIqX7ckkA-tCFhva6JkqcbvSpevJaRjM2fEf_7ILTXSbxhha0CXvX98QllNh21qARdeiKWG2OE8HU81yOrs3hnPFoMQKVlnvDGOms6AWTltzvIP', NULL, NULL),
(22, 22, 'cQAuhpGRIS0:APA91bE-_MjTlC9akoVXPVC8VI-9GmpioOy5CuzheGHCchLD89Ps3XbQYO5JcnMQJozAF8E_6fzbWh3-uxSMf9qCi-8ReLajhNAHedw29YeimGM5DxC_9lzipxALRCru3WO8UmEyVbMa', '2017-09-06 10:31:00', NULL),
(23, 23, 'deSkgcoqYdA:APA91bEeJ3nGJtXI3NO24ZhW8zPenKkoXWOE6LuLCPhXqH1UYpESicnqsn77jxcku9HP3VVOiM_R-Z7yu3NTtAYgFDID0mZQ6KQaDUuxzfHcbcwhbPOht8JLXbFQVacxJDmz1wxOImaS', NULL, NULL),
(24, 24, 'deSkgcoqYdA:APA91bEeJ3nGJtXI3NO24ZhW8zPenKkoXWOE6LuLCPhXqH1UYpESicnqsn77jxcku9HP3VVOiM_R-Z7yu3NTtAYgFDID0mZQ6KQaDUuxzfHcbcwhbPOht8JLXbFQVacxJDmz1wxOImaS', NULL, NULL),
(25, 25, 'cRm6Qwcp5mA:APA91bH7e5W2M0eswyrsWy0SIRgJDu7OyRNqXQeHtdoM1JdsiEcDukgXaChJS6M9Peype7clfd8vbrd3pv8OMly4WlSgqwVrKGCmVaHh5-poWl2PG5c1x3DnxrZJzJxRVl65knTk6udI', NULL, NULL),
(26, 26, 'e6yuvZWI-l8:APA91bF3JW0DhGpTVaBSRfCPNqAk8XO6K5r0AVsvDLm5QYTNrI4_qfqtFsoM-_Q1bo1-rqsDV08pxftGatZv23T3amgl54wVsraJgYIqP-riCrrmRjwv8rv9dW0HUZfY7Dc1mGAazogx', '2017-09-07 14:09:40', NULL),
(27, 27, 'cQAuhpGRIS0:APA91bE-_MjTlC9akoVXPVC8VI-9GmpioOy5CuzheGHCchLD89Ps3XbQYO5JcnMQJozAF8E_6fzbWh3-uxSMf9qCi-8ReLajhNAHedw29YeimGM5DxC_9lzipxALRCru3WO8UmEyVbMa', NULL, NULL),
(28, 28, 'dyidrmrIh4E:APA91bHDhajVHrdTbjcHjW_uPpiu69ZMWrjatOAOJ0p7mGlrhxjdmharhpE7OaONsvyMEbyhBNqSa2kOOq6o6CFUQLzPb5UPbe8UqL9As_9BB1oEDY6hMnHs0qb8IuEv6qmLDGfmwamW', NULL, NULL),
(29, 29, 'dyidrmrIh4E:APA91bHDhajVHrdTbjcHjW_uPpiu69ZMWrjatOAOJ0p7mGlrhxjdmharhpE7OaONsvyMEbyhBNqSa2kOOq6o6CFUQLzPb5UPbe8UqL9As_9BB1oEDY6hMnHs0qb8IuEv6qmLDGfmwamW', NULL, NULL),
(30, 30, 'dyidrmrIh4E:APA91bHDhajVHrdTbjcHjW_uPpiu69ZMWrjatOAOJ0p7mGlrhxjdmharhpE7OaONsvyMEbyhBNqSa2kOOq6o6CFUQLzPb5UPbe8UqL9As_9BB1oEDY6hMnHs0qb8IuEv6qmLDGfmwamW', NULL, NULL),
(31, 31, 'dFaB64C3D4U:APA91bGA8XHmW2h5pU9GDFaMwXVMJZyLdcUyFajiwZm5GvzTAiH5MkwJxHalo6KZCwGBP48jYFPcmgSwzbxnQLOW7v9lKqecqlHHCF7PyjQ7H0kN-45GcdYbnUAWZADac3SMXDe5nIOl', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_history`
--

CREATE TABLE `payment_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `payment_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_history`
--

INSERT INTO `payment_history` (`id`, `booking_id`, `user_id`, `amount`, `payment_token`, `created_at`, `updated_at`) VALUES
(1, 3, 6, 25, 'h0tbppa3', NULL, NULL),
(2, 4, 6, 800, '9em4dfg3', NULL, NULL),
(3, 5, 5, 45, 'dcbkb1pp', NULL, NULL),
(4, 7, 5, 45, 'k26h0wnj', NULL, NULL),
(5, 8, 2, 45, 'q0rqtfr7', NULL, NULL),
(6, 16, 10, 4545, 'nwj2g8wn', NULL, NULL),
(7, 17, 22, 32, 'a90k7e6x', NULL, NULL),
(8, 18, 22, 32, '3chxzw0p', NULL, NULL),
(9, 18, 22, 32, 'pcqpnrj0', NULL, NULL),
(10, 19, 22, 4545, 'amn3f4ds', NULL, NULL),
(11, 19, 22, 4545, 'h0vh6hha', NULL, NULL),
(12, 19, 22, 4545, 'a2dvddvm', NULL, NULL),
(13, 20, 22, 32, '2ahnk4nh', NULL, NULL),
(14, 21, 22, 32, 'p6zxfk1k', NULL, NULL),
(15, 25, 26, 555, 'n9yjfj1y', NULL, NULL),
(16, 27, 6, 800, 'jaqsmhak', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charges` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `user_id`, `service_id`, `about`, `dob`, `charges`, `facebook`, `instagram`, `youtube`, `twitter`, `address`, `latitude`, `longitude`, `is_active`, `updated_at`, `created_at`) VALUES
(1, 1, 2, 'Hhshsjd hsjjsjs', NULL, '45', NULL, NULL, NULL, NULL, 'Toronto, Toronto, Yorkville, M5R, ON, Canada', '43.67093118345012', '-79.38979594748032', 1, '2017-08-21 19:04:02', NULL),
(2, 2, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Toronto, Toronto, Banbury-Don Mills, M3C 3M8, ON, Canada', '43.73008010520837', '-79.32733001266307', 1, '2017-08-24 05:27:41', NULL),
(3, 3, 2, 'Great photographer', NULL, '800', NULL, NULL, NULL, NULL, 'Gautambudh Nagar, Noida, Sector 63, 201301, Uttar Pradesh, India', '28.63019061640323', '77.38177066954529', 1, '2017-08-22 09:45:30', NULL),
(4, 4, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Gautambudh Nagar, Noida, Sector 63, 201301, Uttar Pradesh, India', '28.63011853702206', '77.3817935143239', 1, '2017-08-22 09:52:48', NULL),
(5, 5, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Toronto, Toronto, Yorkville, M5R 3K4, ON, Canada', '43.670969582815', '-79.38970706081763', 1, '2017-08-22 14:47:55', NULL),
(6, 6, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Mumbai, Mumbai, Matunga East, 400019, Maharashtra, India', '19.017615', '72.8561644', 1, '2017-09-06 08:36:02', NULL),
(7, 7, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Mumbai, Mumbai, Matunga East, 400019, Maharashtra, India', '19.017615', '72.8561644', 1, '2017-08-22 19:36:41', NULL),
(8, 8, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Gautambudh Nagar, Noida, Sector 63, 201301, Uttar Pradesh, India', '28.63004547648735', '77.38180405723286', 1, '2017-08-23 05:36:27', NULL),
(9, 9, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Gautambudh Nagar, Noida, Sector 63, 201301, Uttar Pradesh, India', '28.63004547648735', '77.38180405723286', 1, NULL, NULL),
(10, 10, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Gautambudh Nagar, Noida, Sector 63, 201301, Uttar Pradesh, India', '28.63021978433079', '77.38172058949523', 1, '2017-08-25 08:18:45', NULL),
(11, 11, 2, 'Kwdhiqufe', NULL, '25', NULL, NULL, NULL, NULL, 'Mumbai, Mumbai, Matunga East, 400019, Maharashtra, India', '19.017615', '72.8561644', 1, '2017-08-23 08:20:37', NULL),
(12, 12, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Mumbai, Mumbai, Matunga East, 400019, Maharashtra, India', '19.017615', '72.8561644', 1, '2017-08-23 08:34:50', NULL),
(13, 13, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Mumbai, Mumbai, Matunga East, 400019, Maharashtra, India', '19.017615', '72.8561644', 1, '2017-08-23 08:39:55', NULL),
(14, 14, 4, 'Asdasdasd', NULL, '234234', NULL, NULL, NULL, NULL, 'Mumbai, Mumbai, Matunga East, 400019, Maharashtra, India', '19.017615', '72.8561644', 1, '2017-08-23 08:49:42', NULL),
(15, 16, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Mumbai, Mumbai, Matunga East, 400019, Maharashtra, India', '19.017615', '72.8561644', 1, '2017-08-23 10:19:00', NULL),
(16, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-08-24 06:09:42', NULL),
(17, 17, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Mumbai, Mumbai, Matunga East, 400019, Maharashtra, India', '19.017615', '72.8561644', 1, '2017-08-23 15:25:42', NULL),
(18, 18, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Mumbai, Mumbai, Matunga East, 400019, Maharashtra, India', '19.017615', '72.8561644', 1, '2017-08-23 15:29:21', NULL),
(19, 19, 2, 'Hhhh', NULL, '555', NULL, NULL, NULL, NULL, 'Mumbai, Mumbai, Matunga East, 400019, Maharashtra, India', '19.017615', '72.8561644', 1, '2017-08-23 15:32:45', NULL),
(20, 20, 3, 'Hsjsjs', NULL, '580', NULL, NULL, NULL, NULL, 'San Francisco, San Francisco, Union Square, 94108, CA, United States', '37.785834', '-122.406417', 1, '2017-09-06 13:50:49', NULL),
(21, 21, 3, 'Jkhgk', NULL, '546456', NULL, NULL, NULL, NULL, 'Mumbai, Mumbai, Matunga East, 400019, Maharashtra, India', '19.017615', '72.8561644', 1, '2017-08-24 06:13:05', NULL),
(22, 22, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Gautambudh Nagar, Noida, Sector 63, 201301, Uttar Pradesh, India', '28.628454', '77.376945', 1, '2017-08-24 13:50:19', NULL),
(23, 23, 2, 'Adscsdvgbreber', NULL, '4545', NULL, NULL, NULL, NULL, 'Gautambudh Nagar, Noida, Sector 63, 201301, Uttar Pradesh, India', '28.628454', '77.376945', 1, '2017-08-24 08:31:05', NULL),
(24, 24, 2, 'Fgergerg', NULL, '32', NULL, NULL, NULL, NULL, 'Gautambudh Nagar, Noida, Sector 63, 201301, Uttar Pradesh, India', '28.628454', '77.376945', 1, '2017-08-24 14:01:36', NULL),
(25, 25, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Toronto, Toronto, Yorkville, M5R, ON, Canada', '43.67092735834922', '-79.38981380434099', 1, '2017-08-25 18:28:47', NULL),
(26, 26, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'San Francisco, San Francisco, Union Square, 94108, CA, United States', '37.785834', '-122.406417', 1, '2017-09-05 14:39:56', NULL),
(27, 27, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, 'Gautambudh Nagar, Noida, Sector 63, 201301, Uttar Pradesh, India', '28.63008677112468', '77.38177292812718', 1, '2017-09-06 10:21:13', NULL),
(28, 28, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-13 06:53:52', NULL),
(29, 29, 5, 'ASAasasaS', NULL, '2131', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-13 07:42:56', NULL),
(30, 30, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-13 07:43:59', NULL),
(31, 31, 1, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-09-13 09:40:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `receiver_id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `rating` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service`, `is_active`) VALUES
(1, 'NONE', 1),
(2, 'PHOTOGRAPHER', 1),
(3, 'MUA', 1),
(4, 'INFLUENCER', 1),
(5, 'MODEL', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_images`
--

CREATE TABLE `service_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile_id` int(10) UNSIGNED NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_Active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_images`
--

INSERT INTO `service_images` (`id`, `profile_id`, `image_path`, `is_Active`) VALUES
(1, 1, 'http://bookmii.apphosthub.com/public/images/uploads/service_599b2db32f116.png', 1),
(2, 1, 'http://bookmii.apphosthub.com/public/images/uploads/service_599b2db32f213.png', 1),
(3, 3, 'http://bookmii.apphosthub.com/public/images/uploads/service_599bfbff602b0.png', 1),
(4, 3, 'http://bookmii.apphosthub.com/public/images/uploads/service_599bfbff609a1.png', 1),
(5, 7, 'http://bookmii.apphosthub.com/public/images/uploads/service_599c87c96960a.png', 1),
(6, 7, 'http://bookmii.apphosthub.com/public/images/uploads/service_599c87c969704.png', 1),
(7, 11, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d39fac4de8.png', 1),
(8, 11, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d39fac4ed6.png', 1),
(9, 12, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d3e2a623b7.png', 1),
(10, 12, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d3e2a6246c.png', 1),
(11, 13, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d3f5b06019.png', 1),
(12, 13, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d3f5b060ff.png', 1),
(13, 14, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d40e6c194a.png', 1),
(14, 14, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d40e6c1a1d.png', 1),
(15, 17, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d9e7622a4e.png', 1),
(16, 17, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d9e7622b4b.png', 1),
(17, 18, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d9f51d3cbc.png', 1),
(18, 18, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d9f51d3d81.png', 1),
(19, 19, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d9ff60a62f.png', 1),
(20, 19, 'http://bookmii.apphosthub.com/public/images/uploads/service_599d9ff60a6f0.png', 1),
(44, 20, 'http://bookmii.apphosthub.com/public/images/uploads/service_59affd22c0602.png', 1),
(43, 20, 'http://bookmii.apphosthub.com/public/images/uploads/service_59affd22c02f2.png', 1),
(23, 21, 'http://bookmii.apphosthub.com/public/images/uploads/service_599e6e1d9c801.png', 1),
(24, 21, 'http://bookmii.apphosthub.com/public/images/uploads/service_599e6e1d9c8c5.png', 1),
(25, 23, 'http://bookmii.apphosthub.com/public/images/uploads/service_599e8e8d78b40.png', 1),
(26, 23, 'http://bookmii.apphosthub.com/public/images/uploads/service_599e8e8d78c5b.png', 1),
(46, 29, 'http://bookmii.apphosthub.com/public/images/uploads/service_59b8e12485ed6.png', 1),
(45, 29, 'http://bookmii.apphosthub.com/public/images/uploads/service_59b8e12485df9.png', 1),
(42, 24, 'http://bookmii.apphosthub.com/public/images/uploads/service_599edc4030bdf.png', 1),
(41, 24, 'http://bookmii.apphosthub.com/public/images/uploads/service_599edc4030ac9.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci,
  `facebook_token` text COLLATE utf8mb4_unicode_ci,
  `type` enum('customer','provider') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_stage` int(10) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `display_name`, `email`, `device_token`, `password`, `profile_image`, `api_token`, `facebook_token`, `type`, `remember_token`, `registration_stage`, `created_at`, `updated_at`) VALUES
(1, 'Abdallah Haji', 'Abdallah Haji', 'abdallahhaji@gmail.com', '158a8d19a42e45fefc8e5e0f67fb6ceaf5b84ffe21fa952e32175b315afcd5d0', '$2y$10$cs5oLBUfe7GkPKrcYqsYZO.lM.YsEDhjqe60ojZmLHsmNEth2OlKG', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/20292808_10155328557720552_1818191794219134365_n.jpg?oh=5cc05bc0c3d7a2cc885c3ee689bfd14b&oe=5A2A3C21', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly9ib29rbWlpLmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL3JlZ2lzdGVyIiwiaWF0IjoxNTAzNjg1NTEwLCJleHAiOjE1MDM5MDE1MTAsIm5iZiI6MTUwMzY4NTUxMCwianRpIjoiQnFjY3A0WHFDbzJEbmw1biJ9.PB-y9OXYuB9QMEGidxyu7wBU2vBgQ3bBMT7DlA5vs8M', '10155306406475552', 'provider', NULL, 1, '2017-08-21 22:59:01', '2017-08-25 22:25:10'),
(2, 'Vic anand', 'Vic', 'abdallahhaji@test.com', '158a8d19a42e45fefc8e5e0f67fb6ceaf5b84ffe21fa952e32175b315afcd5d0', '$2y$10$c2WdhGz8AG.wFsLPc0xZAOLyXgEpCCiCigTzAM0EI4k2L9JA9YC56', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599e63cdf2dfb.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6Ly9ib29rbWlpLmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL2xvZ2luIiwiaWF0IjoxNTAzNTUyMjE4LCJleHAiOjE1MDM3NjgyMTgsIm5iZiI6MTUwMzU1MjIxOCwianRpIjoiOTVIR3V0b0tLNm9rb082TyJ9.-o7UzU7ChY3DXsNEI6cLxsICfekXpElOWziWUPIkX1M', NULL, 'customer', NULL, 1, '2017-08-22 02:01:09', '2017-08-24 09:27:41'),
(3, 'Peter Parker', 'Peter Parker', 'apptesting2003@gmail.com', 'd324e9b10f8ee4cfc539f511d188dc84de8de7f2281840eaae20a5d500f4b3da', '$2y$10$SEDgbRrxdPSUPL1vTL3i5uf6HK.bszpNEaKJgPUvelEp0KzRE771W', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/17098238_258629591260269_5949461518085072908_n.jpg?oh=def0e04cbe9c152eff0c17e9879cd845&oe=5A2E907E', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMsImlzcyI6Imh0dHA6Ly9ib29rbWlpLmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL3JlZ2lzdGVyIiwiaWF0IjoxNTAzNDY4Mjg1LCJleHAiOjE1MDM2ODQyODUsIm5iZiI6MTUwMzQ2ODI4NSwianRpIjoiYXpDUTVOUDc1Z2tzZWJQSiJ9.fcD5M0QGJZMgZ8QPo1xfNXzSIyNOC-6Fbg12l43feTk', '344867412636486', 'provider', NULL, 1, '2017-08-22 13:37:41', '2017-08-23 10:04:45'),
(4, 'John', 'Smith', 'john@gmail.com', 'd324e9b10f8ee4cfc539f511d188dc84de8de7f2281840eaae20a5d500f4b3da', '$2y$10$39TVyPVFUF/q5cZd3iKl0eUaqMfeC9N9M8d.9boqdyz5UFZnBQO.G', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599bfedfdae60.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQsImlzcyI6Imh0dHA6Ly9ib29rbWlpLmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL2xvZ2luIiwiaWF0IjoxNTAzMzk1NTA3LCJleHAiOjE1MDM2MTE1MDcsIm5iZiI6MTUwMzM5NTUwNywianRpIjoiekJvMmpEbVVPNHlkblZ3aCJ9.oFKKYDJ36BTqKZ4NtcPrinhLuyI0jGdfa7Gu4BafNTw', NULL, 'customer', NULL, 1, '2017-08-22 13:47:45', '2017-08-22 13:52:48'),
(5, 'Bob Dan', 'Bob Dan', 'danb90041@gmail.com', '3203833a6e96c2041eb7a0360cc22907415420d33df5f7cc6f6abe6e5b038455', '$2y$10$noG7Vl4BCEoYDueOIyjzc.J7y5aCUmaNBY0ZDQLJtO3oOX3YDzmYa', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/17952581_102052123692824_3304360229962278160_n.jpg?oh=fd817f9554497c56090ab92fdead8758&oe=5A16B245', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUsImlzcyI6Imh0dHA6Ly9ib29rbWlpLmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL3JlZ2lzdGVyIiwiaWF0IjoxNTAzNTI0MzkzLCJleHAiOjE1MDM3NDAzOTMsIm5iZiI6MTUwMzUyNDM5MywianRpIjoiSHRtNkc4aDZOT1o2WFdhNCJ9.Mz6CE9Q8vtNn8rbJoymWROCU34JTY5WWoWgfgLGpB0A', '128408114390558', 'customer', NULL, 1, '2017-08-22 18:47:25', '2017-08-24 01:39:53'),
(6, 'Sukhii Demo', 'Sukhii Demo', 'sukhmander121@yahoo.com', NULL, '$2y$10$r2JWK1Os1QG5d.VRQzYdUO4kZU80ZN5UXezdhTa/n9Rcg8DSLmnGC', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599d5260cc6c8.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjYsImlzcyI6Imh0dHA6Ly9ib29rbWlpLmFwcGhvc3RodWIuY29tL2FwaS92MS9hdXRoL3JlZ2lzdGVyIiwiaWF0IjoxNTAzNDc4MjA1LCJleHAiOjE1MDM2OTQyMDUsIm5iZiI6MTUwMzQ3ODIwNSwianRpIjoiUWNHOVhxb2RQNXNKSkNxZiJ9.0_XEfmfviTQ5b-kO33s835Il2f_4SP4uEftatDx9VOo', '1983048985300173', 'customer', NULL, 1, '2017-08-22 23:29:49', '2017-08-23 14:01:04'),
(7, 'Dfsdfsdfsd', 'Sdfsdfsdfsdf', 'sdfsdfsdf@sasdasdad.com', NULL, '$2y$10$pUtP4JnEtb1mYtxsYRsAJOtJJ.Jq7fmG.FjKIYQCyyKg58wOaBhLi', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599c87a562f10.png', NULL, NULL, 'customer', NULL, 1, '2017-08-22 23:35:50', '2017-08-22 23:36:41'),
(8, 'Allie', 'Allie', 'Allie@gmail.com', 'd324e9b10f8ee4cfc539f511d188dc84de8de7f2281840eaae20a5d500f4b3da', '$2y$10$hMJUECz0Gs4/uuJ8Y4v9hOpP2mmW.f1yto8nPPNrfcIl6LrlVY81a', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599d145b85ec5.png', NULL, NULL, 'customer', NULL, 0, '2017-08-23 09:36:10', '2017-08-23 09:36:27'),
(9, 'Allie', 'Allie', 'Alliee@gmail.com', 'd324e9b10f8ee4cfc539f511d188dc84de8de7f2281840eaae20a5d500f4b3da', '$2y$10$uY6ux4hu.MAmRGnEokuPS.VKl2Qd5nRDfTrF5c4s6qRD.kP6q3GlW', '', NULL, NULL, 'customer', NULL, 0, '2017-08-23 09:37:34', '2017-08-23 09:37:34'),
(10, 'Zack', 'Zack', 'zack@gmail.com', 'd324e9b10f8ee4cfc539f511d188dc84de8de7f2281840eaae20a5d500f4b3da', '$2y$10$oTdzyy6S7xoMgWtZh1L.augDOvYGZC0Vd/BsveLQjHGMUutKM/TCC', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599fdd65e88d2.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOi8vYm9va21paS5hcHBob3N0aHViLmNvbS9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwMzY0ODkwOCwiZXhwIjoxNTAzODY0OTA4LCJuYmYiOjE1MDM2NDg5MDgsImp0aSI6Ijh6TWlaU0QxWjRjT3lIYVUifQ._meziRPzE8J-8R2O6WW3NxfMXE7j6WiTW7ILzyhrRJU', NULL, 'customer', NULL, 1, '2017-08-23 10:06:03', '2017-08-25 12:18:45'),
(11, 'Qweqweqwrqqerq', 'Qwerqwewqeqw', 'qweqwe@asdad.com', NULL, '$2y$10$ZOt/P8kVTyLdGPufv4wgp.6S0oiqarbL8FV2ysehXR1U3jxw5KrPa', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599d39da3348c.png', NULL, NULL, 'provider', NULL, 1, '2017-08-23 12:16:11', '2017-08-23 12:20:37'),
(15, 'Sukhmander Singh', 'Sukhmander Singh', 'sukhmander121@hotmail.com', NULL, '$2y$10$.EswUKdtfzodMt1Jj/nyD.dHgnOFmI.OgUc730ENizqvNFRiDyDoy', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599e6da6b6a73.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE1LCJpc3MiOiJodHRwOi8vYm9va21paS5hcHBob3N0aHViLmNvbS9hcGkvdjEvYXV0aC9yZWdpc3RlciIsImlhdCI6MTUwMzU1Njc5NCwiZXhwIjoxNTAzNzcyNzk0LCJuYmYiOjE1MDM1NTY3OTQsImp0aSI6IkREbWtGVmZPNDRzNjJja08ifQ.b9oicdrwb_yhcKR7CIZZ8FqxXTjBuuorUSdqVaVc86c', '952589098178003', 'customer', NULL, 1, '2017-08-23 14:09:13', '2017-08-24 10:39:54'),
(12, 'Possible', 'Possible', 'possible@gmail.com', NULL, '$2y$10$hQfOLKeDis6l/55Agre4wOIBxn2bh45KFQfpvJ5WmAqlos5Dbsh8e', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599d3e044b955.png', NULL, NULL, 'customer', NULL, 1, '2017-08-23 12:34:00', '2017-08-23 12:34:50'),
(13, 'Asdasd', 'Asdasd', 'asdasd@asjef.com', NULL, '$2y$10$R.eyCn4huPZon2CEKLS2F.3nopCqAlXrZ2C0E/k6de.G6qHng/CgK', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599d3f397da38.png', NULL, NULL, 'customer', NULL, 1, '2017-08-23 12:39:10', '2017-08-23 12:39:55'),
(14, 'ZxZX', 'ZXZX', 'ZXZX@gmail.com', NULL, '$2y$10$YP88K8Erf1ls5LbyGI25bOxCwx9869dxGtsY7Ctzjlbl3G7j5MRKW', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599d40526068e.png', NULL, NULL, 'provider', NULL, 1, '2017-08-23 12:43:51', '2017-08-23 12:49:42'),
(16, 'Sfewrwer', 'Werwerwer', 'werwerew@g.com', NULL, '$2y$10$qdHp/lQltGvTFGLjQ9Pjmeocx/vxQ8bw6YNV.o.3q3e9rR8kA6pmq', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599d5687c4498.png', NULL, NULL, 'customer', NULL, 1, '2017-08-23 14:18:33', '2017-08-23 14:19:00'),
(21, 'Sdfsdfsdf', 'Sdfsdfsdf', 'sdfsdf@gmail.com', NULL, '$2y$10$MWtd/UINdFQ0PLfTexhpPe/s6TxaIZCx1kIW2H.VcNAw68BdFgN.e', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599e6e7165985.png', NULL, NULL, 'provider', NULL, 1, '2017-08-24 10:10:56', '2017-08-24 10:13:05'),
(17, 'Bnbnbnb', 'Bvvvvb', 'ggg@gg.com', NULL, '$2y$10$6u0wqgNTEZcR9xIgQf63oexMaCX9SZbcKr2dNaBoEs6dwf0qLgqVC', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599d9e51b85ca.png', NULL, NULL, 'customer', NULL, 1, '2017-08-23 19:24:50', '2017-08-23 19:25:42'),
(18, 'Gghghghghghgh', 'Bvbbvbvbv', 'ggggg@gg.com', NULL, '$2y$10$67wLB1m7lOHsAxFQW9k18.0ZTCsBz3McQJX9NXQh4NbchdmPgQfoS', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599d9f2dd717f.png', NULL, NULL, 'customer', NULL, 1, '2017-08-23 19:28:30', '2017-08-23 19:29:21'),
(19, 'Treat', 'Ghghgh', 'go@hhh.com', NULL, '$2y$10$iW6n3QepeBBfCk7.3pOaMe4VDMI6ehtfqWIrZ9kiLtIqCnSGKjZH2', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599d9fda1735c.png', NULL, NULL, 'provider', NULL, 1, '2017-08-23 19:31:26', '2017-08-23 19:32:45'),
(20, 'Provider', 'Provider', 'provider@gmail.com', NULL, '$2y$10$1CSkwKC3yyimscR7e.J.HeiDSrdCEx1MHplBQeWQvo40z9.gTJ1kC', 'http://bookmii.apphosthub.com/public/images/uploads/profile_59affcf569c94.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIwLCJpc3MiOiJodHRwOi8vYm9va21paS5hcHBob3N0aHViLmNvbS9hcGkvdjEvYXV0aC9yZWdpc3RlciIsImlhdCI6MTUwNDcwNTc2NCwiZXhwIjoxNTA0OTIxNzY0LCJuYmYiOjE1MDQ3MDU3NjQsImp0aSI6IjRLc0hORnpUOFh2bXdZZGgifQ.DGEkv2IqeBDXR4ox9UtzOlLQHavCf7fU3emB4U62Scc', NULL, 'provider', NULL, 1, '2017-08-23 23:08:53', '2017-09-06 17:49:41'),
(22, 'Leo', 'Leo', 'leo@gmail.com', NULL, '$2y$10$1nmmMMyvtUybWx.rqWLr3eySHph.BYEoUn2PfYdXprV8jWeWyUjke', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599e7d97daff3.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIyLCJpc3MiOiJodHRwOi8vYm9va21paS5hcHBob3N0aHViLmNvbS9hcGkvdjEvYXV0aC9sb2dpbiIsImlhdCI6MTUwNDY5Mzg2MCwiZXhwIjoxNTA0OTA5ODYwLCJuYmYiOjE1MDQ2OTM4NjAsImp0aSI6IklGdkFDOHoxeVF2UTRnclEifQ.zUMk3vZvtL4GmCen9w-_Cfpijn19cMgfC8Sph2KiDzQ', NULL, 'customer', NULL, 1, '2017-08-24 11:17:04', '2017-09-06 14:31:00'),
(23, 'Kjgdfg', 'Dsjkfejkg', 'e@e.com', NULL, '$2y$10$BO6iK9nTbnlu5VsA2YZ2NOP56pGzRTR2NnB3X8oK/cTfjGRwi0jCa', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599e8e517f6ae.png', NULL, NULL, 'provider', NULL, 1, '2017-08-24 12:28:50', '2017-08-24 12:31:05'),
(24, 'Gfbhn', 'Fbfgjbn', 'q@q.com', NULL, '$2y$10$/wdva1EQJDsjyAaXnRPXEOBDnlBDAKUiM.4/.hpXLtnIXWyQVuiDa', 'http://bookmii.apphosthub.com/public/images/uploads/profile_599ebca7e427f.png', NULL, NULL, 'provider', NULL, 1, '2017-08-24 15:46:30', '2017-08-24 15:47:57'),
(25, 'Eldhose', 'Ldos', 'eldhose@devfusion.ca', '88ab79bc8b26350a122ce18c0480ade68771979ab454a11bae7cd9253d7f154b', '$2y$10$i176kJXuUHLu4XbjERsvs.S3xynlXE7Q0J/ig8uslQNVY9ewDBd82', 'http://bookmii.apphosthub.com/public/images/uploads/profile_59a06c381a7c4.png', NULL, NULL, 'customer', NULL, 1, '2017-08-25 22:27:17', '2017-08-25 22:28:47'),
(26, 'John Smith', 'John Smith', 'apptesting2000@gmail.com', NULL, '$2y$10$Ib5LJwByLQh0o8muMrO4D.v77Dgz37KlO/Le8NKKqkLNzr8Tgv/fO', 'https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/1379841_10150004552801901_469209496895221757_n.jpg?oh=a727fee4935f5df7c99f0b1a9f719eb6&oe=5A55D1F8', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI2LCJpc3MiOiJodHRwOi8vYm9va21paS5hcHBob3N0aHViLmNvbS9hcGkvdjEvYXV0aC9yZWdpc3RlciIsImlhdCI6MTUwNDc5MzM4MCwiZXhwIjoxNTA1MDA5MzgwLCJuYmYiOjE1MDQ3OTMzODAsImp0aSI6IkRleE1qTm5vUDhJNmtqYXQifQ.S7Ap4RyGqetnjb48ZO1YWvUEXO9C3ZaLqq-3MAabguU', '306085129845632', 'customer', NULL, 1, '2017-09-05 18:37:34', '2017-09-07 18:09:40'),
(27, 'David Watson', 'David Watson', 'apptesting2001@gmail.com', 'f515cf03e20c9421dd70e672fa3d6b98426ab6f59c2e4ed51e294314b6bf6865', '$2y$10$xHcJwi69tL6YxoUqJEQGReLTqiZuftbhZW/tuzmPmepB1iV3Y4Q8i', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/17155194_255989874860741_1968895520373599525_n.jpg?oh=f6b03df10c0b94f7ab8f877fcb5dd9af&oe=5A52048F', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI3LCJpc3MiOiJodHRwOi8vYm9va21paS5hcHBob3N0aHViLmNvbS9hcGkvdjEvYXV0aC9yZWdpc3RlciIsImlhdCI6MTUwNDcwNzgwMSwiZXhwIjoxNTA0OTIzODAxLCJuYmYiOjE1MDQ3MDc4MDEsImp0aSI6IjZaekU2U2EwVWNFM3RJa3kifQ.6HoyzTbqc6a9P8mbsI5sYhGcqmMvUE31ZUY_QAhrFog', '294743664318695', 'customer', NULL, 1, '2017-09-06 14:20:55', '2017-09-06 18:23:21'),
(28, 'QWERTY', 'QWERTY', 'qwerty@qwerty.com', NULL, '$2y$10$tFCcHMGuAU01GqPIx5riSuP9FCCoRDplJwlh3VfqeMlRicBOtqgua', 'http://bookmii.apphosthub.com/public/images/uploads/profile_59b8d60022fbd.png', NULL, NULL, 'customer', NULL, 0, '2017-09-13 10:53:31', '2017-09-13 10:53:52'),
(29, 'QWERTY', 'QWERTY', 'qwerty@q.com', NULL, '$2y$10$ep1krT9wFAXuQBxDedhC.eUirAdYqOCUXa13mRBdiLwWpeGUWjB6C', 'http://bookmii.apphosthub.com/public/images/uploads/profile_59b8e0fe18842.png', NULL, NULL, 'provider', NULL, 1, '2017-09-13 11:40:28', '2017-09-13 11:42:56'),
(30, 'DSADASD', 'ASDASDASD', 'ASDASDAS@ASDASD.COM', NULL, '$2y$10$HEzf2GF.nJ6aUhnhftPIJuEowrIa1cfDOJtqn7V.ERcWWp/YBlhBG', 'http://bookmii.apphosthub.com/public/images/uploads/profile_59b8e1aedfd72.png', NULL, NULL, 'customer', NULL, 1, '2017-09-13 11:43:30', '2017-09-13 11:43:59'),
(31, 'Test Client', 'Test Client', 'tc@bm.com', NULL, '$2y$10$8izFedZUkFyL6wQKg6zVuuy5lYOxZ9v/Dql2a6ZE.eFS4LPzxrSp2', 'http://bookmii.apphosthub.com/public/images/uploads/profile_59b8fcdf7d260.png', NULL, NULL, 'customer', NULL, 1, '2017-09-13 13:39:28', '2017-09-13 13:40:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_services`
--

CREATE TABLE `user_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `service_id` int(10) DEFAULT NULL,
  `is_acitve` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_detail`
--
ALTER TABLE `account_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_detail_user_id_foreign` (`user_id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings_provider_id_foreign` (`provider_id`),
  ADD KEY `bookings_client_id_foreign` (`client_id`);

--
-- Indexes for table `cancelation_history`
--
ALTER TABLE `cancelation_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cancelation_history_booking_id_foreign` (`booking_id`),
  ADD KEY `cancelation_history_canceled_by_foreign` (`canceled_by`);

--
-- Indexes for table `card_detail`
--
ALTER TABLE `card_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `card_detail_user_id_foreign` (`user_id`);

--
-- Indexes for table `chat_head`
--
ALTER TABLE `chat_head`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourite`
--
ALTER TABLE `favourite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favourite_provider_id_foreign` (`provider_id`),
  ADD KEY `favourite_client_id_foreign` (`client_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_token`
--
ALTER TABLE `notification_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_history_booking_id_foreign` (`booking_id`),
  ADD KEY `payment_history_user_id_foreign` (`user_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_user_id_foreign` (`user_id`),
  ADD KEY `profile_service_id_foreign` (`service_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_receiver_id_foreign` (`receiver_id`),
  ADD KEY `reviews_sender_id_foreign` (`sender_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_images`
--
ALTER TABLE `service_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_images_profile_id_foreign` (`profile_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_services`
--
ALTER TABLE `user_services`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_detail`
--
ALTER TABLE `account_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `cancelation_history`
--
ALTER TABLE `cancelation_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `card_detail`
--
ALTER TABLE `card_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `chat_head`
--
ALTER TABLE `chat_head`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `favourite`
--
ALTER TABLE `favourite`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `notification_token`
--
ALTER TABLE `notification_token`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `service_images`
--
ALTER TABLE `service_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `user_services`
--
ALTER TABLE `user_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
