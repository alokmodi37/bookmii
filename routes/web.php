<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/expire-post', 'UserController@expirePosts')->name('expirePosts');
Route::match(['get', 'post'], '/reset-password/{token}', 'UserController@resetPassword')->name('resetPassword');
Route::get('/upload-image/{token}', 'UserController@uploadImage')->name('uploadImage');
Route::get('/activate-account/{token}', 'UserController@activateAccount')->name('uploadImage');

Route::group(['prefix' => 'admin'], function() {
	 Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
     Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');


	 Route::match(['get', 'post'], '/', 'Admin\AdminController@index')->name('landingPage');

	 Route::group(['prefix' => 'users'], function(){
	 	Route::match(['get', 'post'], '/', 'Admin\UserController@index')->name('listUser');
	 	Route::match(['get', 'post'], '/list', 'Admin\UserController@index')->name('listUser');
	 	Route::match(['get', 'post'], '/edit', 'Admin\UserController@edit')->name('editUser');
	 	Route::match(['get', 'post'], '/delete', 'Admin\UserController@delete')->name('deleteUser');
	 	Route::match(['get', 'post'], '/view', 'Admin\UserController@view')->name('viewUser');
	 });

	 Route::group(['prefix' => 'payment'], function(){
	 	Route::match(['get', 'post'], '/', 'Admin\PaymentController@index')->name('listPayment');
	 	Route::match(['get', 'post'], '/list', 'Admin\PaymentController@index')->name('listPayment');
	 	Route::match(['get', 'post'], '/view', 'Admin\PaymentController@view')->name('userView');
	 	Route::match(['get', 'post'], '/invoice', 'Admin\PaymentController@invoice')->name('viewInvoice');
	 });

	 Route::group(['prefix' => 'booking'], function(){
	 	Route::match(['get', 'post'], '/', 'Admin\BookingController@index')->name('listBooking');
	 	Route::match(['get', 'post'], '/list', 'Admin\BookingController@index')->name('listBooking');
	 	Route::match(['get', 'post'], '/edit', 'Admin\BookingController@edit')->name('editBooking');
	 	Route::match(['get', 'post'], '/delete', 'Admin\BookingController@delete')->name('deleteBooking');
	 	Route::match(['get', 'post'], '/detail', 'Admin\BookingController@detail')->name('viewBooking');

	 });

	 Route::group(['prefix' => 'notification'], function(){
	 	Route::match(['get', 'post'], '/', 'Admin\NotificationController@index')->name('listNotificationSetting');
	 	Route::match(['get', 'post'], '/list',  'Admin\NotificationController@index')->name('listNotificationSetting');
	 	Route::match(['get', 'post'], '/list-notification', 'Admin\NotificationController@notificationTemplate')->name('listNotification');
	 	Route::match(['get', 'post'], '/edit', 'Admin\NotificationController@edit')->name('editNotification');
	 	Route::match(['get', 'post'], '/delete', 'Admin\NotificationController@delete')->name('deleteNotification');
	 	Route::match(['get', 'post'], '/view', 'Admin\NotificationController@view')->name('viewNotification');
	 });

	 Route::group(['prefix' => 'setting'], function(){
	 	Route::match(['get', 'post'], '/', 'Admin\SettingController@index')->name('listSetting');
	 	Route::match(['get', 'post'], '/list', 'Admin\SettingController@index')->name('listSetting');
	 	Route::match(['get', 'post'], '/edit', 'Admin\SettingController@edit')->name('editSetting');
	 	Route::match(['get', 'post'], '/delete', 'Admin\SettingController@delete')->name('deleteSetting');
	 	Route::match(['get', 'post'], '/view', 'Admin\SettingController@view')->name('viewSetting');
	 });

	 Route::group(['prefix' => 'content'], function(){
	 	Route::match(['get', 'post'], '/', 'Admin\ContentController@index')->name('listContent');
	 	Route::match(['get', 'post'], '/list', 'Admin\ContentController@index')->name('listContent');
	 	Route::match(['get', 'post'], '/edit', 'Admin\ContentController@edit')->name('editContent');
	 	Route::match(['get', 'post'], '/view', 'Admin\ContentController@detail')->name('viewContent');
	 });

});

/*Route::any('/reset-password/{token}', function($token = null){
    dd($token);
});*/

Auth::routes();
