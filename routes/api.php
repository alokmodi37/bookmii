<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::group(['prefix' => 'v1'], function() {
	Route::group(['prefix' => 'auth'], function(){
		Route::post('register', 'UserController@register');
		Route::post('login', 'UserController@login');
		Route::post('verify', 'UserController@verifyEmail');
		Route::post('forgot-password', 'UserController@forgotPassword');
		Route::match(['get', 'post'], 'get-services', 'UserController@getServices');
	});

  Route::group(['prefix' => 'search'], function(){
  	Route::match(['get', 'post'], '/', 'SearchController@getResult')->name('search');
  });


  Route::group(['prefix' => 'site'], function(){
		Route::match(['get', 'post'], 'test-notification', 'UserController@testNotification')->name('testNotification');
		Route::match(['get', 'post'], 'help-support', 'UserController@getSupport')->name('getSupport');
		Route::match(['get', 'post'], 'term-policy', 'UserController@getTerms')->name('getTerms');
	});

	Route::group(['prefix' => 'cron'], function(){
		Route::match(['get', 'post'], 'expire-bookings', 'CronController@expireBooking')->name('expiredBooking');
	});


	Route::group(['middleware' => 'jwt.auth'], function () {
	    Route::post('user', 'UserController@getAuthUser')->name('userDetail');
	    Route::match(['get', 'post'], 'services', 'UserController@getServices')->name('services');

	    Route::post('validate-token', 'UserController@isValidToken');

	    Route::group(['prefix' => 'profile'], function(){
	    	Route::match(['get', 'post'], 'get', 'ProfileController@getProfile')->name('getProfile');
	    	Route::match(['get', 'post'], 'save', 'ProfileController@saveProfile')->name('saveProfile');
				Route::match(['post'], 'update-charge', 'ProfileController@updateCharge')->name('updateCharge');
		});

	    Route::group(['prefix' => 'notification'], function(){
	    	Route::match(['post'], 'save', 'ProfileController@setNotificationToken')->name('setNotificationToken');
	    });

	    Route::group(['prefix' => 'rating'], function(){
	    	Route::match(['get', 'post'], 'get', 'RatingController@getRating')->name('getRating');
	    	Route::match(['get', 'post'], 'save', 'RatingController@saveRating')->name('saveRating');
	    });


	    Route::group(['prefix' => 'favourite'], function(){
	    	Route::match(['get', 'post'], 'get-provider', 'FavouriteController@getFavourite')->name('getFavouriteProvider');

	    	Route::match(['get', 'post'], 'add', 'FavouriteController@addFavourite')->name('addFavourateProvider');
	    	Route::match(['get', 'post'], 'remove', 'FavouriteController@removeFavourite')->name('removeFavouriteProvider');
	    });

	    Route::group(['prefix' => 'booking'], function(){
	    	Route::match(['get', 'post'], 'is-available', 'BookingController@isAvailable')->name('isAvailable');
	    	Route::match(['get', 'post'], 'book-provider', 'BookingController@bookProvider')->name('bookProvider');
	    	Route::match(['get', 'post'], 'cancel', 'BookingController@cancelBooking')->name('cancelBooking');
	    	Route::match(['get', 'post'], 'confirm', 'BookingController@confirmBooking')->name('confirmBooking');
	    	Route::match(['get', 'post'], 'finished', 'BookingController@finishBooking')->name('finishBooking');
	    	Route::match(['get', 'post'], 'list', 'BookingController@showBooking')->name('showBooking');

	    	Route::match(['get', 'post'], 'invoice', 'BookingController@invoice')->name('invoice');
	    });

	    Route::group(['prefix' => 'setting'], function(){
	    	Route::match(['get', 'post'], 'set', 'ProfileController@setSetting')->name('setSetting');
	    	Route::match(['get', 'post'], 'get', 'ProfileController@getSetting')->name('getSetting');
	    });

	    Route::group(['prefix' => 'payment'], function(){
	    	Route::match(['get', 'post'], 'token', 'PaymentController@token')->name('paymentToken');
	    	Route::match(['get', 'post'], 'pay', 'PaymentController@pay')->name('paymentPay');
	    });

	    Route::group(['prefix' => 'chat'], function() {
	    	Route::post('init', 'ChatController@initialize')->name('initializeChat');
	    	Route::post('history', 'ChatController@historyChat')->name('historyChat');
	    	Route::post('lastChat', 'ChatController@lastChat')->name('lastChat');
	    	Route::post('send-notification', 'ChatController@sendNotification')->name('sendNotification');
	    });

	});
});
