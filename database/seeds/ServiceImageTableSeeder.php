<?php

use Illuminate\Database\Seeder;

class ServiceImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ServiceImage::class, 5)->create();
    }
}
