<?php

use Illuminate\Database\Seeder;

class ProfileImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        $users = App\User::all();

        foreach ($users as $user) {
        	if( !$user->profile_image )  {
        		 DB::table('users')->update([
	                'profile_image' => $faker->imageUrl(400, 400, 'people')
	            ]);
        	}
           
        }
    }
}
