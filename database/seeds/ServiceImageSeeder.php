<?php

use Illuminate\Database\Seeder;

class ServiceImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        $users = App\Profile::all();

        foreach ($users as $user) {
        	$serviceImages = App\ServiceImage::where('profile_id', $user->id)->count();
        	$serviceImageArray = [];
        	if( !$serviceImages )  {
        		 $counter = 1;
        		 while($counter <= 4) {
	        		 $serviceImageArray[] = [
		                'image_path' => $faker->imageUrl(400, 400, 'people'),
		                'profile_id' => $user->id
		             ];
		             $counter++;
		         };

	            DB::table('service_images')->insert($serviceImageArray);
        	}
           
        }
    }
}
