<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

/* Helper function to filter id field only */
function fillOnlyFields($rowSet){
	$resultRowSet = [];
	foreach($rowSet as $key => $value){
		foreach($value as $subvalue){
			$resultRowSet[$key] = $subvalue;
		}
	}
	return $resultRowSet;
}

/* Seeder factory for User Table*/
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $width = 300;
    $height = 300;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'type' => $faker->randomElement($array = array ('customer','provider')),
        'remember_token' => str_random(10),
        'profile_image' => $faker->imageUrl($width, $height, 'people'), 
    ];
});

/* Seeder factory for Profile Table*/
$factory->define(App\Profile::class, function(Faker\Generator $faker) use ($factory){
	
	$users = fillOnlyFields(App\User::select('id')->get()->toArray());
	$services =   fillOnlyFields(App\Service::select('id')->get()->toArray());
	
    return [
        'user_id' => $faker->randomElement($users),
        'service_id' => $faker->randomElement($services),
       
        'about'  => $faker->sentence, 
        'dob' => $faker->date($format = 'Y-m-d', $max = 'now'), 
        'address' => $faker->streetAddress, 
        'latitude' => $faker->latitude($min = -90, $max = 90) , 
        'longitude' => $faker->longitude($min = -180, $max = 180), 
    ];
});

/* Seeder factory for Review Table*/
$factory->define(App\Review::class, function(Faker\Generator $faker) use ($factory){
	$customer = fillOnlyFields(App\User::select('id')->where('type', 'customer')->get()->toArray());
	$provider = fillOnlyFields(App\User::select('id')->where('type', 'provider')->get()->toArray());
	
    $validCombination = false;
    $totalAttempt = 0;

    $providerId = $faker->randomElement($provider);
    $customerId = $faker->randomElement($customer);

    while(!$validCombination) {
        $hasPreviousRecord = App\Review::where('receiver_id', $providerId)
                    ->where('sender_id', $customerId)
                    ->count();

        if ($hasPreviousRecord && $totalAttempt < 10) {
            $providerId = $faker->randomElement($provider);
            $customerId = $faker->randomElement($customer);
        } else {
            $validCombination = true;
            break;
        }
        $totalAttempt++;
    }
    
    if($validCombination) {
    	return [
            'receiver_id' => $faker->randomElement($provider),
            'sender_id' => $faker->randomElement($customer),
            'comment' => $faker->sentence, 
            'rating' => $faker->biasedNumberBetween($min = 1, $max = 5, $function = 'sqrt')
        ];
    } else {
        return;
    }
});

/* Seeder factory for Favourite Table*/
$factory->define(App\Favourite::class, function(Faker\Generator $faker) use ($factory){
	$customer = fillOnlyFields(App\User::select('id')->where('type', 'customer')->get()->toArray());
	$provider = fillOnlyFields(App\User::select('id')->where('type', 'provider')->get()->toArray());
	
	return [
        'provider_id' => $faker->randomElement($provider),
        'client_id' => $faker->randomElement($customer),
    ];
});

/* Seeder factory for ServiceImage Table*/
$factory->define(App\ServiceImage::class, function(Faker\Generator $faker) use ($factory){
	$profileId = 2;
	$width = 300;
	$height = 300;

	return [
        'profile_id' => $profileId,
        'image_path' => $faker->imageUrl($width, $height, 'cats')
    ];
});


/* Seeder factory for Booking Table*/
$factory->define(App\Booking::class, function(Faker\Generator $faker) use ($factory){
	$customer = fillOnlyFields(App\User::select('id')->where('type', 'customer')->get()->toArray());
	$provider = fillOnlyFields(App\User::select('id')->where('type', 'provider')->get()->toArray());
	
	return [
        'provider_id' => $faker->randomElement($provider),
        'client_id' => $faker->randomElement($customer),
        'start_date' => $faker->date($format = '2017-m-d', $max = 'now'),
        'start_time' => $faker->time($format = 'H:i:s', $max = 'now'),
        'end_date' => $faker->date($format = '2017-m-d', $max = 'now'),
        'end_time' => $faker->time($format = 'H:i:s', $max = 'now'),
    ];
});
