<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('service_images', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profile')->onDelete('cascade');

            $table->string('image_path');
            $table->boolean('is_Active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('service_images');
    }
}
