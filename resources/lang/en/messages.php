<?php

return [
	
	//Token related messages

	'PROCESS_SUCCESS'			=> 'Your request has been processed',
	'PROBLEM_OCCURED'			=> 'Some Problem Occurred',
	'INVALID_REQUEST'			=> 'Invalid Request',


	'FAILED_TOKEN' 				=> 'Failed to create token',
	'SUCCESS_TOKEN'				=> 'Token created successfully',
	'VALID_TOKEN' 				=> 'Provided token is valid',
	'INVALID_TOKEN' 			=> 'Provided token is invalid',


	//Registration and login related messages

	'ALLREDAY_REGISTERED_LOGIN' => 'Email Address is Already Registered.',
	'EMAIL_AVAILABLE' 			=> 'Email Address available',
	'NOT_REGISTERED'			=> 'Your Email Address is not Registered.',
	'SUCCESSFUL_REGISTRATION' 	=> 'You have successfully registered',
	'USER_NOT_EXIST'			=> 'User does not exist. Please sign up.',
	'INVALID_EMAIL_PASSWORD' 	=> 'Invalid email address or password.',
	'RESET_PASSWORD_LINK'		=> 'Please check your inbox for an email to reset your password.',
	'VALIDATION_REQUIRED'		=> 'Required fields has no data',

	// Search related messages

	'NO_RESULT_FOUND'			=> 'No Result Found',
	'RESULT_FOUND'				=> 'Result Found',
	'SUCCESS_BOOKING'			=> 'You have successfully booked the service.',
	'BOOKING_DETAIL'			=> 'Booking Detail',
	'NO_BOOKING'				=> 'You have no Bookings at the moment. Click the button below to browse service providers.',

	// Meeting related messages

	'MEETING_CANCEL'			=> 'Cancellation request has been processed',
	'MEETING_CONFIRM'			=> 'Confirmation request has been processed',


	// Booking related messages
	'BOOKING_AVAILABLE'			=> 'Booking Slot available',
	'BOOKING_NOT_AVAILABLE'		=> 'Booking Slot not available',

	//Favourite Related messages
	'FAVOURITE_ADDED' 			=> 'Service provider is in your favorite list',
	'FAVOURITE_REMOVED'			=> 'Service provider is out from your favorite list',
	'ALLREADY_FAVOURITE'		=> 'Provider is allready in your favorite list',
	'NOT_FAVOURITE'				=> 'Provider is not in your favorite list',
	'NO_RESULT_FAVOURITE'		=> 'You have no favorites at the moment. Click the button below to browse service providers.',

	'UNAUTHORIZE_REQUEST'		=> 'As a client you are not authorize to process this request'
];