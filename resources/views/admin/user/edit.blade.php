@extends('layouts.admin')

@section('content')
<div id="content" class="col-xs-12 col-sm-10">
	<br/>
			<div class="box">
				<div class="box-header">
					<div class="box-name ui-draggable-handle">
						<i class="fa fa-home"></i>
						<span>Edit User</span>
					</div>
					<div class="box-icons">
						
					</div>
					<div class="no-move"></div>
				</div>
				
				<div class="box-content">
					<form method="post">
						{{ csrf_field() }}

						
						<div class="card">
							<h4 class="page-header">User Detail</h4>

							<div class="row">
								<div class="col-xs-3"><b>Name  : </b></div><div class="col-xs-3">
									<input type="text" class="form-control" name="name" value="{{$user->name}}" />
									<div class="error">{{ $errors->first('name') }}</div>
									<input type="hidden" class="form-control" name="id" value="{{$user->id}}" />
								</div>

								<div class="col-xs-3"><b>Email : </b></div>
								<div class="col-xs-3">
									<input type="text" class="form-control" name="email" value="{{$user->email}}" />
									<div class="error">{{ $errors->first('email') }}</div>
								</div>
							</div>
							<br/>
							<div class="row">
								<div class="col-xs-3"><b>Type  : </b></div>
								<div class="col-xs-3">
									<select class="form-control search-control" name="user_type">
										@foreach ($userTypes as $typeId => $userType)
											<option value="{{$typeId }}" 
											@if($user->type && $user->type == $typeId) 
												selected 
											@endif>{{$userType}}</option>
										@endforeach
									</select>
								</div>

								<div class="col-xs-3"><b>Services : </b></div><div class="col-xs-3">
									<select class="form-control search-control" name="service">
										<option value="">--Select Service--</option>
										@foreach ($services as $service)
											<option value="{{$service->id}}" 
											@if($user->services && $user->services->id == $service->id)
												selected 
											@endif>
											{{$service->service}}
											</option>
										@endforeach
									</select>
								</div>
							</div>
							<br/>
							
							<div class="row">
								<div class="col-xs-3"><b>Date Of Birth  : </b></div>
								<div class="col-xs-3">
									<input type="text" class="form-control" name="dob" value="{{ ($user->dob) }}" />
									<div class="error">{{ $errors->first('dob') }}</div>
								</div>

								<div class="col-xs-3"><b>About : </b></div>
								<div class="col-xs-3">
									<textarea class="form-control" name="about">{{$user->about}}</textarea>
									<div class="error">{{ $errors->first('about') }}</div>
								</div>
							</div>
							<br/>
							<div class="row">
								<div class="col-xs-3"><b>Address  : </b></div>
								<div class="col-xs-3">
									<textarea class="form-control" name="address">{{ ucfirst($user->address) }}</textarea>
								</div>

								<div class="col-xs-3"><b>Latitude, Longitude : </b></div><div class="col-xs-3">
									{{$user->latitude}}, {{$user->longitude}}
								</div>
							</div>
							<br/>

							<div class="row">
								<div class="col-xs-3"><b>Facebook  : </b></div>
								<div class="col-xs-3">
									<input type="text" class="form-control" name="facebook" value="{{ ($user->facebook) }}" />
								</div>

								<div class="col-xs-3"><b>Instagram : </b></div><div class="col-xs-3">
									<input type="text" class="form-control" name="instagram" value="{{$user->instagram}}" />
								</div>
							</div>
							<br/>
							<div class="row">
								<div class="col-xs-3"><b>Twitter : </b></div><div class="col-xs-3">
									<input type="text" class="form-control" name="twitter" value="{{$user->twitter}}" />
								</div>
								<div class="col-xs-3"><b>Youtube : </b></div><div class="col-xs-3">
									<input type="text" class="form-control" name="youtube" value="{{$user->youtube}}" />
								</div>
							</div>
							<br/><br/><br/>	
							<div class="row">
								<div class="col-xs-12" align="right">
									<button type="submit" class="btn btn-primary">Update</button>
									<a href="{{ route('listUser') }}" class="btn btn-warning">Back</a>
								</div>
							</div>
							<br/>
						</div>

						@if($user->profile_image || $user->servicesImages->count())
						<div class="card">
							
							<h4 class="page-header">User Images:</h4>
							<div class="row">
								@if($user->profile_image)
								<div class="col-xs-12 col-sm-3">
									<div class="box box-pricing">
										<div class="thumbnail">
											<img src="{{$user->profile_image}}" alt="">    
											<div class="caption">
												<h5 class="text-center">Profile Image</h5>
												<div align="center"><button type="button" class="btn btn-primary">Delete</button></div>
											</div>
										</div>
									</div>
								</div>
								@endif

								@if($user->servicesImages)
									@foreach($user->servicesImages as $serviceImage)
										<div class="col-xs-12 col-sm-3">
											<div class="box box-pricing">
												<div class="thumbnail">
													<img src="{{$serviceImage->image_path}}" alt="">    
													<div class="caption">
														<h5 class="text-center">Service Image</h5>
														<div align="center"><button type="button" class="btn btn-primary">Delete</button></div>
													</div>
												</div>
											</div>
										</div>
									@endforeach
								@endif
							</div>
						</div>
						@endif
					</form>
				</div>
			</div>
	
</div>
@endsection