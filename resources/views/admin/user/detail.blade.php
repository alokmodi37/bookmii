@extends('layouts.admin')

@section('content')
<div id="content" class="col-xs-12 col-sm-10">
	<br/>
			<div class="box">
				<div class="box-header">
					<div class="box-name ui-draggable-handle">
						<i class="fa fa-home"></i>
						<span>User Detail</span>
					</div>
					<div class="box-icons">
						
					</div>
					<div class="no-move"></div>
				</div>
				
				<div class="box-content">
					<div class="card">
						<h4 class="page-header">User Detail</h4>
						<div class="row">
							<div class="col-xs-3"><b>Name  : </b></div><div class="col-xs-3">{{$user->name}}</div>

							<div class="col-xs-3"><b>Email : </b></div><div class="col-xs-3">{{$user->email}}</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-3"><b>Type  : </b></div><div class="col-xs-3">{{ ucfirst($user->type) }}</div>

							<div class="col-xs-3"><b>Services : </b></div><div class="col-xs-3">
							@if($user->services) 
								{{ $user->services->service }}
							@endif
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-3"><b>Date Of Birth  : </b></div><div class="col-xs-3">{{ ($user->dob) }}</div>

							<div class="col-xs-3"><b>About : </b></div><div class="col-xs-3">
								{{$user->about}}
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-3"><b>Address  : </b></div><div class="col-xs-3">{{ ucfirst($user->address) }}</div>

							<div class="col-xs-3"><b>Latitude, Longitude : </b></div><div class="col-xs-3">
								{{$user->latitude}}, {{$user->longitude}}
							</div>
						</div>
						<br/>

						<div class="row">
							<div class="col-xs-3"><b>Facebook  : </b></div>
							<div class="col-xs-3">
								{{ ($user->facebook) }}
							</div>

							<div class="col-xs-3"><b>Instagram : </b></div><div class="col-xs-3">
								{{$user->instagram}}
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-3"><b>Twitter : </b></div><div class="col-xs-3">
								{{$user->twitter}}
							</div>
							<div class="col-xs-3"><b>Youtube : </b></div><div class="col-xs-3">
								{{$user->youtube}}
							</div>
						</div>
						<br/><br/><br/>	
							<div class="row">
								<div class="col-xs-12" align="right">
									<a href="{{ route('listUser') }}" class="btn btn-warning">Back</a>
								</div>
							</div>
						<br/>
					</div>
					@if($user->profile_image || $user->servicesImages->count())
					<div class="card">
						
						<h4 class="page-header">User Images:</h4>
						<div class="row">
							@if($user->profile_image)
								<div class="col-xs-12 col-sm-3">
									<div class="box box-pricing">
										<div class="thumbnail">
											<img src="{{$user->profile_image}}" alt="">    
											<div class="caption">
												<h5 class="text-center">Profile Image</h5>
											</div>
										</div>
									</div>
								</div>
							@endif

							@if($user->servicesImages)
								@foreach($user->servicesImages as $serviceImage)
									<div class="col-xs-12 col-sm-3">
										<div class="box box-pricing">
											<div class="thumbnail">
												<img src="{{$serviceImage->image_path}}" alt="">    
												<div class="caption">
													<h5 class="text-center">Service Image</h5>
												</div>
											</div>
										</div>
									</div>
								@endforeach
							@endif
						</div>
						
						
					</div>
					@endif
				</div>
			</div>
	
</div>
@endsection