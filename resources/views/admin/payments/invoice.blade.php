@extends('layouts.admin')

@section('content')
<div id="content" class="col-xs-12 col-sm-10">
		</br>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-content">
				<div class="col-xs-6">
					<b>BILL TO:</b>
					
					<address>
						<strong>{{ $payment->client_name }}</strong><br>
						<a href="mailto:#">{{ $payment->email }}</a>
					</address>
				</div>
				<div class="col-xs-6 text-right">
					<h3 class="invoice-header">Invoice #{{ $payment->id }}</h3>
					<p>Invoice Date: {{ $payment->created_at }}</p>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12">
					<br/>
					<table class="table table-hover">
						<thead>
							<tr>
								<th>SERVICE</th>
								<th>DURATION</th>
								<th>PRICE</th>
								<th>TOTAL</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="m-ticker"><span>{{ $payment->service }}</span></td>
								<td>2</td>
								<td>{{ $payment->amount }}</td>
								<td>{{ $payment->amount }}</td>
							</tr>
							<tr>
								<td class="m-ticker"><span>Service Charges ()<sup>*</sup></span></td>
								<td>-</td>
								<td>x 10%</td>
								<td>{{ ($payment->amount * 10) / 100 }}</td>
							</tr>
							<tr class="active">
								<td></td>
								<td></td>
								<td>Total</td>
								<td><b>${{ $payment->amount + (($payment->amount * 10) / 100) }}<sup>*</sup> </b></td>
							</tr>
						</tbody>
					</table>
					
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12">
					<div class="col-xs-6">
						&nbsp;
					</div>
					<div class="col-xs-6">
						<a href="{{ route('listPayment') }}" class="btn btn-primary btn-label-left pull-right"><span><i class="fa fa-arrow-left"></i></span> Back</a>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
@endsection