@extends('layouts.admin')

@section('content')
	<div id="content" class="col-xs-12 col-sm-10">
		</br>
		<div class="box ui-draggable ui-droppable">
			<div class="box-header">
				<div class="box-name ui-draggable-handle">
					<i class="fa fa-users"></i>
					<span>Bookmii Notifications</span>
				</div>
				<div class="box-icons">
					
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content no-padding table-responsive">
				<form id="notification-form" method="post">
					 {{ csrf_field() }}
					<table class="table">
						<thead>
							<tr>
								<th rowspan="1" colspan="1">Keyword</th>
								<th rowspan="1" colspan="1">Service</th>
								<th rowspan="1" colspan="1">Booking Status</th>
								<th rowspan="1" colspan="1">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th rowspan="1" colspan="1">
								<input type="text" class="form-control search-control" name="search_text" value="{{$searchData['search_text']}}" />
								<input type="hidden"  id="sorting_field"  name="sorting_field" value="{{$searchData['sorting_field']}}"/>
								<input type="hidden"  id="sorting_order"  name="sorting_order" value="{{$searchData['sorting_order']}}"/>
								</th>
								<th rowspan="1" colspan="1">
									<select class="form-control search-control" name="service">
										<option value="">--Select Service--</option>
										@foreach ($services as $service)
											<option value="{{$service->id}}" 
											@if($searchData['service'] && $searchData['service'] == $service->id)
												selected 
											@endif>
											{{$service->service}}
											</option>
										@endforeach
									</select>
								</th>
								<th rowspan="1" colspan="1">
									<button type="submit" class="btn btn-primary btn-xs">Search</button>
									<button type="button" class="btn btn-danger btn-xs" onClick="clearData()">Canel</button>
								</th>
							</tr>
						</tbody>
					</table>
				</form>
				<table class="table table-bordered table-striped table-datatable" >
					<thead>
						<tr>
							<th rowspan="1" colspan="1">#No</th>
							<th  class="{{ $getSortingClass('provider.name') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('provider.name', '{{ $getSortinOrder('provider.name') }}' )" >Provider Name</th>
							<th  class="{{ $getSortingClass('client.name') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('client.name', '{{ $getSortinOrder('client.name') }}' )" >Client Name</th>

							<th  class="{{ $getSortingClass('services.service') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('services.service', '{{ $getSortinOrder('services.service') }}' )" >For Service</th>

							<th  class="{{ $getSortingClass('bookings.start_date') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('bookings.start_date', '{{ $getSortinOrder('bookings.start_date') }}' )" >Booking From</th>

							<th  class="{{ $getSortingClass('bookings.end_date') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('bookings.end_date', '{{ $getSortinOrder('bookings.end_date') }}' )" >Booking To</th>

							<th  class="{{ $getSortingClass('payment_history.amount') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('payment_history.amount', '{{ $getSortinOrder('payment_history.amount') }}' )" >Amount</th>
							
							<th rowspan="1" colspan="1">Action</th>
						</tr>
					</thead>

					@if($payments->count())
					<tbody>
						@foreach ($payments as $index => $payment)
					        <tr>
								<td>
									{{$index + 1}}
								</td>
								<td>{{$payment->provider_name}}</td>
								<td>{{$payment->client_name}}</td>
								<td>{{$payment->service}}</td>
								<td>{{$payment->start_date}} {{$payment->start_time}}</td>
								<td>{{$payment->end_date}} {{$payment->end_time}}</td>
								<td>{{$payment->amount}}</td>
								<td>
									<a  href="{{ route('viewInvoice', ['payment_id' => $payment->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-credit-card"></i> Invoice</a>
                               </td>
							</tr>
					    @endforeach				
					</tbody>
					<tfoot>
						<tr>
							<th rowspan="1" colspan="1">#No</th>
							<th  class="{{ $getSortingClass('provider.name') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('provider.name', '{{ $getSortinOrder('provider.name') }}' )" >Provider Name</th>
							<th  class="{{ $getSortingClass('client.name') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('client.name', '{{ $getSortinOrder('client.name') }}' )" >Client Name</th>

							<th  class="{{ $getSortingClass('services.service') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('services.service', '{{ $getSortinOrder('services.service') }}' )" >For Service</th>

							<th  class="{{ $getSortingClass('bookings.start_date') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('bookings.start_date', '{{ $getSortinOrder('bookings.start_date') }}' )" >Booking From</th>

							<th  class="{{ $getSortingClass('bookings.end_date') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('bookings.end_date', '{{ $getSortinOrder('bookings.end_date') }}' )" >Booking To</th>

							<th  class="{{ $getSortingClass('payment_history.amount') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('payment_history.amount', '{{ $getSortinOrder('payment_history.amount') }}' )" >Amount</th>
							<th  rowspan="1" colspan="1">Action</th>
						</tr>
					</tfoot>
					@else
						<tbody>
							<th colspan="6"><div align="center"><b>Record Not Found</b></div></th>
						</tbody>
					@endif
				</table>
				<div class="box-content pull-right">
					{{$payments->links()}}
				</div>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	function setSortingField(field, order) {
		$("#sorting_field").val(field);
		$("#sorting_order").val(order);
		$("#notification-form").submit();
	}

	function clearData(){
		$(".search-control").each(function() {
			$(this).val("");
		})
	
		$("#sorting_field").val('provider.name');
		$("#sorting_order").val('asc');
		$("#notification-form").submit();
	}
</script>
@endsection

