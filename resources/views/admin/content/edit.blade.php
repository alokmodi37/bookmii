@extends('layouts.admin')

@section('content')
<div id="content" class="col-xs-12 col-sm-10">
	<br/>
			<div class="box">
				<div class="box-header">
					<div class="box-name ui-draggable-handle">
						<i class="fa fa-home"></i>
						<span>Edit Content</span>
					</div>
					<div class="box-icons">
						
					</div>
					<div class="no-move"></div>
				</div>
				
				<div class="box-content">
					<form method="post">
						{{ csrf_field() }}
						
						<div class="card">
							<h4 class="page-header">Content Detail ({{$content->content_type}})</h4>

							<div class="row">
								<div class="col-xs-12"><b>Content</b></div>
								<div class="col-xs-12">
									<textarea class="form-control" style="height:200px;" name="content">{{$content->content}}</textarea>
									<div class="error">@if($errors) {{ $errors->first('content') }} @endif</div>
									<input type="hidden" class="form-control" name="id" value="{{$content->id}}" />
								</div>
							</div>
							<br/>
							<br/>
							<br/>	
							<div class="row">
								<div class="col-xs-12" align="right">
									<button type="submit" class="btn btn-primary">Update</button>
									<a href="{{ route('listContent') }}" class="btn btn-warning">Back</a>
								</div>
							</div>
							<br/>
						</div>
					</form>
				</div>
			</div>
	
</div>
@endsection