@extends('layouts.admin')

@section('content')
<div id="content" class="col-xs-12 col-sm-10">
	<br/>
			<div class="box">
				<div class="box-header">
					<div class="box-name ui-draggable-handle">
						<i class="fa fa-home"></i>
						<span>View Content</span>
					</div>
					<div class="box-icons">
						
					</div>
					<div class="no-move"></div>
				</div>
				
				<div class="box-content">
						<div class="card">
							<h4 class="page-header">Content Detail ({{$content->content_type}})</h4>

							<div class="row">
								<div class="col-xs-12"><b>Content</b></div>
								<div class="col-xs-12">
									{{$content->content}}
								</div>
							</div>
							<br/>
							<br/>
							<br/>	
							<div class="row">
								<div class="col-xs-12" align="right">
									<a href="{{ route('listContent') }}" class="btn btn-warning">Back</a>
								</div>
							</div>
							<br/>
						</div>
				
				</div>
			</div>
	
</div>
@endsection