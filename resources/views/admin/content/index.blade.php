@extends('layouts.admin')

@section('content')
	<div id="content" class="col-xs-12 col-sm-10">
		</br>
		<div class="box ui-draggable ui-droppable">
			<div class="box-header">
				<div class="box-name ui-draggable-handle">
					<i class="fa fa-users"></i>
					<span>Bookmii Html</span>
				</div>
				<div class="box-icons">
					
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content no-padding table-responsive">
				<table class="table table-bordered table-striped table-datatable" >
					<thead>
						<tr>
							<th rowspan="1" colspan="1">#No</th>
							<th rowspan="1" colspan="1">Content Type</th>
							<th rowspan="1" colspan="1">Content</th>
							<th  rowspan="1" colspan="1">Action</th>
						</tr>
					</thead>

					@if($contents->count())
					<tbody>
						@foreach ($contents as $index => $content)
					        <tr>
								<td>
									{{$index + 1}}
								</td>
								<td>{{$content->content_type}}</td>
								<td>{{$content->content}}</td>
								<td>
									<div class="btn-group">
	                                  <a style="width: 35px;" class="btn btn-primary" href="{{ route('editContent', ['content_id' => $content->id]) }}" title="Edit Content"><i class="fa fa-edit"></i></a>
	                                  <a style="width: 35px;" class="btn btn-success" href="{{ route('viewContent', ['content_id' => $content->id]) }}" title="View Content"><i class="fa fa-eye"></i></a>
	                                </div>
                               </td>
							</tr>
					    @endforeach				
					</tbody>
					<tfoot>
						<tr>
							<th rowspan="1" colspan="1">#No</th>
							<th rowspan="1" colspan="1">Content Type</th>
							<th rowspan="1" colspan="1">Content</th>
							<th  rowspan="1" colspan="1">Action</th>
						</tr>
					</tfoot>
					@else
						<tbody>
							<th colspan="6"><div align="center"><b>Record Not Found</b></div></th>
						</tbody>
					@endif
				</table>
				</div>
			</div>
		</div>
	</div>
@endsection

