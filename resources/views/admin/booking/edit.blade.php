@extends('layouts.admin')

@section('content')
<div id="content" class="col-xs-12 col-sm-10">
	<br/>
			<div class="box">
				<div class="box-header">
					<div class="box-name ui-draggable-handle">
						<i class="fa fa-home"></i>
						<span>Booking Detail</span>
					</div>
					<div class="box-icons">
						
					</div>
					<div class="no-move"></div>
				</div>
				
				<div class="box-content">
					<div class="card">
						<h4 class="page-header">Booking Detail</h4>
						<div class="row">
							<div class="col-xs-3"><b>Provider  : </b></div><div class="col-xs-3">{{$booking->provider_name}}</div>

							<div class="col-xs-3"><b>client : </b></div><div class="col-xs-3">{{$booking->client_name}}</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-3"><b>Service Type  : </b></div><div class="col-xs-3">{{ ($booking->service) }}</div>

							<div class="col-xs-3"><b>Services Duration : </b></div><div class="col-xs-3">
								{{ round($booking->duration, 2) }}
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-3"><b>Service Start Date  : </b></div><div class="col-xs-3">
							{{ ($booking->start_date) }} {{$booking->start_time}}</div>

							<div class="col-xs-3"><b>Service End Date : </b></div><div class="col-xs-3">
								{{($booking->end_date)}}  {{$booking->end_time}}
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-3"><b>Charges  : </b></div><div class="col-xs-3">{{ ($booking->price) }}</div>

							<div class="col-xs-3"><b>Status : </b></div><div class="col-xs-3">
								{{$booking->status}}
							</div>
						</div>
						<br/>

						<br/><br/><br/>	
							<div class="row">
								<div class="col-xs-12" align="right">
									<a href="{{ route('listBooking') }}" class="btn btn-warning">Back</a>
								</div>
							</div>
						<br/>
					</div>
				</div>
			</div>
	
</div>
@endsection