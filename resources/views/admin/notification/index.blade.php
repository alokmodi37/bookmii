@extends('layouts.admin')

@section('content')
	<div id="content" class="col-xs-12 col-sm-10">
		</br>
		<div class="box ui-draggable ui-droppable">
			<div class="box-header">
				<div class="box-name ui-draggable-handle">
					<i class="fa fa-users"></i>
					<span>Bookmii Notifications</span>
				</div>
				<div class="box-icons">
					
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content no-padding table-responsive">
				<form id="notification-form" method="post">
					 {{ csrf_field() }}
					<table class="table">
						<thead>
							<tr>
								<th rowspan="1" colspan="1">Keyword</th>
								<th rowspan="1" colspan="1">User Type</th>
								<th rowspan="1" colspan="1">Services</th>
								<th rowspan="1" colspan="1">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th rowspan="1" colspan="1">
								<input type="text" class="form-control search-control" name="search_text" value="{{$searchData['search_text']}}" />
								<input type="hidden"  id="sorting_field"  name="sorting_field" value="{{$searchData['sorting_field']}}"/>
								<input type="hidden"  id="sorting_order"  name="sorting_order" value="{{$searchData['sorting_order']}}"/>
								</th>
								<th rowspan="1" colspan="1">
									<select class="form-control search-control" name="user_type">
										<option value="">--Select Type--</option>
										@foreach ($notificationTypes as $typeId => $notificationType)
											<option value="{{$typeId }}" 
											@if($searchData['user_type'] && $searchData['user_type'] == $typeId) 
												selected 
											@endif>{{$notificationType}}</option>
										@endforeach
									</select>
								</th>
								<th rowspan="1" colspan="1">
									<select class="form-control search-control" name="service">
										<option value="">--Select Service--</option>
										@foreach ($services as $service)
											<option value="{{$service->id}}" 
											@if($searchData['service'] && $searchData['service'] == $service->id)
												selected 
											@endif>
											{{$service->service}}
											</option>
										@endforeach
									</select>
								</th>
								<th rowspan="1" colspan="1">
									<button type="submit" class="btn btn-primary btn-xs">Search</button>
									<button type="button" class="btn btn-danger btn-xs" onClick="clearData()">Canel</button>
								</th>
							</tr>
						</tbody>
					</table>
				</form>
				<table class="table table-bordered table-striped table-datatable" >
					<thead>
						<tr>
							<th rowspan="1" colspan="1">Notification ?</th>
							<th  class="{{ $getSortingClass('users.name') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('users.name', '{{ $getSortinOrder('users.name') }}' )" >Name</th>
							<th  class="{{ $getSortingClass('users.email') }}"  rowspan="1" colspan="1" onclick="setSortingField('users.email', '{{ $getSortinOrder('users.email') }}')">Email</th>
							<th  class="{{ $getSortingClass('users.type') }}"  rowspan="1" colspan="1" onclick="setSortingField('users.type',  '{{ $getSortinOrder('users.type') }}')">User Type</th>
							<th  class="{{ $getSortingClass('services.service') }}"  rowspan="1" colspan="1" onclick="setSortingField('services.service',  '{{ $getSortinOrder('services.service') }}')" >Services</th>
							<th rowspan="1" colspan="1">Action</th>
						</tr>
					</thead>

					@if($notifications->count())
					<tbody>
						@foreach ($notifications as $notification)
					        <tr>
								<td>
									@if($notification->notification)
										<span class="checkbox">
											<label>
												<input type="checkbox" checked="true">
												<i class="fa fa-square-o"></i>
											</label>
										</span>
									@else
										<span class="checkbox">
											<label>
												<input type="checkbox" checked="false">
												<i class="fa fa-square-o"></i>
											</label>
										</span>
									@endif
								</td>
								<td>{{$notification->name}}</td>
								<td>{{$notification->email}}</td>
								<td>{{$notification->type}}</td>
								<td>{{$notification->service}}</td>
								<td>
									<button class="btn btn-primary btn-xs">&nbsp; Send &nbsp;</button>
                               </td>
							</tr>
					    @endforeach				
					</tbody>
					<tfoot>
						<tr>
							<th rowspan="1" colspan="1">Is Active?</th>
							<th  class="{{ $getSortingClass('users.name') }}"  
								rowspan="1" 
								colspan="1" 
								onclick="setSortingField('users.name', '{{ $getSortinOrder('users.name') }}' )" >Name</th>
							<th  class="{{ $getSortingClass('users.email') }}"  rowspan="1" colspan="1" onclick="setSortingField('users.email', '{{ $getSortinOrder('users.email') }}')">Email</th>
							<th  class="{{ $getSortingClass('users.type') }}"  rowspan="1" colspan="1" onclick="setSortingField('users.type',  '{{ $getSortinOrder('users.type') }}')">User Type</th>
							<th  class="{{ $getSortingClass('services.service') }}"  rowspan="1" colspan="1" onclick="setSortingField('services.service',  '{{ $getSortinOrder('services.service') }}')" >Services</th>
							<th  rowspan="1" colspan="1">Action</th>
						</tr>
					</tfoot>
					@else
						<tbody>
							<th colspan="6"><div align="center"><b>Record Not Found</b></div></th>
						</tbody>
					@endif
				</table>
				<div class="box-content pull-right">
					{{$notifications->links()}}
				</div>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	function setSortingField(field, order) {
		$("#sorting_field").val(field);
		$("#sorting_order").val(order);
		$("#notification-form").submit();
	}
</script>
@endsection

