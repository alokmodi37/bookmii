@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                
               		<div style="color:red">{{$message}}</div>
                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ route('uploadImage', ['token' => $token]) }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('repassword') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Retype Password</label>

                            <div class="col-md-6">
                                <input id="picture" type="file" class="form-control" name="picture" >
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Upload
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection