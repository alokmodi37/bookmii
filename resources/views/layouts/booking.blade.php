<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('public/theme/plugins/bootstrap/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
    <link href="{{ asset('public/theme/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/xcharts/xcharts.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/select2/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/justified-gallery/justifiedGallery.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/css/style_v2.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/chartist/chartist.min.css') }}" rel="stylesheet">
    

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
	<div id="main">
		@yield('content')
	</div>

    <!-- Scripts -->
    <script src="{{ asset('public/theme/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('public/theme/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('public/theme/plugins/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/theme/plugins/justified-gallery/jquery.justifiedGallery.min.js') }}"></script>
    <script src="{{ asset('public/theme/plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('public/theme/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
    <!-- All functions for this theme + document.ready processing -->
    <script src="{{ asset('public/theme/js/devoops.js') }}"></script>
</body>
</html>
