<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('public/theme/plugins/bootstrap/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
    <link href="{{ asset('public/theme/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/xcharts/xcharts.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/select2/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/justified-gallery/justifiedGallery.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/css/style_v2.css') }}" rel="stylesheet">
    <link href="{{ asset('public/theme/plugins/chartist/chartist.min.css') }}" rel="stylesheet">
    

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>

	<header class="navbar">
	<div class="container-fluid expanded-panel">
		<div class="row">
			<div id="logo" class="col-xs-12 col-sm-2">
				<a href="index.html">Bookmii Admin</a>
			</div>
			<div id="top-panel" class="col-xs-12 col-sm-10">
				<div class="row">
					<div class="col-xs-8 col-sm-4">
						&nbsp;
					</div>
					<div class="col-xs-4 col-sm-8 top-panel-right">
						<ul class="nav navbar-nav pull-right panel-menu">
							
							<li id="logo">
								<a href="#" class="dropdown-toggle account" data-toggle="dropdown">
									<div class="user-mini pull-right">
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						                    {{ csrf_field() }}
						                </form>
										 <h3 onclick="event.preventDefault();
					                                 document.getElementById('logout-form').submit();">Logout</h3>
					                    
									</div>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="main" class="container-fluid">
	<div class="row">
		<div id="sidebar-left" class="col-xs-2 col-sm-2">
			<ul class="nav main-menu">
				<li>
					<a href="{{ route('listUser') }}" @if($currentPanel == 'user') class="active-parent active" @endif>
						<i class="fa fa-users"></i>
						<span class="hidden-xs">Manage Users</span>
					</a>
				</li>
				<li>
					<a href="{{ route('listBooking') }}" @if($currentPanel == 'booking') class="active-parent active" @endif>
						<i class="fa fa-ticket"></i>
						<span class="hidden-xs">Manage Booking</span>
					</a>
				</li>
				<li>
					<a href="{{ route('listPayment') }}"  @if($currentPanel == 'payment') class="active-parent active" @endif>
						<i class="fa fa-credit-card"></i>
						<span class="hidden-xs">Manage Payment</span>
					</a>
				</li>
				<!--<li>
					<a href="{{ route('listNotificationSetting') }}"  @if($currentPanel == 'notification') class="dropdown-toggle active-parent  active" @else  
					class="dropdown-toggle" @endif >
						<i class="fa fa-bell"></i>
						<span class="hidden-xs">Manage Notificaiton</span>
					</a>
					<ul class="dropdown-menu" @if($currentPanel == 'notification') style="display:block" @endif>
						<li><a @if($currentSubPanel == 'setting') class="active-parent active" @endif href="{{ route('listNotificationSetting') }}">List Setting</a></li>
						<li><a @if($currentSubPanel == 'notification') class="active-parent active" @endif href="{{ route('listNotification') }}">List Notification</a></li>
					</ul>
				</li>-->
				<li>
					<a href="{{ route('listContent') }}"  @if($currentPanel == 'content') class="active-parent  active" @endif >
						<i class="fa fa-copy"></i>
						<span class="hidden-xs">Manage Content</span>
					</a>
				</li>
			</ul>
   		</div>
		@yield('content')
	</div>
       
    </div>

    <!-- Scripts -->
    <script src="{{ asset('public/theme/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('public/theme/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('public/theme/plugins/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/theme/plugins/justified-gallery/jquery.justifiedGallery.min.js') }}"></script>
    <script src="{{ asset('public/theme/plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('public/theme/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
    <!-- All functions for this theme + document.ready processing -->
    <script src="{{ asset('public/theme/js/devoops.js') }}"></script>
</body>
</html>
