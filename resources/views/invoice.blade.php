<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Invoice</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
body {
  margin: 0px;
  padding: 0px;
  font-family: Arial;
    color: #fff; font-size: 15px; background-image: url('{{ asset('public/images/bg.jpg') }}'); background-position: center top; background-repeat: repeat-x;
}
p {
  margin: 0px;
  padding: 0px;
}
#wrapper {
  padding: 30px 8px;
}
#header { width:100%; float: left; display: block; padding-bottom:10px; }
 
  #footer{ padding-top:10px; font-size: 10px;}
#logo {
  font-size: 30px;  
  font-weight: bold;
  margin-bottom: 10px; text-align: center;  position: relative;margin-top: -70px; margin-bottom: -50px;
}
  .head-right{padding:0 8px; }
.head-right p {
  margin-bottom: 5px; text-align: right;
}
.head-right span {
  width: 150px;
  float: left; text-align: left;
} 
table {
  border-collapse: collapse;
}
table tr{
  border-bottom: white solid 2px;
  }
table tr:last-child{ border-bottom:none;} 
table th{
  background-color: white;
  padding: 10px 8px; color: #ec727e;
}
tfoot td {
  background-color: white; color: #ec727e;
} 
table td.col {
  padding: 10px 8px;
 }
table strong {
  display: block;
  padding-bottom: 5px;
}
.total{ float:right; }
.total span{ float:left; width:100px; } 
</style>
</head>

<body>
<div id="wrapper">
  <div id="header">
    <div id="logo"><img src="{{ asset('public/images/bookmi.png') }}" alt=""></div>
    <!--logo-->
    <div class="head-right">
      <p><span>Invoice Date</span>{{ $payment->invoice_date->format('d-M-Y h:i A') }}</p>
      <p><span>Client Name</span>{{ $payment->client_name }}</p>
      <p><span>Service Date</span>{{ $payment->booking_date->format('d-M-Y h:i A') }}</p>
    </div>
    <!--head-right--> 
  </div>
  <!--header-->
  <div id="content">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
     <thead>
     	<tr>
          <th align="left" valign="middle">Service</th>
          <th align="right" valign="middle">Amount</th>
        </tr>
     </thead>
      <tbody>
        <tr>
          <td align="left" valign="middle" class="col"><strong>{{ $payment->service }}</strong> {{ round($payment->duration, 2) }} x ${{ $payment->charges }} </td>
          <td align="right" valign="middle" class="col">${{ $payment->amount }}</td>
        </tr>
        <tr>
          <td align="left" valign="middle" class="col"><strong>Service Charges</strong>  x 8% </td>
          <td align="right" valign="middle" class="col">${{ ($payment->amount * 8) / 100 }}</td>
        </tr>
        <tr>
               <td align="left" valign="middle" width="40%" class="col"></td>
           <td colspan="1" align="right" valign="middle" width="60%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td align="right" valign="middle" class="col">Sub Total:</td>
                  <td align="right" valign="middle" class="col">${{ $payment->amount + (($payment->amount * 10) / 100) }}</td>
                </tr>
                </tbody>
            </table></td>
        </tr>
      </tbody>
      <tfoot>
      	 <tr>
               <td align="left" valign="middle" class="col"  width="40%"></td>
           <td colspan="1" align="right" valign="middle" class="col"  width="60%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                  <td align="right" valign="middle">Total:</td>
                  <td align="right" valign="middle">${{ $payment->amount + (($payment->amount * 10) / 100) }}</td>
                </tr>
            </table></td>
        </tr>
      </tfoot>
    </table>
  </div>
  <!--content--> 
  <div id="footer">
  	<h4>Terms & Conditions applied*</h4>
   </div><!--footer-->
 </div>
<!--wrapper-->

</body>
</html>