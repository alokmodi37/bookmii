<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ChatHead;

use J42\LaravelFirebase\LaravelFirebaseFacade as FirebaseApi;

class Chat extends Model
{
    protected $connection = 'firebase';

    public static function getFirebaseIdentifier($senderOne, $senderTwo){
        $charHeadRow = ChatHead::where(function($query) use($senderOne, $senderTwo){
                $query->where('sender_one', $senderOne->id)
                        ->where('sender_two', $senderTwo->id);
                })
                ->orWhere(function($query) use($senderOne, $senderTwo){
                    $query->where('sender_one',  $senderTwo->id)
                            ->where('sender_two', $senderOne->id);
                })
                ->select('firebase_identifier')
                ->first();

        
        if(!$charHeadRow){
            $firebase_identifier = md5($senderOne->email . "-" . $senderTwo->email);

            ChatHead::insert([
                'sender_one' => $senderOne->id,
                'sender_two' => $senderTwo->id,
                'firebase_identifier' => $firebase_identifier
            ]);


            return ['firebase' => $firebase_identifier, 'initialize' => true];
        } else {

            $chatData = Chat::getChat($charHeadRow['firebase_identifier']); 

            if( $chatData == 'null' ){

                $initialize = true;
            }else{

                $initialize = false;
            }

            return ['firebase' => $charHeadRow->firebase_identifier, 'initialize' => $initialize];
        }
    }

    
    public static function initializeChat($param) {
        $firebase_identifier = $param['firebase_identifier'];
        unset($param['firebase_identifier']);
        
        $data = FirebaseApi::set('/chathead/' . $firebase_identifier, $param);
    }

    public static function updateTyping($param) {
        $firebase_identifier = $param['firebase_identifier'];
        unset($param['firebase_identifier']);

        $data = FirebaseApi::set('/chathead/' . $firebase_identifier . '/typingindicator', $param);
        return Chat::getMessage($firebase_identifier);
    }


    public static function sendMessage($param) {
        $firebase_identifier = $param['firebase_identifier'];
        unset($param['firebase_identifier']);

        foreach($param as $indicator => $data) {
             $data = FirebaseApi::set('/chathead/' . $firebase_identifier . '/messages/' .  $indicator, $data);
        }
       
        return Chat::getMessage($firebase_identifier);
    }

    public static function getSetting($firebase_identifier) {
        $data = FirebaseApi::get('/chathead/' . $firebase_identifier . '/setting');
        return ($data->getContents());
    }

    public static function getMessage($firebase_identifier, $lastMessageId = null) {

        $firebaseUrl = '/chathead/' . $firebase_identifier . '/messages';

        if($lastMessageId) {
            $firebaseUrl .= "/" . $lastMessageId;
        }

        
        $data = FirebaseApi::get($firebaseUrl);

        return ($data->getContents());
    }

    public static function getChat($firebase_identifier) {
        $data = FirebaseApi::get('/chathead/' . $firebase_identifier);
        return ($data->getContents());
    }


    public static function getLastMessage($firebase_identifier){
        $firebaseUrl = '/chathead/' . $firebase_identifier . '/lastMessageId';
        $lastMessageId = FirebaseApi::get($firebaseUrl)->getContents();
        $lastMessageId = str_replace('"', '', $lastMessageId);
       
        return self::getMessage($firebase_identifier, ($lastMessageId && $lastMessageId != 'null') ? $lastMessageId : 0);
    }

}
