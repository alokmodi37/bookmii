<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserService extends Authenticatable
{
    use Notifiable;
    public $timestamps = false;
    
}