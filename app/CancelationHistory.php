<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CancelationHistory extends Authenticatable
{
    use Notifiable;

    protected $table = 'cancelation_history';

    protected $fillable = [
        'booking_id', 'canceled_by', 'charge_amount', 'is_charge_applicable', 'is_active'
    ];

    protected $hidden = ['is_active', 'created_at', 'updated_at'];
}
