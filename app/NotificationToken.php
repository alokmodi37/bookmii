<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class NotificationToken extends Authenticatable
{
    use Notifiable;

    protected $table = 'notification_token';

    protected $fillable = [
        'user_id', 'firebase_token'
    ];

    protected $hidden = ['user_id', 'firebase_token'];
}
