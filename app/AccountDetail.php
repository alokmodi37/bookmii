<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AccountDetail extends Authenticatable
{
    use Notifiable;

    protected $table = 'account_detail';

    protected $fillable = [
        'user_id', 'account_number', 'transit_number', 'branch_number'
    ];

    protected $hidden = ['is_active', 'created_at', 'updated_at'];
    public $timestamps = true;
}
