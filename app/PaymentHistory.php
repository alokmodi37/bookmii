<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PaymentHistory extends Authenticatable
{
    use Notifiable;

    protected $table = 'payment_history';
    public $timestamps = true;

    protected $fillable = [
        'user_id', 'booking_id', 'payment_token', 'amount'
    ];
}
