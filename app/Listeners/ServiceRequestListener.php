<?php

namespace App\Listeners;

use App\Events\ServiceRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class ServiceRequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServiceRequest  $event
     * @return void
     */
    public function handle(ServiceRequest $event)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Booking Request');
        $notificationBuilder->setBody($this->createMessage($event->booking))
                            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($event->booking->other);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $event->booking->firebase_token;

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
    }

    public function createMessage($notifiable) {

         return "You have a booking request from " . $notifiable->client_name . " on : " . $notifiable->start_date . " at " . $notifiable->start_time;
    }
}
