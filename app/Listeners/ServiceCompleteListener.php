<?php

namespace App\Listeners;

use App\Events\ServiceComplete;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
class ServiceCompleteListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServiceComplete  $event
     * @return void
     */
    public function handle(ServiceComplete $event)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Booking Finished');
        $notificationBuilder->setBody($this->createMessage($event->booking))
                            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([$event->booking->other]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $event->booking->firebase_token;

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
    }

    public function createMessage($notifiable) {
         if($notifiable->notification_for == 'user') {
            return "You have a used the service of " . $notifiable->provider_name . " on : " . $notifiable->start_date . " at " . $notifiable->start_time;
         } else {
            return "You have a provided services to " . $notifiable->client_name . " on : " . $notifiable->start_date . " at " . $notifiable->start_time;
         }

    }
}
