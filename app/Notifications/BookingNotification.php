<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BookingNotification extends Notification
{
    use Queueable;
    public $emailParams;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($emailParams)
    {
       $this->emailParams = $emailParams;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = "";
        switch(strtoupper($this->emailParams->requestFor)) {
            case "CANCEL" :  $message = $this->createCancelMessage($notifiable); break;
            case "BOOKING" :  $message = $this->createBookingMessage($notifiable);break;
            case "CONFIRM" :  $message = $this->createConfirmMessage($notifiable); break;
        }
       
        return (new MailMessage)
                    ->greeting('Hello, ' . title_case($notifiable->name))
                    ->line($message)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function createCancelMessage($notifiable) {
         return "Booking request on Data - " . $this->emailParams->start_date . " from " . $this->emailParams->start_time . " to " . $this->emailParams->end_time . " has been cancelled by " . title_case($this->emailParams->rejectedBy);
    }

    public function createConfirmMessage($notifiable) {
         return "Your booking request has been confirmed by service provider "  .  title_case($this->emailParams->provider_name) . " on Data - " . $this->emailParams->start_date . " from " . $this->emailParams->start_time . " to " . $this->emailParams->end_time;
    }


    public function createBookingMessage($notifiable) {
         return ($notifiable->type == 'provider') ? "You have a booking request from user " . title_case($this->emailParams->client_name) . " on Data - " . $this->emailParams->start_date . " between " . $this->emailParams->start_time . " - " . $this->emailParams->end_time : 
            "You have requested services to service provider " .  title_case($this->emailParams->provider_name) . " on Data - " . $this->emailParams->start_date . " between " . $this->emailParams->start_time . " - " . $this->emailParams->end_time;
    }
}
