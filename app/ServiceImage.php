<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ServiceImage extends Authenticatable
{
    use Notifiable;

    protected $table = 'service_images';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = [
        'profile_id', 'image_path'
    ];

    protected $hiden = ['id', 'profile_id', 'is_active'];

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }
}