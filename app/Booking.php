<?php

namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Booking extends Authenticatable
{
    use Notifiable;

    protected $table = 'bookings';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   
    protected $fillable = [
        'provider_id', 'client_id', 'charges', 'start_date', 'start_time', 'end_date', 'end_time', 'is_active'
    ];
    
    static public function getDatePart($dateTime) {
        $startDateTime = Carbon::parse($dateTime);
        $datePart = [$startDateTime->year, $startDateTime->month, $startDateTime->day];
        return implode("-", $datePart);
    }

    static public function getTimePart($dateTime) {
        $startDateTime = Carbon::parse($dateTime);
        return ( $startDateTime->hour . ":" .  (($startDateTime->minute) ? $startDateTime->minute : '00') . ":" .  (($startDateTime->second) ? $startDateTime->second : '00') );
    }

    static public function isAvailable($providerId, $bookingStartDate, $bookingEndDate) {
        $startDate = Date('Y-m-d', strtotime($bookingStartDate));
        
        $sub = Booking::select(DB::raw("(concat(bookings.start_date, ' ', bookings.start_time)) as start_date_time"), DB::raw("(concat(bookings.end_date, ' ', bookings.end_time)) as end_date_time"), 'bookings.*')
            ; 

        return DB::table( DB::raw("({$sub->toSql()}) as b") )
                        ->where(function($query) use($bookingStartDate, $bookingEndDate){
                        $query->whereBetween('b.start_date_time',[$bookingStartDate, $bookingEndDate])
                              ->orWhereBetween('b.end_date_time',[$bookingStartDate, $bookingEndDate]);
                        })
                        ->where('b.start_date', $startDate)
                        ->where('b.provider_id', $providerId)
                        ->whereIn('b.status', ['CONFIRM'])
                        ->where('is_active', '1')
                        ->count();
    }
}