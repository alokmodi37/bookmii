<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatHead extends Model
{
    protected $table = 'chat_head';

    protected $fillable = [
        'sender_one', 'sender_two', 'firebase_identifier',
    ];
}
