<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => '',
            'display_name' => '',
            'email' => 'email',
            'password' => '',
            'facebook_token' => '',
            'profile_image' => '',
            'address' => '',
            'latitude' => '',
            'longitude' => ''
        ];
    }
}
