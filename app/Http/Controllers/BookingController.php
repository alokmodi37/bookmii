<?php

namespace App\Http\Controllers;

use JWTAuth;
use JWTAuthException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\BookingRequest as BookingRequest;

use App\Booking as Booking;
use App\User as User;
use App\Profile as Profile;
use App\CancelationHistory as CancelationHistory;
use App\NotificationToken as NotificationToken;
use App\Notification;
use App\PaymentHistory;
use App\CardDetail;

// Email notification section
use App\Notifications\BookingNotification as BookingNotification;
use App\Events\ServiceRequest;
use App\Events\ServiceComplete;
use App\Events\ServiceEvent;

use Carbon\Carbon;

/**
 * @resource Booking
 *
 * Booking section has all the related API related to Booking, Availability check etc
 */
class BookingController extends Controller
{
    public function getCurrentUser($request) {
		    return JWTAuth::toUser($request->token);
  	}

  /**
    * Check Provider Availability API
    */
	public function isAvailable(Request $request) {
		//$user = $this->getCurrentUser($request)->id;
		try {
			$validation = Validator::make($request->all(), [
	            'provider_id'   => 'required',
	            'start_date'    => 'required',
	            'end_date'      => 'required'
	        ]);

	        if($validation->fails()){
	           $errors = $validation->errors();

	           return response()->json(['error_code'=>500,'msg_string'=> __('messages.VALIDATION_REQUIRED'), 'result' => $errors->toJson()]);

	        } else {
				$bookingData = $request->only('provider_id', 'start_date', 'end_date');

				if(Booking::isAvailable($bookingData['provider_id'], $bookingData['start_date'], $bookingData['end_date'])) {
					return response()->json(['error_code'=>200,'msg_string'=> __('messages.BOOKING_NOT_AVAILABLE'), 'result' => []]);
				} else {
					return response()->json(['error_code'=>200,'msg_string'=> __('messages.BOOKING_AVAILABLE'), 'result' => []]);
				}
			}
		} catch (Exception $e) {
			return response()->json(['error_code'=>500,'msg_string'=> __('messages.PROBLEM_OCCURED'), 'result' => $e->toMessage()]);
		}

	}


	/**
    * Book Provider API
    */
	public function bookProvider(BookingRequest $request) {
		try{
			$validation = Validator::make($request->all(), [
	            'provider_id'   => 'required',
	            'start_date'    => 'required',
	            'end_date'      => 'required'
	        ]);

	        if($validation->fails()){
	           $errors = $validation->errors();

	           return response()->json(['error_code'=>500,'msg_string'=> __('messages.VALIDATION_REQUIRED'), 'result' => $errors->toJson()]);

	        } else {
		        $bookingData = $request->only('provider_id', 'start_date', 'end_date');

	        	if(!Booking::isAvailable($bookingData['provider_id'], $bookingData['start_date'], $bookingData['end_date'])) {

	        		$bookingData['start_time'] 	= Booking::getTimePart($bookingData['start_date']);
		        	$bookingData['end_time'] 	= Booking::getTimePart($bookingData['end_date']);

		        	$bookingData['start_date'] 	= Booking::getDatePart($bookingData['start_date']);
		        	$bookingData['end_date'] 	= Booking::getDatePart($bookingData['end_date']);
		        	$bookingData['is_active'] 	= 0;

					$client = $this->getCurrentUser($request);
					$provider = User::where('id', $bookingData['provider_id'])->first();

					$bookingData['client_id'] = $client->id;

					//Create Booking Request
					$booking = Booking::create($bookingData);

					$bookingRow = Booking::where('id', $booking->id)
										->select(DB::raw("time_to_sec(timediff(concat(bookings.end_date, ' ', bookings.end_time), concat(bookings.start_date,' ', bookings.start_time) )) / 3600 as duration"), 'bookings.*')
										->first();

					$charges = Profile::where('user_id', $provider->id)->select('charges')->first();

					$totalFee = ($charges->charges * $bookingRow->duration);

					$bookingRow->charges = $totalFee + (($totalFee * 8) / 100);
					$bookingRow->update();

					return response()->json(['error_code'=>200,'msg_string'=> __('messages.SUCCESS_BOOKING'),
						'result' => $bookingRow,
						'token' => \Braintree_ClientToken::generate(),
						'credit_card' => CardDetail::where('user_id', $client->id)->first()
					]);
				} else {
					return response()->json(['error_code'=>200,'msg_string'=> __('messages.BOOKING_NOT_AVAILABLE'), 'result' => []]);
				}
			}
		} catch(Exception $e) {
			return response()->json(['error_code'=>500,'msg_string'=> __('messages.PROBLEM_OCCURED'), 'result' => $e->toMessage()]);
		}
	}

	/**
    * Cancel Booking API
    */
	public function cancelBooking(Request $request) {
		try {
			$client = $this->getCurrentUser($request);
			$bookingId = $request->get('booking_id', null);

			if($bookingId) {
				$bookingQuery = Booking::where('id', $bookingId);

				if($client->type == 'customer') {
					$bookingQuery = $bookingQuery->where('client_id', $client->id)
												 ->select(DB::raw("time_to_sec(timediff(now(), concat(bookings.start_date,' ', bookings.start_time) )) / 3600 as duration"), 'bookings.*');
				} else {
					$bookingQuery = $bookingQuery->where('provider_id', $client->id)
												 ->select(DB::raw("time_to_sec(timediff(now(), concat(bookings.start_date,' ', bookings.start_time) )) / 3600 as duration"), 'bookings.*');
				}

				$bookingDetail = $bookingQuery->first();
				if ($bookingDetail && in_array($bookingDetail->status, ['PENDING', 'CONFIRM'])) {
					$bookingQuery->update(['status' => 'REJECTED', 'is_active' => 0]);

					$isChargeApplicable = 0;
					$chargeAmount = 0;

					if($client->type == 'customer' && $bookingDetail->status == 'CONFIRM') {
						if($bookingDetail->duration < 24) {
							$isChargeApplicable = 1;
							$chargeAmount = $bookingDetail->charges / 2;
						}
					}

					CancelationHistory::create([
						'booking_id' => $bookingId,
						'canceled_by' => $client->id,
						'charge_amount' => $chargeAmount,
						'is_charge_applicable' => $isChargeApplicable
					]);

					$provider = User::where('id', ($client->type == 'customer') ? $bookingDetail['provider_id'] : $bookingDetail['client_id'])				->first();

					if($client->type == 'customer') {
						$bookingDetail->client_name = $client->name;
						$bookingDetail->provider_name = $provider->name;
					} else {
						$bookingDetail->client_name = $provider->name;
						$bookingDetail->provider_name = $client->name;
					}


					$bookingDetail->requestFor = "Cancel";
					$bookingDetail->rejectedBy = $client->name;

					// Email to client and provider
					$client->notify(new BookingNotification($bookingDetail));
					$provider->notify(new BookingNotification($bookingDetail));


					$notificationMessage = [
				 		'title' => 'Booking Cancel',
				 		'body' => 'Booking has been cancelled by ' . $bookingDetail->rejectedBy,
				 		'other' => [
				 			  'type' => 'cancelation',
                'client_id' => $client->id,
                'provider_id' => $provider->id,
                'booking_id' => $bookingId,
                'canceled_by' => $client->id
				 		]
				 	];
					$this->sendNotification($bookingDetail, $client->id, $notificationMessage);
					$this->sendNotification($bookingDetail, $provider->id, $notificationMessage);

				} else {
					return response()->json(['error_code'=>500,'msg_string'=> __('messages.INVALID_REQUEST'), 'result' => []]);
				}

				return response()->json(['error_code'=>200,'msg_string'=> __('messages.MEETING_CANCEL'), 'result' => []]);
			} else {
				return response()->json(['error_code'=>500,'msg_string'=> __('messages.PROBLEM_OCCURED'), 'result' => []]);
			}

		} catch (Exception $e) {
			return response()->json(['error_code'=>500,'msg_string'=> __('messages.PROBLEM_OCCURED'), 'result' => $e->toMessage()]);
		}
	}


	public function sendNotification($bookingDetail, $userId, $notificationMessage = []) {
		$notificationSetting = Notification::where('user_id', $userId)->first();
		if( $notificationSetting && ($notificationSetting->notification == 1)) {
				$notificationToken = NotificationToken::where('user_id', $userId)->select('firebase_token')->first();

				if( $notificationToken ){
				 	$bookingDetail->firebase_token = $notificationToken->firebase_token;
				 	event(new ServiceEvent($bookingDetail, $notificationMessage));
				}

		}
	}

	/**
    * Confirm Booking API
    */
	public function confirmBooking(Request $request) {
		try {
			$provider = $this->getCurrentUser($request);

			if($provider->type == 'customer') {
				return response()->json([
					'error_code'=>500,
					'msg_string'=> __('messages.UNAUTHORIZE_REQUEST'),
					'result' => []]);
			}

			$bookingId = $request->get('booking_id', null);

			if($bookingId) {
				$bookingQuery = Booking::where('id', $bookingId);
				$bookingDetail = $bookingQuery->first();

				if ($bookingDetail && $bookingDetail->status == 'PENDING') {
					$bookingQuery->update(['status' => 'CONFIRM']);

					$client = User::where('id', $bookingDetail['client_id'])->first();

					$bookingDetail->provider_name = $provider->name;
					$bookingDetail->client_name = $client->name;

					$bookingDetail->requestFor = "Confirm";
					$emailBookingDetail = $bookingDetail;

					$client->notify(new BookingNotification($emailBookingDetail));

					$notificationMessage = [
				 		'title' => 'Booking Confirmation',
				 		'body' => 'Booking has been confirmed by ' . $bookingDetail->provider_name,
				 		'other' => [
				 			'type' => 'confirmation',
              'client_id' =>  $client->id,
              'booking_id' => $bookingId
				 		]
				 	];
					$this->sendNotification($bookingDetail, $client->id, $notificationMessage);

				} else {
					return response()->json(['error_code'=>500,'msg_string'=> __('messages.INVALID_REQUEST'), 'result' => []]);
				}

				return response()->json(['error_code'=>200,'msg_string'=> __('messages.MEETING_CONFIRM'), 'result' => []]);
			} else {
				return response()->json(['error_code'=>500,'msg_string'=> __('messages.PROBLEM_OCCURED'), 'result' => []]);
			}

		} catch (Exception $e) {
			return response()->json(['error_code'=>500,'msg_string'=> __('messages.PROBLEM_OCCURED'), 'result' => $e->toMessage()]);
		}
	}

	/**
    * Confirm Booking API
    */
	public function finishBooking(Request $request) {
		try {
			$client = $this->getCurrentUser($request);
			$bookingId = $request->get('booking_id', null);

			if($bookingId) {
				$bookingQuery = Booking::where('id', $bookingId);
				$bookingDetail = $bookingQuery->first();



				if ($bookingDetail) {
					$bookingQuery->update(['status' => 'FINISHED']);

					$client = $this->getCurrentUser($request);
					$provider = User::where('id', $bookingDetail['provider_id'])->first();

					$bookingDetail['client_id'] = $client->id;

					$bookingDetail->client_name = $client->name;
					$bookingDetail->provider_name = $provider->name;

					// Notification to client and provider
					//$client->notify(new BookingNotification($booking));
					//$provider->notify(new BookingNotification($booking));

					$notificationSetting = Notification::where('user_id', $bookingDetail->provider_id)->first();
					if( $notificationSetting && $notificationSetting->notification) {

						$notificationToken = NotificationToken::where('user_id', $bookingDetail->provider_id)->select('firebase_token')->first();

						if( $notificationToken ){
							$bookingDetail->notification_for = "provider";
						 	$bookingDetail->firebase_token = $notificationToken->firebase_token;
						 	event(new ServiceComplete($bookingDetail));
						}
					}

					$notificationSetting = Notification::where('user_id', $bookingDetail->client_id)->first();
					if( $notificationSetting && $notificationSetting->notification) {

						$notificationToken = NotificationToken::where('user_id', $bookingDetail->client_id)->select('firebase_token')->first();

						if( $notificationToken ){
							$bookingDetail->notification_for = "user";
              $bookingDetail->other = [
                  'type' => 'finished',
                  'client_id' => $client->id,
                  'provider_id' => $provider->id,
                  'booking_id' => $bookingDetail->id
              ];
						 	$bookingDetail->firebase_token = $notificationToken->firebase_token;
						 	event(new ServiceComplete($bookingDetail));
						}
					}

				} else {
					return response()->json(['error_code'=>500,'msg_string'=> __('messages.INVALID_REQUEST'), 'result' => []]);
				}

				return response()->json(['error_code'=>200,'msg_string'=> __('messages.MEETING_CONFIRM'), 'result' => []]);
			} else {
				return response()->json(['error_code'=>500,'msg_string'=> __('messages.PROBLEM_OCCURED'), 'result' => []]);
			}

		} catch (Exception $e) {
			return response()->json(['error_code'=>500,'msg_string'=> __('messages.PROBLEM_OCCURED'), 'result' => $e->toMessage()]);
		}
	}

	/**
    * List Booking API
    */
	public function showBooking(Request $request) {
		try{
		  $client = $this->getCurrentUser($request);



		  $bookingType = $request->get('type', 'current');

		  switch(strtoupper($bookingType)) {
		  	case 'CURRENT'  :  $status = ['PENDING', 'CONFIRM']; break;
		  	case 'PAST'		:  $status = ['FINISHED']; break;
		  }

		  if($client->type == 'customer') {
		  	$bookingRowset = Booking::join('users', 'users.id', '=', 'bookings.provider_id')
		  								->join('profile', 'profile.user_id', '=', 'users.id')
		  								->join('services', 'services.id', '=', 'profile.service_id')
							  			->where('bookings.client_id', $client->id)
							  			->where('bookings.is_active', 1)
							  			->whereIn('bookings.status', $status)
							  			->orderBy('bookings.start_date', 'asc')
							  			->orderBy('bookings.start_time', 'asc')
							  			->select('users.id', 'users.name', 'bookings.id as booking_id', 'bookings.start_date', 'bookings.start_time', 'bookings.end_date', 'bookings.end_time', 'bookings.status', 'bookings.is_active', 'profile.charges', 'profile.latitude', 'profile.longitude', 'profile.address',  'users.profile_image', 'services.service', DB::raw('round(time_to_sec(timediff( concat(bookings.end_date, " ", bookings.end_time), concat(bookings.start_date, " ", bookings.start_time))) / 3600) AS total_hour'));

										if($bookingType == 'CURRENT') {
											$bookingRowset = $bookingRowset->where('bookings.start_date', '>=', DB::raw('now()'));
										}

							  			$bookingRowset = $bookingRowset->get();
		  } else {

		  	$userProfile = Profile::join('services', 'services.id', '=', 'profile.service_id')
		  					->where('profile.user_id', $client->id)
		  					->first();


		  	$bookingRowset = Booking::join('users', 'users.id', '=', 'bookings.client_id')
		  								->join('profile', 'profile.user_id', '=', 'users.id')
		  								->join('services', 'services.id', '=', 'profile.service_id')
							  			->where('bookings.provider_id', $client->id)
							  			->where('bookings.is_active', 1)
							  			->whereIn('bookings.status', $status)
							  			->orderBy('bookings.start_date', 'asc')
							  			->orderBy('bookings.start_time', 'asc')
							  			->select('users.id', 'users.name', 'bookings.id as booking_id', 'bookings.start_date', 'bookings.start_time', 'bookings.end_date', 'bookings.end_time', 'bookings.status', 'bookings.is_active', 'profile.charges', 'profile.latitude', 'profile.longitude', 'profile.address', 'users.profile_image', DB::raw('"' .$userProfile->service . '" as service') , DB::raw('round(time_to_sec(timediff( concat(bookings.end_date, " ", bookings.end_time), concat(bookings.start_date, " ", bookings.start_time))) / 3600) AS total_hour'));
			if($bookingType == 'CURRENT') {
						$bookingRowset = $bookingRowset->where('bookings.start_date', '>=', DB::raw('now()'));
			}
			$bookingRowset = $bookingRowset->get();

		 }

		  $responseArray = [];


		  foreach($bookingRowset->toArray() as $bookingRow) {
		  	if( !isset($responseArray[$bookingRow['start_date']]) ) {
		  		$responseArray[$bookingRow['start_date']] = [];
		  	}

		  	$responseArray[$bookingRow['start_date']][] = $bookingRow;
		  }

		  $response = [];

		  foreach($responseArray as $value){
		  		$response[] = $value;
		  }

		  return response()->json(['error_code'=>200,'msg_string'=> (count($response)) ? __('messages.BOOKING_DETAIL') : __('messages.NO_BOOKING'), 'result' => $response]);

		} catch(Exception $e) {
			return response()->json(['error_code'=>500,'msg_string'=> __('messages.PROBLEM_OCCURED'), 'result' => $e->toMessage()]);
		}
	}

	public function invoice(Request $request){
		try{
			$validation = Validator::make($request->all(), [
	            'provider_id'   => 'required',
	            'start_date'    => 'required',
	            'end_date'      => 'required'
	        ]);

	        if($validation->fails()){
	           $errors = $validation->errors();

	           return response()->json(['error_code'=>500,'msg_string'=> __('messages.VALIDATION_REQUIRED'), 'result' => $errors->toJson()]);

	        } else {
		        $bookingData = $request->only('provider_id', 'start_date', 'end_date');

	        	if(!Booking::isAvailable($bookingData['provider_id'], $bookingData['start_date'], $bookingData['end_date'])) {

	        		$bookingData['start_time'] 	= Booking::getTimePart($bookingData['start_date']);
		        	$bookingData['end_time'] 	= Booking::getTimePart($bookingData['end_date']);

		        	$bookingData['start_date'] 	= Booking::getDatePart($bookingData['start_date']);
		        	$bookingData['end_date'] 	= Booking::getDatePart($bookingData['end_date']);
		        	$bookingData['is_active'] 	= 0;

					$client = $this->getCurrentUser($request);
					$provider = User::where('id', $bookingData['provider_id'])->first();

					$bookingData['client_id'] = $client->id;

					$bookingRow = User::select(DB::raw("time_to_sec(timediff(concat('".$bookingData['end_date']."', ' ', '".$bookingData['end_time']."'), concat('".$bookingData['start_date']."',' ', '".$bookingData['start_time']."') )) / 3600 as duration"))
										->first();

					$charges = Profile::join('services', 'services.id', '=', 'profile.service_id')
										->where('profile.user_id', $provider->id)
										->first();

					$totalFee = ($charges->charges * $bookingRow->duration);
					$bookingRow->client_name = $client->name;
					$bookingRow->provider_name = $provider->name;
					$bookingRow->amount = $totalFee;
					$bookingRow->invoice_date = Carbon::now('America/Toronto');
					$bookingRow->booking_date = Carbon::parse($bookingData['start_date'] . " " . $bookingData['start_time']);
					$bookingRow->service = $charges->service;
					$bookingRow->charges = $charges->charges;

					//return view('invoice')->with('payment', $bookingRow);
					return response()->json([
						'error_code'=>200,
						'msg_string'=> __('messages.PROCESS_SUCCESS'),
						'result' => view('invoice')->with('payment', $bookingRow)->render()
					]);

				} else {
					return response()->json(['error_code'=>500,'msg_string'=> __('messages.BOOKING_NOT_AVAILABLE'), 'result' => []]);
				}
			}
		} catch(Exception $e) {
			return response()->json(['error_code'=>500,'msg_string'=> __('messages.PROBLEM_OCCURED'), 'result' => $e->toMessage()]);
		}

	}
}
