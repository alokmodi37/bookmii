<?php

namespace App\Http\Controllers;

use JWTAuth;
use JWTAuthException;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Requests\BookingRequest as BookingRequest;

use App\Booking as Booking;
use App\User as User;
use App\Profile as Profile;
use App\CancelationHistory as CancelationHistory;
use App\NotificationToken as NotificationToken;
use App\Notification;
use App\PaymentHistory;

// Email notification section
use App\Notifications\BookingNotification as BookingNotification;
use App\Events\ServiceRequest;
use App\Events\ServiceComplete;
use App\Events\ServiceEvent;

class PaymentController extends Controller
{

 	public function getCurrentUser($request) {
		return JWTAuth::toUser($request->token);
	}

    public function token()
    {
        return response()->json([
        	'error_code'=>200,
            'msg_string'=> __('messages.PROCESS_SUCCESS'),
            'result' => \Braintree_ClientToken::generate()
           ]);

    }

    public function createCustomer() {
        /*$result = \Braintree_Customer::create([
            'firstName' => 'Mike',
            'lastName' => 'Jones',
            'email' => 'mike.jones@example.com',
            'phone' => '281.330.8004'
        ]);


          'creditCard' => [
                'cardholderName' => 'Mike',
                'cvv'   => 123,
                'number' => '12634878383838646'
            ]
        */

        $result = \Braintree_CreditCard::create([
            'customer_id' => '864149101',
            'cardholderName' => 'Mike',
            'cvv'   => 123,
            'number' => '12634878383838646'
        ]);

        if($result->success) {
            return response()->json([
                'error_code'=>500,
                'msg_string'=> 'Success',
                'result' => $result->customer->id
            ]);
        } else {
          $errorMessages = [];

          foreach($result->errors->deepAll() as $errors){
             $errorMessages[] = ($errors->message);
          }

          return response()->json([
            'error_code'=>500,
            'msg_string'=> __('messages.PROBLEM_OCCURED'),
            'result' => $errorMessages
          ]);
        }
    }

    public function pay(Request $request)
    {
        $client = $this->getCurrentUser($request);

        $result = \Braintree_Transaction::sale([
          'amount' => $request->get('amount', 0),
          'paymentMethodNonce' => $request->get('nonceFromTheClient', 0),
        ]);


        if($result->success) {
            PaymentHistory::insert([
                'user_id' => $client->id,
                'booking_id' => $request->get('booking_id', null),
                'amount' => $request->get('amount', null),
                'payment_token' => $result->transaction->id
            ]);

            $booking = Booking::where('id',  $request->get('booking_id', null))->first();
            $booking->update(['is_active' => 1]);

            $provider = User::where('id', $booking->provider_id)->first();

            $booking->client_name = $client->name;
            $booking->provider_name = $provider->name;

            $booking->requestFor = "Booking";
            $booking->other = [
                'type' => 'payment',
                'booking_id' => $booking->id,
                'client_id' => $client->id,
                'provider_id' => $provider->id
            ];

            // Notification to client and provider
            $client->notify(new BookingNotification($booking));
            $provider->notify(new BookingNotification($booking));

            $notificationSetting = Notification::where('user_id', $booking->provider_id)->first();
            if( $notificationSetting && $notificationSetting->notification) {
                $notificationToken = NotificationToken::where('user_id', $booking->provider_id)->select('firebase_token')->first();

                if( $notificationToken ){
                  $booking->firebase_token = $notificationToken->firebase_token;
                  event(new ServiceRequest($booking));
                }

            }

            return response()->json([
              'error_code'=>200,
              'msg_string'=> __('messages.PROCESS_SUCCESS'),
              'result' => $result->transaction->id
            ]);
        } else {
          $errorMessages = [];

          foreach($result->errors->deepAll() as $errors){
             $errorMessages[] = ($errors->message);
          }

          if(count($errorMessages) > 1){
            unset($errorMessages[0]);
          }

          return response()->json([
            'error_code'=>500,
            'msg_string'=> str_replace(".", " ", implode(", ", $errorMessages)),
            'result' => $errorMessages
          ]);
        }
    }
}
