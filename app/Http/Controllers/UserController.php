<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User as User;
use App\Service as Service;
use App\Profile as Profile;
use App\Notification;
use App\NotificationToken;
use App\Content;
use JWTAuthException;
use App\Booking;

// Email notification section
use App\Notifications\RegistrationSuccess as RegistrationSuccess;
use App\Notifications\ForgotPasswordNotification as ForgotPasswordNotification;

use App\Http\Requests\UserRegisrationRequest as UserRegisrationRequest;
use App\Events\ServiceEvent;

/**
 * @resource Auth
 *
 * Auth section has responsibility of Registration, Login, Token Validation
 */
class UserController extends Controller
{
    private $user;
    public function __construct(User $user){
        $this->user = $user;
    }

    public function getSupport(){
        return response()->json([
            'error_code'=>200,
            'msg_string'=> __('messages.PROCESS_SUCCESS'),
            'result' => Content::where('content_type', 'HELP')->select('content')->first()
        ]);
    }

    public function getTerms(){
        return response()->json([
            'error_code'=>200,
            'msg_string'=> __('messages.PROCESS_SUCCESS'),
            'result' => Content::where('content_type', 'TERMS')->select('content')->first()
        ]);
    }

    public function hasAllFieldValues($request) {

        foreach($request as $field => $value) {
            if(!$value){
                return false;
            }
        }

        return true;
    }

    public function removeEmptyFieldValues($request) {
        foreach($request as $field => $value) {
            if(!$value){
                unset($request[$field]);
            }
        }

        return $request;
    }

   /**
   * Registration API
   */
    public function register(UserRegisrationRequest $request){

        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if($validation->fails()){
           $errors = $validation->errors();

           return response()->json(['error_code'=>500,'msg_string'=> __('messages.VALIDATION_REQUIRED'), 'result' => $errors->toJson()]);

        } else {

            $isRegisterdUser = User::where('email', $request->get('email', null))->count();
            $isFacebookRegisterdUser = User::where('facebook_token', $request->get('facebook_token', 0))->count();

            if( $isFacebookRegisterdUser ) {
               return $this->login($request);
            }

            if( $isRegisterdUser) {
                if($facebookToken = $request->get('facebook_token', 0)) {
                    $user = User::where('email', $request->get('email', null))->first();
                    $user->update(['facebook_token' => $facebookToken]);

                    return response()->json(['error_code'=>200,'msg_string'=> __('messages.SUCCESS_TOKEN'),'token'=>JWTAuth::fromUser($user), 'registration_stage' => $user->registration_stage, 'setting' => $this->getNotification($user), 'type' => $user->type, 'process' => 'login', 'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get()]);
                }

                return response()->json(['error_code'=>500,
                  'msg_string'=>__('messages.ALLREDAY_REGISTERED_LOGIN'),
                  'token'=>[]
                ]);
            }

            $password = $request->get('password', $request->get('email')."secret");


            $user = $this->user->create([
              'name' => $request->get('name'),
              'display_name' => $request->get('display_name', null),
              'email' => $request->get('email'),
              'password' => bcrypt($password),
              'facebook_token' => $request->get('facebook_token', null),
              'device_token' => $request->get('device_token', null),
              'profile_image' => ($request->get('facebook_token', null)) ? $request->get('profile_image', null) : $this->uploadProfileImage($request->file('profile_image')),
              'remember_token' => md5(microtime())
            ]);

            $user->notify(new RegistrationSuccess());

            // Create a profile for the user;
            $hasProfile = Profile::where('profile.user_id', $user->id)->count();

            if (!$hasProfile) {
                $profileData = $request->only('about', 'dob', 'address', 'latitude', 'longitude');
                $profileData['user_id'] = $user->id;
                $profileData['service_id'] = 1;

                Profile::insert($profileData);


                $this->storeAccountDetail($request, $user);
                $this->storeCardDetail($request, $user);

            }
            $this->storeFirebaseToken($request, $user);

            $this->createSetting($user);


            //$credentials = ['email' => $request->get('email'), 'password' => $password];

            return response()->json([
                'error_code'=>200,
                'msg_string'=>__('messages.SUCCESSFUL_REGISTRATION'),
                'token'=>JWTAuth::fromUser($user),
                'process' => 'registration',
                'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get(),
                'setting' => $this->getNotification($user)
            ]);
        }
    }


     /**
    * Store Account Detail API
    */
    public function storeAccountDetail(Request $request, $user) {
        $accountDetailData =   $request->only('account_number', 'transit_number', 'branch_number');


        if( $this->hasAllFieldValues($accountDetailData) ) {

            if( AccountDetail::where('user_id', $user->id)->count() ) {
                $accountDetailRow = AccountDetail::where('user_id', $user->id)->first();

                $accountDetailRow->update($accountDetailData);
            } else {
                $accountDetailData['user_id'] = $user->id;
                AccountDetail::insert($accountDetailData);
            }
        }
    }

    /**
    * Store Card Detail API
    */
    public function storeCardDetail(Request $request, $user) {
        $cardDetailData = $request->only('first_name', 'card_number', 'card_cvv');

        if( $this->hasAllFieldValues($cardDetailData) ) {

            if( CardDetail::where('user_id', $user->id)->count() ) {
                $cardDetailRow = CardDetail::where('user_id', $user->id)->first();

                $cardDetailRow->update($cardDetailData);
            } else {
                $cardDetailData['user_id'] = $user->id;
                CardDetail::insert($cardDetailData);
            }
        }
    }


    public function getNotification($user) {
        return ($notification = Notification::where('user_id', $user->id)->select('notification')->first()) ? $notification : [];
    }

    public function createSetting($user) {
          if( !($notificationSetting = Notification::where('user_id', $user->id)->first()) ) {
                $settingData = [];
                $settingData['notification'] = 1;
                $settingData['user_id'] = $user->id;
                Notification::insert($settingData);
            }
    }


    public function uploadProfileImage($photo){
          if($photo) {
            $photoName= uniqid('profile_') . '.png';

            $photo->move( realpath('.') . env('APP_UPLOAD_FOLDER', '/public/images/uploads/'), $photoName);

            return env('APP_URL', 'http://sparehandz.apphosthub.com') . env('APP_UPLOAD_FOLDER', '/public/images/uploads/') . $photoName;
          }

          return "";
    }


    /**
    * Login API
    */

    public function login(Request $request){
        if($request->get('facebook_token', null)){
          $isUserExist = User::where('facebook_token', $request->get('facebook_token'))->count();

          if($isUserExist) {
              $currentUser = User::where('facebook_token', $request->get('facebook_token'))->first();

              try {
                 $token = JWTAuth::fromUser($currentUser);

                 User::where('id', $currentUser->id)
                      ->update(['api_token' => $token]);

              } catch (JWTAuthException $e) {
                  return response()->json(['error_code'=>500, 'msg_string'=>__('messages.FAILED_TOKEN'), 'token'=>[]]);
              }

              $this->createSetting($currentUser);
              $this->storeFirebaseToken($request, $currentUser);
              return response()->json(['error_code'=>200,
                  'msg_string'=> __('messages.SUCCESS_TOKEN'),
                  'token' => $token,
                  'type' => $currentUser->type,
                  'process' => 'login',
                  'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get(),
                  'setting' => $this->getNotification($currentUser),
                  'registration_stage' => $currentUser->registration_stage
                ]);

          } else {
              return response()->json(['error_code'=>500, 'msg_string'=> __('messages.USER_NOT_EXIST'),'result'=>[]]);
          }
        } else {
          $credentials = $request->only('email', 'password');
        }


        $token = null;
        try {
           if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error_code'=>422, 'msg_string'=> __('messages.INVALID_EMAIL_PASSWORD'),'result'=>[]]);
           } else {
               $user = JWTAuth::toUser($token);
               User::where('id', $user->id)
                    ->update(['api_token' => $token]);
           }
        } catch (JWTAuthException $e) {
            return response()->json(['error_code'=>500, 'msg_string'=>__('messages.FAILED_TOKEN'), 'result'=>[]]);
        }

        $this->createSetting($user);

        $this->storeFirebaseToken($request, $user);
        return response()->json(['error_code'=>200, 'msg_string'=>__('messages.SUCCESS_TOKEN'), 'token' => $token, 'type' => $user->type, 'process' => 'login', 'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get(), 'registration_stage' => $user->registration_stage,  'setting' => $this->getNotification($user)]);
    }


    public function storeFirebaseToken(Request $request, $user) {
            $notificationData = $request->only('firebase_token');

            if($notificationData['firebase_token']) {
                $notificationData['user_id'] = $user->id;

                if(NotificationToken::where('user_id', $user->id)->count()) {
                    $notificationRow = NotificationToken::where('user_id', $user->id)->first();
                    $notificationRow->update($notificationData);
                } else {
                    NotificationToken::insert($notificationData);
                }
            }
    }

    /**
    * Verify Email API
    */
    public function verifyEmail(Request $request){
         $email = $request->only('email');

         $userRow = User::where('email', $email)->get();

         if( count($userRow->toArray()) ) {
             return response()->json(['error_code'=>500, 'msg_string'=> __('messages.ALLREDAY_REGISTERED'), 'result' => false]);
         } else {
             return response()->json(['error_code'=>200, 'msg_string'=> __('messages.EMAIL_AVAILABLE'), 'result' => true]);
         }
    }

    /**
    * Get Services API
    */
    public function getServices(Request $request){
       return response()->json(['error_code'=>200, 'msg_string'=>'Success', 'result' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get()]);
    }

    /**
    * Forgot Password API
    */
    public function forgotPassword(Request $request) {
        $email = $request->only('email', 0);
        $userRowset = User::where('email', $email)->get();
        if( count($userRowset->toArray()) ) {
          foreach($userRowset as $userRow){
            $userRow->update(['remember_token' => md5(microtime())]);
            $userRow->notify(new ForgotPasswordNotification());
          }
          return response()->json(['error_code'=>200, 'msg_string'=> __('messages.RESET_PASSWORD_LINK'), 'result' => true]);
        } else {
          return response()->json(['error_code'=>500, 'msg_string'=> __('messages.NOT_REGISTERED'), 'result' => true]);
        }
    }

    /**
    * Validate Token API
    */
    public function isValidToken(Request $request) {
        $user = JWTAuth::toUser($request->token);

        $this->storeFirebaseToken($request, $user);
        return response()->json(['error_code'=>200, 'msg_string'=> __('messages.VALID_TOKEN'), 'result' => [
            'isValid' => true,
            'type' => $user->type,
            'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get(),
            'registration_stage' => $user->registration_stage,
            'setting' => $this->getNotification($user)
          ]
        ]);
    }

    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }

    public function sendNotification($bookingDetail, $userId, $notificationMessage = []) {
  		$notificationSetting = Notification::where('user_id', $userId)->first();
  		if( $notificationSetting && ($notificationSetting->notification == 1)) {
  				$notificationToken = NotificationToken::where('user_id', $userId)->select('firebase_token')->first();

  				if( $notificationToken ){
  				 	$bookingDetail->firebase_token = $notificationToken->firebase_token;
  				 	event(new ServiceEvent($bookingDetail, $notificationMessage));
  				}

  		}
  	}

    public function testNotification(Request $request){
        $bookingDetail = Booking::where('id', 21)->first();

        $notificationMessage = [
          'title' => 'Booking Cancel',
          'body' => 'Booking has been cancelled by leo',
          'other' => [
              'type' => 'cancelation',
              'client_id' => 22,
              'provider_id' => 24,
              'booking_id' => 21,
              'canceled_by' => 22
          ]
        ];
        $this->sendNotification($bookingDetail, 22, $notificationMessage);

        return response()->json(['error_code'=>200, 'msg_string'=> "Notification Send", 'result' => []]);
    }

    public function resetPassword(Request $request, $token=null){
        	$userRow = User::where('remember_token', $token)->count();

        	if($userRow) {
        		$validation = Validator::make($request->all(), [
    		            'password' => 'required|min:8',
    		            'repassword' => 'required|min:8|same:password'
    		    ]);

    	    	$errors = $validation->errors();
    	    	if($request->all()) {

    				if($validation->fails()){
    	    			return view('app.reset-password', ['token' => $token, 'message' => '', 'errors' => $errors]);
    	    		}

    	    		$userRow = User::where('remember_token', $token)->first();
    	    		$resetData = $request->only('password', 'repassword');

    	    		if($resetData['password']) {
    	    			$userRow->update(
    		    			[
    		    				'password' => bcrypt($resetData['password']),
    		    				'remember_token' => ''
    		    			]
    		    		);

    		    		return view('app.success-message', ['message' => 'Password has been rest, Please login with your new credentials']);
    	    		} else {
    	    			 return view('app.reset-password', ['token' => $token, 'message' => '', 'errors' => $errors]);
    	    		}

    	    	} else {
    	  	        return view('app.reset-password', ['token' => $token, 'message' => '', 'errors' => $errors]);
    	    	}
    	    } else {
    	    	return view('app.error-message', ['message' => 'Invalid token provided']);
    		}
        }

        public function uploadImage(Request $request, $token=null){

         	if ($request->hasFile('picture')) {

    		    if($request->file('picture')->isValid()) {
    		        try {
    		            $file = $request->file('picture');
    		            $name = time() . '.' . $file->getClientOriginalExtension();

    		            $request->file('picture')->move("upload", $name);

    		            return response()->json(['meessage' => 'Image uploaded']);
    		        } catch (Illuminate\Filesystem\FileNotFoundException $e) {
    		        	echo $e->getMessage();
    		        }
    		    }
    		} else {
        		return view('app.upload-images', ['token' => $token, 'message' => '']);
        	}
        }


        public function expirePosts(Request $request) {
        	$field =  DB::raw('((time_to_sec(timediff(now(), CONCAT(bookings.end_date," ", bookings.end_time)))) / 3600)');

        	Booking::where($field, ">", 24)->update(["is_active" => 0]);
        	return response()->json([
        		'error_code'=>200,
        		'msg_string'=> __('messages.PROCESS_SUCCESS'),
        		'result' => []
        	]);
        }


        public function activateAccount(Request $request, $token = null) {
            if($token) {
              $userRow = User::where('remember_token', $token)->first();

              if($userRow) {
                  $userRow->is_active = 1;
                  $userRow->remember_token = "";
                  $userRow->save();
                  return view('app.success-message', ['message' => 'Welcome to Bookmii, Your account has been activated enjoy our services']);
              }
            }

            return view('app.error-message', ['message' => 'Token provided by you is either invalid or expired, Please contact bookmii@support.com']);
        }
}
