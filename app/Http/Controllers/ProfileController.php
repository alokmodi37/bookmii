<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use JWTAuthException;
use App\User;
use App\Profile as Profile;
use App\Service as Service;
use App\ServiceImage as ServiceImage;
use App\AccountDetail as AccountDetail;
use App\CardDetail as CardDetail;
use App\Review as Review;
use App\NotificationToken as NotificationToken;
use App\Notification;
use App\Booking;
use App\Content;

/**
 * @resource Profile
 *
 * Profile section has all the related API related to Profile ADD, EDIT, DELETE, LIST, PAGINATION etc
 */
class ProfileController extends Controller
{
	public function getCurrentUser($request) {
		return JWTAuth::toUser($request->token);
	}

    public function hasAllFieldValues($request) {

        foreach($request as $field => $value) {
            if(!$value){
                return false;
            }
        }

        return true;
    }

    public function removeEmptyFieldValues($request) {
        foreach($request as $field => $value) {
            if(!$value){
                unset($request[$field]);
            }
        }

        return $request;
    }


    public function getSetting(Request $request) {
        $user = $this->getCurrentUser($request)->id;
        $result = Notification::where('user_id', $user)->first();
        return response()->json(['error_code'=>200,'msg_string'=> __('messages.PROCESS_SUCCESS'), 'result' => ($result) ? $result : []]);

    }

    public function setSetting(Request $request) {
        $user = $this->getCurrentUser($request)->id;

        $settingData = $request->only('notification');


        if( $notificationSetting = Notification::where('user_id', $user)->first()) {
            Notification::where('user_id', $user)->update($settingData);
        } else {
            $settingData['user_id'] = $user;
            Notification::insert($settingData);
        }

        return response()->json(['error_code'=>200,'msg_string'=> __('messages.PROCESS_SUCCESS'), 'result' => []]);

    }

		public function updateCharge(Request $request) {
			$authuser = $this->getCurrentUser($request);
			if($authuser->type == 'provider') {
					$newCharge = $request->input('charge', null);
					if($newCharge) {
							Profile::where('user_id', $authuser->id)
											->update(['charges' => $newCharge]);

							return $this->getProfile($request);
					}

			}

			return response()->json(['error_code'=>500, 'msg_string'=> __('messages.INVALID_REQUEST'), 'result' => []]);
		}

    /**
    * Get Profile API
    */
    public function getProfile(Request $request) {

    	if( !($user = $request->input('id', null)) )
    		$user = $this->getCurrentUser($request)->id;

    	$userProfile = Profile::join('services', 'services.id', '=', 'profile.service_id')
    						->join('users', 'users.id', '=', 'profile.user_id')
    						->where('profile.user_id', $user)
    						->select('profile.*', 'users.id as current_user_id', 'users.name', 'users.display_name', 'users.email', 'services.service', 'users.type', 'users.profile_image')
    						 ->first();

    	if($userProfile) {
    		$userProfile->service_images = Profile::find($userProfile->id)->serviceImages;
            $userProfile->other_services = Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get();

            if( !$request->input('id', null) ) {
                $userProfile->card_info = CardDetail::where('user_id', $user)->select('first_name', 'card_number', 'card_cvv')->first();
                $userProfile->card_info = ($userProfile->card_info) ? $userProfile->card_info : [];
            }


            $userProfile->account_info = AccountDetail::where('user_id', $user)->select('account_number', 'transit_number', 'branch_number')->first();

            $userProfile->account_info = ($userProfile->account_info) ? $userProfile->account_info : [];



            $reviewQuery = Review::join('users', 'reviews.sender_id', '=', 'users.id')
                                ->join('profile', 'profile.user_id', '=', 'users.id')
                                ->where('reviews.receiver_id', $userProfile->current_user_id);

            $rating =  $reviewQuery->select(DB::raw('SUM(reviews.rating) as totalRating'), DB::raw('COUNT(*) as totalReviews'))->get();

            $userProfile->rating =  0;
            if($rating && $rating[0]['totalReviews']) {
                 $userProfile->rating =  $rating[0]['totalRating'] / $rating[0]['totalReviews'];
            }

            $userProfile->reviews =  $reviewQuery->select('reviews.comment', 'reviews.rating', 'users.profile_image', 'users.name')
                                        ->limit(5)
                                        ->get();

            foreach($userProfile->other_services as $index => $value) {
                $userProfile->other_services[$index]['provider_service'] = ($value->id == $userProfile->service_id);
            }

            $userProfile->canReviews = 0;

            if($providerId = $request->input('id', null)) {
                  $currentUserId =  $this->getCurrentUser($request)->id;

                    if( Booking::where('status', 'FINISHED')
                        ->where(function($query) use ($providerId, $currentUserId){
                            $query->where('provider_id', $providerId)
                                  ->where('client_id', $currentUserId);
                         })
                        ->orWhere(function($query) use ($providerId, $currentUserId){
                            $query->where('provider_id', $currentUserId)
                                  ->where('client_id', $providerId);
                         })
                         ->count()
                    )   {
                        $userProfile->canReviews = 1;

                    }

                    if( Booking::where('status', 'CONFIRM')
                        ->where(DB::raw('concat(end_date, " ", end_time)'), '<', DB::raw('now()'))
                        ->where(function($query) use ($providerId, $currentUserId){
                            $query->where('provider_id', $providerId)
                                  ->where('client_id', $currentUserId);
                         })
                        ->orWhere(function($query) use ($providerId, $currentUserId){
                            $query->where('provider_id', $currentUserId)
                                  ->where('client_id', $providerId);
                         })
                         ->count()
                    )   {
                        $userProfile->canReviews = 1;

                    }

            }

            $siteContentRowSet = Content::get();

            foreach($siteContentRowSet as $siteContentRow){
                switch($siteContentRow->content_type){
                    case "HELP" : $userProfile->help = $siteContentRow->content; break;
                    case "TERMS" : $userProfile->terms = $siteContentRow->content; break;
                    case "POLICY" : $userProfile->policy = $siteContentRow->content; break;
                }
            }

            $userProfile = array_map(function($v){
                return (is_null($v)) ? "" : $v;
            }, $userProfile->toArray());



    		return response()->json(['error_code'=>200,'msg_string'=>'Success','result' => [$userProfile]]);
    	} else {
    		return response()->json(['error_code'=>404,'msg_string'=>'Profile not found','result' => []]);
    	}
    }



    public function uploadImage($photo, $type) {
          if($photo) {
            $photoName= uniqid($type) . '.png';

            $photo->move( realpath('.') . env('APP_UPLOAD_FOLDER', '/public/images/uploads/'), $photoName);

            return env('APP_URL', 'http://sparehandz.apphosthub.com') . env('APP_UPLOAD_FOLDER', '/public/images/uploads/') . $photoName;
          }

          return "";
    }


    /**
    * Save / Update Profile API
    */
    public function saveProfile(Request $request) {
        $response = [];
        $updateRegistrationStage = true;
    	$user = $this->getCurrentUser($request);

    	$userData = $request->only('name', 'type');
        $user->update($this->removeEmptyFieldValues($userData));


        $userQuery = Profile::where('profile.user_id', $user->id);

        $userProfile = 	$userQuery->get();

        $profileData = $request->only('service_id', 'charges', 'facebook', 'instagram', 'twitter', 'youtube', 'about', 'dob', 'address', 'latitude', 'longitude');

        if($userType = $request->get('user_type', null)){
            $user->update(['type' => $userType]);
        }

        if($displayName = $request->get('display_name', null)){
              $user->update(['display_name' => $displayName]);
        }



        if($request->file('profile_image')) {
           $profileImage = $this->uploadImage($request->file('profile_image'), 'profile_');
           $user->update(['profile_image' => $profileImage ]);
           $response['profile_image'] = $profileImage;

           $updateRegistrationStage = false;

        }


    	if( count($userProfile->toArray()) ) {
    		foreach($profileData as $field => $value){
    			if(!$value) {
    				unset($profileData[$field]);
    			}
    		}
    		$userQuery->update($profileData);

            $this->storeCardDetail($request, $user);
            $this->storeAccountDetail($request, $user);
            $response['service_image'] = $this->storeServiceImages($request, $userProfile[0]->id);

            if($updateRegistrationStage) {
                $user->update(['registration_stage' => 1]);
            }

    		return response()->json([
                'error_code' =>200,
                'msg_string' =>'Profile updated successfully',
                'result' => $response
            ]);
    	} else {
            $profileData['service_id'] = isset($profileData['service_id']) ? $profileData['service_id'] : 1;
    		$profileData['user_id'] = $user->id;

	   		$profile = Profile::insert($profileData);

            $profile = Profile::where('user_id', $user->id)->first();

            $this->storeCardDetail($request, $user);
            $this->storeAccountDetail($request, $user);
            $response['service_image'] = $this->storeServiceImages($request, $profile->id);

            if($updateRegistrationStage) {
                $user->update(['registration_stage' => 1]);
            }

	   		return response()->json([
                    'error_code' =>200,
                    'msg_string' =>'Profile created successfully',
                    'result' => $response
                ]);
    	}

    }

    public function storeServiceImages(Request $request, $profileId) {
        $uploadedServiceImages = [];
        $serviceImages = $request->only('service_image');

        if($serviceImages['service_image'] && is_array($serviceImages['service_image'])) {
            ServiceImage::where('profile_id', $profileId)->delete();

            $serviceImageData = [];
            foreach($serviceImages['service_image'] as $index => $value){
                $imagePath = $this->uploadImage($value, 'service_');
                $serviceImageData[] = [
                    'profile_id' => $profileId,
                    'image_path' => $imagePath
                ];

                $uploadedServiceImages[] = $imagePath;
            }

            ServiceImage::insert($serviceImageData);
        }

        return $uploadedServiceImages;
    }

    /**
    * Store Account Detail API
    */
    public function storeAccountDetail(Request $request, $user) {
        $accountDetailData =   $request->only('account_number', 'transit_number', 'branch_number');


        if( $this->hasAllFieldValues($accountDetailData) ) {

            if( AccountDetail::where('user_id', $user->id)->count() ) {
                $accountDetailRow = AccountDetail::where('user_id', $user->id)->first();

                $accountDetailRow->update($accountDetailData);
            } else {
                $accountDetailData['user_id'] = $user->id;
                AccountDetail::insert($accountDetailData);
            }
        }
    }

    /**
    * Store Card Detail API
    */
    public function storeCardDetail(Request $request, $user) {
        $cardDetailData = $request->only('first_name', 'card_number', 'card_cvv');

        if( $this->hasAllFieldValues($cardDetailData) ) {

            if( CardDetail::where('user_id', $user->id)->count() ) {
                $cardDetailRow = CardDetail::where('user_id', $user->id)->first();

                $cardDetailRow->update($cardDetailData);
            } else {
                $cardDetailData['user_id'] = $user->id;
                CardDetail::insert($cardDetailData);
            }
        }
    }


    public function setNotificationToken(Request $request) {
        try {
            $user = $this->getCurrentUser($request);

            $notificationData = $request->only('firebase_token');

            if($notificationData['firebase_token']) {
                $notificationData['user_id'] = $user->id;

                if(NotificationToken::where('user_id', $user->id)->count()) {
                    $notificationRow = NotificationToken::where('user_id', $user->id)->first();
                    $notificationRow->update($notificationData);
                } else {
                    NotificationToken::insert($notificationData);
                }

                return response()->json(['error_code'=>200,'msg_string'=> __('messages.PROCESS_SUCCESS'), 'result' => []]);
            }
        } catch (Exception $e) {
            return response()->json(['error_code'=>500,'msg_string'=> __('messages.PROBLEM_OCCURED'), 'result' => []]);
        }
    }


    public function getSupport(){
        return response()->json([
            'error_code'=>200,
            'msg_string'=> __('messages.PROCESS_SUCCESS'),
            'result' => Content::where('content_type', 'HELP')->select('content')->first()
        ]);
    }

    public function getTerms(){
        return response()->json([
            'error_code'=>200,
            'msg_string'=> __('messages.PROCESS_SUCCESS'),
            'result' => Content::where('content_type', 'TERMS')->select('content')->first()
        ]);
    }
}
