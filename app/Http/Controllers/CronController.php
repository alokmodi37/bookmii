<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\User as User;
class CommonController extends Controller
{
    
  public function expireBooking() {
    $expiredBookings = Booking::where('bookings.status', 'PENDING')
                  ->where(DB::raw("concat(bookings.end_date, ' ', bookings.end_time)"), '>', DB::raw("now()"))
                  ->get();

    foreach($expiredBookings as $expiredBooking){
      $expiredBooking->is_active = 0;
      $expiredBooking->update();
    }
  }

}
