<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use JWTAuthException;

use App\Chat;
use App\User;
use App\ChatHead;
use App\Post;
use App\Notification;
use App\Profile;
use App\NotificationToken;

Use App\Events\ServiceEvent;
Use App\Events\ChatEvent;


use Carbon\Carbon;
class ChatController extends Controller
{
	public function getCurrentUser($request) {
		return JWTAuth::toUser($request->token);
	}

    public function initialize(Request $request)
    {
    	$senderOne = $this->getCurrentUser($request);
        $senderTwo = User::where('id', $request->get('receiver_id', null))->first();

        if($senderOne && $senderTwo) {
        	$firebase_identifier = Chat::getFirebaseIdentifier($senderOne, $senderTwo);

	        $senderOneId = md5($senderOne->email);
	        $senderTwoId = md5($senderTwo->email);

	        if($firebase_identifier['initialize']) {

	        	if($senderOne->type == 'provider') {
	        		$charge = Profile::where('user_id', $senderOne->id)->select('charges')->first();
	        	} else {
	        		$charge = Profile::where('user_id', $senderTwo->id)->select('charges')->first();
	        	}

	            Chat::initializeChat([
	                'firebase_identifier' => $firebase_identifier['firebase'],
	                'setting' => [
	                    $senderOneId => [
	                        'image' => $senderOne->profile_image,
	                        'name' => $senderOne->name,
	                        'type' => $senderOne->type
	                    ],
	                    $senderTwoId => [
	                        'image' => $senderTwo->profile_image,
	                        'name' => $senderTwo->name,
	                        'type' => $senderTwo->type
	                    ],
	                    'charges' => $charge->charges
	                ],
	                'lastMessageId' => 0,
	                'messages' => 0,
	                'typingindicator' => [
	                    $senderOneId => false,
	                    $senderTwoId => false
	                ]

	            ]);
	        }

	        /*$dt = Carbon::now();
	        Chat::sendMessage([
	            'firebase_identifier' => $firebase_identifier['firebase'],
	             $dt->timestamp => [
	                'senderId' => $faker->randomElement([$senderOneId, $senderTwoId]),
	                'text' => $faker->text
	             ]
	        ]);*/

	        return response()->json([
		    	'error_code'=>200,
		    	'msg_string'=> __('messages.PROCESS_SUCCESS'),
		    	'result' => [
		    		'history' => Chat::getChat($firebase_identifier['firebase']),
		    		'firebase' => $firebase_identifier['firebase'],
		    		'senderId' => $senderOneId,
		    		'receiverId' => $senderTwoId
		    	]
		    ]);
        } else {
        	return response()->json(['error_code'=>500,
        		'msg_string'=> __('messages.PROBLEM_OCCURED'),
        		'result' => []]);
        }
   }

    public function historyChat(Request $request) {
    	$chatHistory = array();

    	$senderOne = $this->getCurrentUser($request);

    	$chatHeadRow = ChatHead::where('sender_one', $senderOne->id)
    			->orWhere('sender_two',  $senderOne->id)
    			->select('firebase_identifier', 'sender_one', 'sender_two')
    			->get();

    	foreach ($chatHeadRow as $key => $value) {
    		if($senderOne->type == 'provider') {
        		$charge = Profile::where('user_id', $senderOne)->select('charges')->first();
        	} else {
        		$charge = Profile::where('user_id', ($senderOne->id == $value->sender_one) ? $value->sender_two : $value->sender_one)->select('charges')->first();
        	}

    		$chatHistory[$key]['userData'] = User::where('id', ($value->sender_two == $senderOne->id) ?  $value->sender_one : $value->sender_two)
    										->select('id', 'name', 'profile_image')
    										->first();
    		$chatHistory[$key]['message'] = json_decode(Chat::getLastMessage($value->firebase_identifier));
    		$chatHistory[$key]['message'] = ($chatHistory[$key]['message']) ? $chatHistory[$key]['message'] : "";
    		$chatHistory[$key]['firebase_id'] = $value->firebase_identifier;
    		$chatHistory[$key]['charges'] = ($charge) ? $charge->chareges : 0;
		}

    	return response()->json([
		    	'error_code'=>200,
		    	'msg_string'=> __('messages.PROCESS_SUCCESS'),
		    	'result' => [
		    		'chatHistory' => $chatHistory
		    	]
		    ]);

    }

    public function sendNotification(Request $request) {
				$sender = $this->getCurrentUser($request);
        $receiver = User::where('id', $request->get('receiver_id', null))->first();

        if($receiver) {
        	$notificationSetting = Notification::where('user_id', $receiver->id)->first();
	        if( $notificationSetting && $notificationSetting->notification) {

		        $notificationToken = NotificationToken::where('user_id', $receiver->id)->select('firebase_token')->first();

		        if( $notificationToken ){
							  $firebaseToken = Chat::getFirebaseIdentifier($sender, $receiver);
		            $notificationMessage = [
		                'title' => 'You have a new message',
		                'body' =>  $sender->name . ' : ' . $request->get('message', null),
		                'other' => [
		                    'type' => 'chat',
												'firebase' => $firebaseToken['firebase'],
                        'sender_fid' => md5($sender->email),
                        'receiver_fid' => md5($receiver->email),
                        'sender_id' => $sender->id,
                        'receiver_id' => $receiver->id,
                        'sender_name' => $sender->name
		                ],
		                'firebase_token' => $notificationToken->firebase_token
		            ];
		            event(new ChatEvent($notificationMessage));
		        }

		    }
        }

        return response()->json([
        	'error_code'=>200,
        	'msg_string'=> __('messages.PROCESS_SUCCESS'),
        	'result' => []]
        );

	}

}
