<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use JWTAuthException;
use App\Profile as Profile;
use App\Service as Service;

/**
 * @resource Search
 *
 * Search section has all the related API related to searching, sorting, listing etc
 */
class SearchController extends Controller
{
		public function getCurrentUser($request) {
			if($request->token)
				return JWTAuth::toUser($request->token);

			return 0;
		}

   /**
   * Search Result API
   */
    public function getResult(Request $request) {

    	$searchText = $request->get('search_text', null);
      $serviceId = $request->get('service_id', null);

      $user = $this->getCurrentUser($request);

			if($user) {
				      $userProfile = Profile::where('user_id', $user->id)->first();

							$userProfile->latitude = ($userProfile->latitude) ? $userProfile->latitude : "43.84896927051604";
							$userProfile->longitude = ($userProfile->longitude) ? $userProfile->longitude : "-79.46143825545856";

							$latitude = $request->get('latitude', $userProfile->latitude);
							$longitude = $request->get('longitude', $userProfile->longitude);

				      $searchResult = Profile::join('services', 'services.id', '=', 'profile.service_id')
				      						->join('users', 'users.id', '=', 'profile.user_id')
				                  ->leftJoin('favourite', function($join) use ($user){
				                      $join->on('favourite.provider_id', '=', 'users.id')
				                          ->on('favourite.client_id', '=', DB::raw($user->id))
				                          ->on('favourite.is_active', '=', DB::raw(1));
				                  })
				      						->where('profile.user_id','<>', $user->id)
													->where('services.service', '<>', 'NONE');
 		  } else {

				$latitude = $request->get('latitude', "43.84896927051604");
				$longitude = $request->get('longitude', "-79.46143825545856");

				$searchResult = Profile::join('services', 'services.id', '=', 'profile.service_id')
										->join('users', 'users.id', '=', 'profile.user_id')
										->leftJoin('favourite', function($join){
												$join->on('favourite.provider_id', '=', 'users.id')
														->on('favourite.is_active', '=', DB::raw(1));
										})
										->where('users.type', '=', 'provider')
										->where('services.service', '<>', 'NONE');
			}

      if ($searchText) {
					$searchResult = $searchResult->where(function($query) use ($searchText){
						$query->where('services.service', 'like', '%' . $searchText . '%')
							    ->orWhere('users.name', 'like', '%' . $searchText . '%')
									->orWhere('users.display_name', 'like', '%' . $searchText . '%')
                  ->orWhere('profile.address', 'like', '%' . $searchText . '%');
					});
      }

      if($serviceId) {
          $searchResult = $searchResult->where('profile.service_id', $serviceId);
      }

			  //$userProfile->latitude = ($userProfile->latitude) ? $userProfile->latitude : "0.000";
        //$userProfile->longitude = ($userProfile->longitude) ? $userProfile->longitude : "0.000";

				$searchResult = $searchResult->select('profile.*', 'users.display_name', 'users.profile_image',  'users.id as current_user_id', 'users.name', 'users.email', 'services.service', DB::raw('ROUND((6371
           * acos( cos( radians(' . $latitude . ') )
             * cos( radians(profile.latitude) )
             * cos( radians(profile.longitude) - radians(' . $longitude . ')) + sin(radians(' . $latitude. '))
             * sin( radians(profile.latitude)))
        ), 2) AS distance'), DB::raw('if(favourite.id, true, false) as is_favourite'))
          ->orderBy('distance', 'asc')
          ->paginate($request->get('limit', 10));

				foreach($searchResult as $searchIndex => $searchData) {
						if( is_null($searchResult[$searchIndex]->distance) ) {
								$searchResult[$searchIndex]->distance = 0;
						}

						$dataFieldArray = ['charges', 'dob', 'about', 'twitter', 'youtube', 'facebook', 'instagram', 'address', 'latitude', 'longitude', 'profile_image', 'name', 'email', 'service'];
						foreach($dataFieldArray as $dataField) {
							if(!$searchData[$dataField])
								$searchData[$dataField] = "";
						}

				}

      	if($searchResult->count()) {
           return response()->json(['error_code'=>200, 'msg_string'=> __('messages.RESULT_FOUND'), 'result' => $searchResult]);
        } else {
           return response()->json(['error_code'=>200, 'msg_string'=> __('messages.NO_RESULT_FOUND'), 'result' => $searchResult]);
        }
    }
}
