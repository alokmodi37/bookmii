<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use JWTAuthException;
use App\Favourite as Favourite;
use App\Profile as Profile;
use App\Service as Service;
/**
 * @resource Favourite
 *
 * Favourite section has all the related API related to add to favourite, remove from favourite
 */
class FavouriteController extends Controller
{
    public function getCurrentUser($request) {
		return JWTAuth::toUser($request->token);
	}

	/**
    * Get Favourite List
    */
	public function getFavourite(Request $request) {

    $user = $this->getCurrentUser($request);

		$serviceId = $request->get('service_id', null);

		$userProfile = Profile::where('user_id', $user->id)->first();
    //echo "<pre>";print_r($userProfile);die;
    $searchResult = Profile::join('services', 'services.id', '=', 'profile.service_id')
  						->join('users', 'users.id', '=', 'profile.user_id')
	                ->join('favourite', function($join) use ($user){
	                      $join->on('favourite.provider_id', '=', 'users.id')
	                          ->on('favourite.client_id', '=', DB::raw($user->id))
	                          ->on('favourite.is_active', '=', DB::raw(1));
	                })
  						->where('profile.user_id','<>', $user->id);

                if($serviceId) {
                    $searchResult = $searchResult->where('profile.service_id', $serviceId);
                }

    $latitude = $request->get('latitude', $userProfile->latitude);
    $longitude = $request->get('longitude', $userProfile->longitude);
		//$userProfile->latitude = ($userProfile->latitude) ? $userProfile->latitude : "0.000";
    //$userProfile->longitude = ($userProfile->longitude) ? $userProfile->longitude : "0.000";

		$favouriteList = $searchResult->select('profile.*', 'users.display_name', 'users.profile_image',  'users.id as current_user_id', 'users.name', 'users.email', 'services.service', DB::raw('ROUND((6371
           * acos( cos( radians(' . $latitude . ') )
             * cos( radians(profile.latitude) )
             * cos( radians(profile.longitude) - radians(' . $longitude . ')) + sin(radians(' . $latitude. '))
             * sin( radians(profile.latitude)))
        ), 2) AS distance'), DB::raw('true as is_favourite'))
				->orderBy('distance', 'asc')
          		->paginate($request->get('limit', 10));

		if($favouriteList && $favouriteList->count()) {
			return response()->json(['error_code'=>200,'msg_string'=> __('messages.RESULT_FOUND'), 'result' => $favouriteList]);
		} else {
			return response()->json(['error_code'=>200,'msg_string'=> __('messages.NO_RESULT_FAVOURITE'), 'result' => $favouriteList]);
		}


	}

	/**
    * Add Favourite API
    */
	public function addFavourite(Request $request){
		$client = $this->getCurrentUser($request);
		$providerId = $request->get('provider_id', null);

		$favouriteQuery  = Favourite::where('provider_id', $providerId)
										->where('client_id', $client->id);

		if($favouriteQuery->count()) {
			$favouriteList = $favouriteQuery->get();
			if($favouriteList[0]->is_active) {
				return response()->json(['error_code'=>200,'msg_string'=> __('messages.ALLREADY_FAVOURITE'), 'result' => []]);
			} else {
				$favouriteQuery->update(['is_active' => 1]);
			}
		} else {
			Favourite::create([
				'client_id' => $client->id,
				'provider_id' => $providerId
			]);
		}

		return response()->json(['error_code'=>200,'msg_string'=> __('messages.FAVOURITE_ADDED'), 'result' => []]);

	}

	/**
    * Remove Favourite API
    */
	public function removeFavourite(Request $request){
		$client = $this->getCurrentUser($request);
		$providerId = $request->get('provider_id', null);

		$favouriteQuery = Favourite::where('provider_id', $providerId)
						->where('client_id', $client->id);

		if($favouriteQuery->count()) {
			$favouriteQuery->update(['is_active' => 0]);

			return response()->json(['error_code'=>200,'msg_string'=> __('messages.FAVOURITE_REMOVED'), 'result' => []]);
		} else {
			return response()->json(['error_code'=>200,'msg_string'=> __('messages.NOT_FAVOURITE'), 'result' => []]);
		}
	}
}
