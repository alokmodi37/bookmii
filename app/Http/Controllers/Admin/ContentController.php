<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Service;
use App\Notification;
use App\Content;

class ContentController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }
	public function index(){
		$content = Content::get();

		return view('admin.content.index', [
    		'contents' => $content,
    		'currentPanel' => 'content',
            'currentSubPanel' => ''
        ]);
	}


	public function edit(Request $request){
		if($contentId = $request->get('content_id', 0)) {
			$content = Content::where('id', $contentId)->first();
			$errors = null;
			if($request->isMethod('post')) {
		        $validation = Validator::make($request->all(), [ 
		            'content' => 'required'
		        ]);

		        $errors = $validation->errors();
		        if( !$validation->fails() ){
		        	$userUpdateData = $request->get('content');

		            $content->content = $userUpdateData;
		            $content->update();
		            
		            return redirect()->route('listContent');
		        }
		    }
		
			return view('admin.content.edit', [
	    		'content' => $content,
	    		'currentPanel' => 'content',
	            'currentSubPanel' => '',
	            'errors' =>  $errors
	        ]);
		}
	}

	public function detail(Request $request){
		if($contentId = $request->get('content_id', 0)) {
			$content = Content::where('id', $contentId)->first();
			return view('admin.content.detail', [
	    		'content' => $content,
	    		'currentPanel' => 'content',
	            'currentSubPanel' => ''
	        ]);
		}
	}
}