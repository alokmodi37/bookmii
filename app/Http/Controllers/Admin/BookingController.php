<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Booking;
use App\Service;
class BookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
     public function isValidValues($requestData) {
        $validRequestData = false;
        foreach($requestData as $field => $value) {
            if($value) {
                $validRequestData = true;
                break;
            }
        }

        return $validRequestData;
    }

    public function detail(Request $request) {
        $bookingRow = Booking::join(DB::raw('users as provider'), 'provider.id', '=', 'bookings.provider_id')
                                    ->join(DB::raw('users as client'), 'client.id', '=', 'bookings.client_id')
                                    ->join('user_services', 'user_services.id', '=', 'bookings.provider_id')
                                    ->join('services', 'services.id', '=', 'user_services.service_id')
                                    ->select('bookings.*', 'services.service', DB::raw('provider.name as provider_name, client.name as client_name'), DB::raw("time_to_sec(timediff(concat(bookings.end_date,' ', bookings.end_time), concat(bookings.start_date,' ', bookings.start_time) )) / 3600 as duration"))
                                    ->where('bookings.id', $request->get('booking_id', 0))
                                    ->first();

        return view('admin.booking.detail', [
            'booking' => $bookingRow,
            'currentPanel' => 'booking',
            'currentSubPanel' => ''
        ]);
    }

    public function edit(Request $request) {
        $bookingRow = Booking::join(DB::raw('users as provider'), 'provider.id', '=', 'bookings.provider_id')
                                    ->join(DB::raw('users as client'), 'client.id', '=', 'bookings.client_id')
                                    ->join('user_services', 'user_services.id', '=', 'bookings.provider_id')
                                    ->join('services', 'services.id', '=', 'user_services.service_id')
                                    ->select('bookings.*', 'services.service', DB::raw('provider.name as provider_name, client.name as client_name'), DB::raw("time_to_sec(timediff(concat(bookings.end_date,' ', bookings.end_time), concat(bookings.start_date,' ', bookings.start_time) )) / 3600 as duration"))
                                    ->where('bookings.id', $request->get('booking_id', 0))
                                    ->first();

        return view('admin.booking.edit', [
            'booking' => $bookingRow,
            'currentPanel' => 'booking',
            'currentSubPanel' => ''
        ]);
    }

    public function index(Request $request) {
    	$searchData = $request->only('search_text', 'status', 'service', 'sorting_order', 'sorting_field');



    	if( !$searchData['sorting_order'] ) {
    		$searchData['sorting_order'] = 'asc';
    	}

    	if( !$searchData['sorting_field'] ) {
    		$searchData['sorting_field'] = 'provider.name';
    	}

        $bookingRowSet = Booking::join(DB::raw('users as provider'), 'provider.id', '=', 'bookings.provider_id')
                                    ->join(DB::raw('users as client'), 'client.id', '=', 'bookings.client_id')
                                    ->join('user_services', 'user_services.id', '=', 'bookings.provider_id')
                                    ->join('services', 'services.id', '=', 'user_services.service_id')
                                    ->select('bookings.*', 'services.service', DB::raw('provider.name as provider_name, client.name as client_name'))
                                    ->orderBy($searchData['sorting_field'], $searchData['sorting_order']);

        if($this->isValidValues($searchData )) {
            foreach($searchData as $field => $value) {
                if($value) {
                    switch($field) {
                        case 'search_text' : $bookingRowSet = $bookingRowSet->where(function($query) use($value) {
                                                        return $query->where('provider.name', 'like', "%$value%")
                                                                    ->orWhere('client.name', 'like', "%$value%")
                                                                    ->orWhere('provider.email', 'like', "%$value%")
                                                                    ->orWhere('client.email', 'like', "%$value%");
                                                    });
                                             break;

                        case 'status' : $bookingRowSet = $bookingRowSet->where('bookings.status', $value); break;
                        case 'service' : $bookingRowSet = $bookingRowSet->where('services.id', $value); break;
                    }
                }
            }
        }


    	$bookingRowSet = $bookingRowSet->paginate(10);

    	if($this->isValidValues($searchData)) {
            $paginationParamData = $searchData;

            unset($paginationParamData['sorting_order']);
            unset($paginationParamData['sorting_field']);

            $bookingRowSet->appends($paginationParamData);
        }

    	return view('admin.booking.index', [
    		'bookings' => $bookingRowSet,
    		'searchData' =>  $searchData,
    		'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get(),
    		'bookingStatus' => ['PENDING','CONFIRM','REJECTED','FINISHED'],
            'currentPanel' => 'booking',
            'currentSubPanel' => '',
    	 	'getSortingClass' => function($field) use($searchData) {
    	 		if($searchData['sorting_field'] == $field) {
    	 			if($searchData['sorting_order'] == 'desc') {
    	 				return "sorting_desc";
    	 			} else {
    	 				return "sorting_asc";
    	 			}
    	 		} else {
    	 			return "sorting";
    	 		}
    	 	},
    	 	'getSortinOrder' => function($field) use($searchData) {
    	 		if($searchData['sorting_field'] == $field) {
    	 			if($searchData['sorting_order'] == 'desc') {
    	 				return "asc";
    	 			} else {
    	 				return "desc";
    	 			}
    	 		} else {
    	 			return "asc";
    	 		}
    	 	}

    	 ]);
  	}


    public function delete(Request $request) {
        $bookingId = $request->get('booking_id', 0);
        if($bookingId) {
            if($bookingDetail = Booking::where('id', $bookingId)->first()){
                $bookingDetail->delete();
            }
        }
        return redirect()->route('listBooking');
    }
}
