<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Service;
use App\Notification;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
	 public function isValidValues($requestData) {
        $validRequestData = false;
        foreach($requestData as $field => $value) {
            if($value) {
                $validRequestData = true;
                break;
            }
        }

        return $validRequestData;
    }

    public function index(Request $request){
    	$searchData = $request->only('search_text', 'user_type', 'service', 'sorting_order', 'sorting_field');


    	if($this->isValidValues($searchData )) {

    	}

    	if( !$searchData['sorting_order'] ) {
    		$searchData['sorting_order'] = 'asc';
    	}

    	if( !$searchData['sorting_field'] ) {
    		$searchData['sorting_field'] = 'users.name';
    	}


    	$notificationRowSet = User::leftJoin('notifications', 'notifications.user_id', '=', 'users.id')
    								->leftJoin('user_services', 'user_services.user_id', '=', 'users.id')
    								->leftJoin('services', 'services.id', '=', 'user_services.service_id')
    								->orderBy($searchData['sorting_field'], $searchData['sorting_order'])
    								->paginate(10);

    	return view('admin.notification.index', [
    		'notifications' => $notificationRowSet,
    		'searchData' =>  $searchData,
    		'notificationTypes' => [ 'customer' => 'Customer', 'provider' => 'Service Provider'],
    	 	'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get(),
            'currentPanel' => 'notification',
            'currentSubPanel' => 'setting',
    	 	'getSortingClass' => function($field) use($searchData) {
    	 		if($searchData['sorting_field'] == $field) {
    	 			if($searchData['sorting_order'] == 'desc') {
    	 				return "sorting_desc";
    	 			} else {
    	 				return "sorting_asc";
    	 			}
    	 		} else {
    	 			return "sorting";
    	 		}
    	 	},
    	 	'getSortinOrder' => function($field) use($searchData) {
    	 		if($searchData['sorting_field'] == $field) {
    	 			if($searchData['sorting_order'] == 'desc') {
    	 				return "asc";
    	 			} else {
    	 				return "desc";
    	 			}
    	 		} else {
    	 			return "asc";
    	 		}
    	 	}

    	 ]);
  	}

    public function notificationTemplate(Request $request){
        $searchData = $request->only('search_text', 'user_type', 'service', 'sorting_order', 'sorting_field');


        if($this->isValidValues($searchData )) {

        }

        if( !$searchData['sorting_order'] ) {
            $searchData['sorting_order'] = 'asc';
        }

        if( !$searchData['sorting_field'] ) {
            $searchData['sorting_field'] = 'users.name';
        }


        $notificationRowSet = User::leftJoin('notifications', 'notifications.user_id', '=', 'users.id')
                                    ->leftJoin('user_services', 'user_services.user_id', '=', 'users.id')
                                    ->leftJoin('services', 'services.id', '=', 'user_services.service_id')
                                    ->orderBy($searchData['sorting_field'], $searchData['sorting_order'])
                                    ->paginate(10);

        return view('admin.notification.index', [
            'notifications' => $notificationRowSet,
            'searchData' =>  $searchData,
            'notificationTypes' => [ 'customer' => 'Customer', 'provider' => 'Service Provider'],
            'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get(),
            'currentPanel' => 'notification',
            'currentSubPanel' => 'notification',
            'getSortingClass' => function($field) use($searchData) {
                if($searchData['sorting_field'] == $field) {
                    if($searchData['sorting_order'] == 'desc') {
                        return "sorting_desc";
                    } else {
                        return "sorting_asc";
                    }
                } else {
                    return "sorting";
                }
            },
            'getSortinOrder' => function($field) use($searchData) {
                if($searchData['sorting_field'] == $field) {
                    if($searchData['sorting_order'] == 'desc') {
                        return "asc";
                    } else {
                        return "desc";
                    }
                } else {
                    return "asc";
                }
            }

         ]);
    }



  	public function updateNotfication(Request $request) {
  		if($userId = $request->get('userId')) {
  			$userDetail = User::where('id', $userId)->first();
  			$userNotification = Notification::where('user_id', $userId)->first();

  			if($userDetail && $userNotification) {
  				$userNotification->notification = 1;
  				$userNotification->update();
  			}
		}
  	}
}
