<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Profile;
use App\Service;
use App\UserService;
use App\ServiceImage;
use App\Booking;
use App\ChatHead;
use App\CardDetail;
use App\AccountDetail;
use App\Favourite;
use App\Review;

use Carbon\Carbon;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function isValidValues($requestData) {
        $validRequestData = false;
        foreach($requestData as $field => $value) {
            if($value) {
                $validRequestData = true;
                break;
            }
        }

        return $validRequestData;
    }

    public function index(Request $request){
    	$searchData = $request->only('search_text', 'user_type', 'service', 'sorting_order', 'sorting_field');
    	$userRowset = User::join('profile', 'profile.user_id', '=', 'users.id')
    						->leftJoin('user_services', 'user_services.user_id', '=', 'users.id')
    						->leftJoin('services', 'services.id', '=', 'user_services.service_id');


        if( !$searchData['sorting_order'] ) {
            $searchData['sorting_order'] = 'asc';
        }

        if( !$searchData['sorting_field'] ) {
            $searchData['sorting_field'] = 'users.name';
        }


    	foreach($searchData as $field => $value) {
    		if($value) {
    			switch($field) {
    				case 'search_text'	: $userRowset = $userRowset->where(function($query) use($value) {
    										return $query->where('users.name', 'like', '%'.$value.'%')
    													 ->orWhere('users.email', 'like', '%'.$value.'%');
					    					});
					    					break;
    				case 'user_type'	: $userRowset =    $userRowset->where('users.type', $value);
					    					break;
    				case 'service'		: $userRowset =   $userRowset->where('services.id', $value);
					    					break;
    			}
    		}
    	}

    	$userRowset = $userRowset->select('users.*', 'profile.id as profile_id', 'profile.*', 'services.service', 'user_services.user_id', 'user_services.service_id')->orderBy($searchData['sorting_field'], $searchData['sorting_order'])->paginate(10);

        if($this->isValidValues($searchData)) {
            $userRowset->appends($searchData);
        }

       	return view('admin.user.index', [
    		'users' => $userRowset,
    		'searchData' =>  $searchData,
    		'userTypes' => [ 'customer' => 'Customer', 'provider' => 'Service Provider'],
    	 	'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get(),
            'currentPanel' => 'user',
            'currentSubPanel' => '',
            'getSortingClass' => function($field) use($searchData) {
                if($searchData['sorting_field'] == $field) {
                    if($searchData['sorting_order'] == 'desc') {
                        return "sorting_desc";
                    } else {
                        return "sorting_asc";
                    }
                } else {
                    return "sorting";
                }
            },
            'getSortinOrder' => function($field) use($searchData) {
                if($searchData['sorting_field'] == $field) {
                    if($searchData['sorting_order'] == 'desc') {
                        return "asc";
                    } else {
                        return "desc";
                    }
                } else {
                    return "asc";
                }
            }
    	 ]);
    }


    public function edit(Request $request) {
        $errors = [];

        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'user_type' => 'required',
            'dob' => 'required',
            'about' => 'required',
            'address' => 'required'
        ]);

        $errors = $validation->errors();
        if(!$validation->fails()){
            $userUpdateData = $request->only('name', 'user_type', 'service', 'dob', 'about', 'address', 'facebook', 'instagram', 'twitter', 'youtube');
        }


    	$userRow = User::join('profile', 'profile.user_id', '=', 'users.id')
    						->where('users.id', $request->get('user_id', null))
    						->select('users.*', 'profile.id as profile_id', 'profile.*')
    						->first();



    	$userRow->services = UserService::join('services', 'services.id', '=', 'user_services.service_id')
    										->where('user_services.user_id',  $request->get('user_id', null))
    										->first();

    	$userRow->servicesImages = ServiceImage::where('service_images.profile_id', $userRow->profile_id)
    										->get();

    	return view('admin.user.edit', [
    		'user' => $userRow,
    		'userTypes' => [ 'customer' => 'Customer', 'provider' => 'Service Provider'],
    	 	'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get(),
            'currentPanel' => 'user',
            'currentSubPanel' => '',
            'errors' =>  $errors
    	 ]);
    }

    public function view(Request $request) {
    	$userRow = User::join('profile', 'profile.user_id', '=', 'users.id')
    						->where('users.id', $request->get('user_id', null))
    						->select('users.*', 'profile.id as profile_id', 'profile.*')
    						->first();


    	$userRow->services = UserService::join('services', 'services.id', '=', 'user_services.service_id')
    										->where('user_services.user_id',  $request->get('user_id', null))
    										->first();

    	$userRow->servicesImages = ServiceImage::where('service_images.profile_id', $userRow->profile_id)
    										->get();

    	return view('admin.user.detail', [
    		'user' => $userRow,
    		'userTypes' => [ 'customer' => 'Customer', 'provider' => 'Service Provider'],
    	 	'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get(),
            'currentPanel' => 'user',
            'currentSubPanel' => ''
    	 ]);
    }

    public function delete(Request $request) {
        $userId = $request->get('user_id', 0);

        if($userId) {
            User::where('id', $userId)->delete();
            $profile = Profile::where('user_id', $userId)->first();
            if($profile) {
              ServiceImage::where('profile_id', $profile->id)->delete();
              Profile::where('user_id', $userId)->delete();
            }
            Booking::where('provider_id', $userId)
                    ->orWhere('client_id', $userId)
                    ->delete();

            Review::where('receiver_id', $userId)->orWhere('sender_id', $userId)->delete();
            Favourite::where('provider_id', $userId)->orWhere('client_id', $userId)->delete();
            ChatHead::where('sender_one', $userId)->orWhere('sender_two', $userId)->delete();
            if($CardDetail = CardDetail::where('user_id', $userId)->first()) {
                $CardDetail->delete();
            }

            if($AccountDetail = AccountDetail::where('user_id', $userId)->first()) {
                $AccountDetail->delete();
            }
        }
        return redirect()->route('listUser');
    }
}
