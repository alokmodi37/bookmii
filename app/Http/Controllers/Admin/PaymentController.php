<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Booking;
use App\Service;
use App\PaymentHistory;
class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function isValidValues($requestData) {
        $validRequestData = false;
        foreach($requestData as $field => $value) {
            if($value) {
                $validRequestData = true;
                break;
            }
        }

        return $validRequestData;
    }

    public function invoice(Request $request) {

        if ($paymentId = $request->get('payment_id', 0)) {
            $paymentRow = PaymentHistory::join('bookings', 'bookings.id', '=', 'payment_history.booking_id')
                                    ->join(DB::raw('users as provider'), 'provider.id', '=', 'bookings.provider_id')
                                    ->join(DB::raw('users as client'), 'client.id', '=', 'bookings.client_id')
                                    ->join('user_services', 'user_services.user_id', '=', 'bookings.provider_id')
                                    ->join('services', 'services.id', '=', 'user_services.service_id')
                                    ->select('payment_history.*',
                                        'bookings.start_date',
                                        'services.service',
                                        DB::raw('provider.name as provider_name, client.name as client_name'
                                    ))
                                    ->where('payment_history.id', $paymentId)
                                    ->first();

            return view('admin.payments.invoice', [
                'payment' => $paymentRow,
                'currentPanel' => 'payment',
                'currentSubPanel' => ''
             ]);
        } else {

        }


    }

    public function index(Request $request){
    	$searchData = $request->only('search_text', 'status', 'service', 'sorting_order', 'sorting_field');


        if( !$searchData['sorting_order'] ) {
    		$searchData['sorting_order'] = 'asc';
    	}

    	if( !$searchData['sorting_field'] ) {
    		$searchData['sorting_field'] = 'provider.name';
    	}


    	$paymentRowSet = PaymentHistory::join('bookings', 'bookings.id', '=', 'payment_history.booking_id')
    								->join(DB::raw('users as provider'), 'provider.id', '=', 'bookings.provider_id')
    								->join(DB::raw('users as client'), 'client.id', '=', 'bookings.client_id')
    								->join('user_services', 'user_services.user_id', '=', 'bookings.provider_id')
    								->join('services', 'services.id', '=', 'user_services.service_id')
    								->select('payment_history.*',
                                        'bookings.start_date',
                                        'bookings.start_time',
                                        'bookings.end_date',
                                        'bookings.end_time',
                                        'services.service',
                                        DB::raw('provider.name as provider_name, client.name as client_name'))
    								->orderBy($searchData['sorting_field'], $searchData['sorting_order']);

        foreach($searchData as $field => $value) {

            if($value) {
                switch($field) {
                    case 'search_text'  : $paymentRowSet = $paymentRowSet->where(function($query) use($value) {

                                                    return $query->where('provider.name', 'like', "%$value%")
                                                                    ->orWhere('client.name', 'like', "%$value%")
                                                                    ->orWhere('provider.email', 'like', "%$value%")
                                                                    ->orWhere('client.email', 'like', "%$value%");

                                            });
                                            break;
                    case 'user_type'    : $paymentRowSet =    $paymentRowSet->where('users.type', $value);
                                            break;
                    case 'service'      : $paymentRowSet =   $paymentRowSet->where('services.id', $value);
                                            break;
                }
            }
        }


    	$paymentRowSet = $paymentRowSet->paginate(10);

    	if($this->isValidValues($searchData)) {
            $paymentRowSet->appends($searchData);
        }

    	return view('admin.payments.index', [
    		'payments' => $paymentRowSet,
    		'searchData' =>  $searchData,
    		'services' => Service::where('service', '<>', 'NONE')->orderBy('order', 'asc')->get(),
    		'bookingStatus' => ['PENDING','CONFIRM','REJECTED','FINISHED'],
            'currentPanel' => 'payment',
            'currentSubPanel' => '',
    	 	'getSortingClass' => function($field) use($searchData) {
    	 		if($searchData['sorting_field'] == $field) {
    	 			if($searchData['sorting_order'] == 'desc') {
    	 				return "sorting_desc";
    	 			} else {
    	 				return "sorting_asc";
    	 			}
    	 		} else {
    	 			return "sorting";
    	 		}
    	 	},
    	 	'getSortinOrder' => function($field) use($searchData) {
    	 		if($searchData['sorting_field'] == $field) {
    	 			if($searchData['sorting_order'] == 'desc') {
    	 				return "asc";
    	 			} else {
    	 				return "desc";
    	 			}
    	 		} else {
    	 			return "asc";
    	 		}
    	 	}

    	 ]);
  	}
}
