<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Booking;

class CommonController extends Controller
{
    public function resetPassword(Request $request, $token=null){
    	$userRow = User::where('remember_token', $token)->count();

    	if($userRow) {
    		$validation = Validator::make($request->all(), [
		            'password' => 'required|min:8',
		            'repassword' => 'required|min:8|same:password'
		    ]);

	    	$errors = $validation->errors();
	    	if($request->all()) {

				if($validation->fails()){
	    			return view('app.reset-password', ['token' => $token, 'message' => '', 'errors' => $errors]);
	    		}

	    		$userRow = User::where('remember_token', $token)->first();
	    		$resetData = $request->only('password', 'repassword');

	    		if($resetData['password']) {
	    			$userRow->update(
		    			[
		    				'password' => bcrypt($resetData['password']),
		    				'remember_token' => ''
		    			]
		    		);

		    		return view('app.success-message', ['message' => 'Password has been rest, Please login with your new credentials']);
	    		} else {
	    			 return view('app.reset-password', ['token' => $token, 'message' => '', 'errors' => $errors]);
	    		}

	    	} else {
	  	        return view('app.reset-password', ['token' => $token, 'message' => '', 'errors' => $errors]);
	    	}
	    } else {
	    	return view('app.error-message', ['message' => 'Invalid token provided']);
		}
    }

    public function uploadImage(Request $request, $token=null){

     	if ($request->hasFile('picture')) {

		    if($request->file('picture')->isValid()) {
		        try {
		            $file = $request->file('picture');
		            $name = time() . '.' . $file->getClientOriginalExtension();

		            $request->file('picture')->move("upload", $name);

		            return response()->json(['meessage' => 'Image uploaded']);
		        } catch (Illuminate\Filesystem\FileNotFoundException $e) {
		        	echo $e->getMessage();
		        }
		    }
		} else {
    		return view('app.upload-images', ['token' => $token, 'message' => '']);
    	}
    }


    public function expirePosts(Request $request) {
    	$field =  DB::raw('((time_to_sec(timediff(now(), CONCAT(bookings.end_date," ", bookings.end_time)))) / 3600)');

    	Booking::where($field, ">", 24)->update(["is_active" => 0]);
    	return response()->json([
    		'error_code'=>200,
    		'msg_string'=> __('messages.PROCESS_SUCCESS'),
    		'result' => []
    	]);
    }


    public function activateAccount(Request $request, $verification){
        dd('acitivate');
    }

}
