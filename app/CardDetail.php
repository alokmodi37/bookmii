<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CardDetail extends Authenticatable
{
    use Notifiable;

    protected $table = 'card_detail';

    protected $fillable = [
        'user_id', 'first_name', 'card_number', 'card_cvv'
    ];

    protected $hidden = ['is_active', 'created_at', 'updated_at'];
    public $timestamps = true;
}
