<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Profile extends Authenticatable
{
    use Notifiable;

    protected $table = 'profile';

    protected $fillable = [
        'user_id', 'service_id', 'about', 'dob', 'address', 'latitude', 'longitude', 'facebook', 'twitter', 'instagram', 'youtube'
    ];

    protected $hidden = ['user_id', 'is_active'];

    public function serviceImages()
    {
        return $this->hasMany('App\ServiceImage');
    }
}
