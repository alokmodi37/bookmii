<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Favourite extends Authenticatable
{
    use Notifiable;

    protected $table = 'favourite';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = [
        'provider_id', 'client_id', 'is_active'
    ];
}