<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Content extends Authenticatable
{
    use Notifiable;

    protected $table = 'content';
    public $timestamps = false;

    protected $hidden = ['is_active'];
}